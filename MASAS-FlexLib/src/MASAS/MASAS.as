package MASAS
{
    
    import com.adobe.utils.DateUtil;
    
    import flash.events.Event;
    import flash.events.EventDispatcher;
    import flash.net.URLRequest;
    import flash.net.URLVariables;
    import flash.net.navigateToURL;
    import flash.utils.setTimeout;
    
    import mx.controls.Alert;
    import mx.messaging.messages.HTTPRequestMessage;
    import mx.rpc.events.FaultEvent;
    import mx.rpc.events.ResultEvent;
    import mx.rpc.http.HTTPService;
    
    import MASAS.Publish.PublicationManager;
    import MASAS.common.ProxyUrl;
    import MASAS.core.Account;
    import MASAS.core.AuthenticationMethodEnum;
    import MASAS.core.EnumLoadingState;
    import MASAS.core.Hub;
    import MASAS.data.HubSettings;
    import MASAS.error.MASASError;
    import MASAS.events.HubChangedEvent;
    import MASAS.events.SignedInEvent;
    import MASAS.events.SignedOutEvent;
    import MASAS.feedlib.cap.Alert;
    import MASAS.feedlib.events.FeedEvent;
    import MASAS.feedlib.feeds.MasasFeed;
    import MASAS.feedlib.utilities.SupportedLocale;
    import MASAS.model.EntryModelManager;
    import MASAS.model.Selection.SelectionManager;
    import MASAS.symbology.SymbologyManager;
    
    public class MASAS extends EventDispatcher
    {
        
        // Events...
        [Event(name=SignedInEvent.MASAS_SIGNED_IN, type="masas.events.SignedInEvent")]
        [Event(name=SignedOutEvent.MASAS_SIGNED_OUT, type="masas.events.SignedOutEvent")]
        [Event(name=HubChangedEvent.MASAS_HUB_CHANGED, type="masas.events.HubChangedEvent")]
        
        // Private members...
        private var _authenticationMethod  :AuthenticationMethodEnum = AuthenticationMethodEnum.useDefault;
        
        private var _token : String = null;
        private var _proxy : String = null;
        private var _hubUrl : ProxyUrl = null;
        private var _hubAccessUrl : ProxyUrl = null;
        
        private var _activeUser : Account = null;
        private var _language : SupportedLocale = SupportedLocale.en_CA;
        
        private var _availableHubsConfigState : String = EnumLoadingState.UNKNOWN;
        private var _availableHubs : Array = new Array();
        private var _defaultHub	: Hub = null;
        
        private var _configXML : XML;
        
        // Misc Managers...
        private var _symbolMgr : SymbologyManager = null;
        private var _publicationMgr : PublicationManager = null;
        // TODO: Create a Configuration Manager...
        
        // Data Managers...
        private var _masasDataMgr : MasasFeed;
        
        // Model Managers...
        private var _entryModelMgr : EntryModelManager;
        
        // Presentation Managers...
        // TODO: Move ArcPresentation Manager here...
        
        // Selection Manager...
        private var _selectionMgr : SelectionManager;
        
        // Accessors...
        
        public function get authenticationMethod() : AuthenticationMethodEnum
        {
            return _authenticationMethod;
        }
        
        public function set authenticationMethod( value : AuthenticationMethodEnum ) : void
        {
            _authenticationMethod = value;
        }
        
        public function get selectionMgr() : SelectionManager
        {
            return _selectionMgr;
        }
        
        public function get masasDataMgr() : MasasFeed
        {
            return _masasDataMgr;
        }
        
        public function get entryModelMgr() : EntryModelManager
        {
            return _entryModelMgr;
        }
        
        public function get defaultHub() : Hub
        {
            return _defaultHub;
        }
        
        public function set defaultHub( value : Hub ) : void
        {
            _defaultHub = value;
        }
        
        public function get availableHubs() : Array
        {
            return _availableHubs;
        }
        
        public function get symbolManager() : SymbologyManager
        {
            return _symbolMgr;
        }
        
        public function get publicationManager() : PublicationManager
        {
            return _publicationMgr;
        }
        
        public function get language() : SupportedLocale
        {
            return _language;
        }
        
        public function set language( value : SupportedLocale ) : void
        {
            _language = value;
        }
        
        public function get proxy() : String
        {
            return _proxy;
        }
        
        public function set proxy( value : String ) : void
        {
            _proxy = value;
        }
        
        public function get hubUrl() : ProxyUrl
        {
            return _hubUrl;
        }
        
        public function set hubUrl( value : ProxyUrl ) : void
        {
            _hubUrl = value;
            
            // Clear the selections...
            _selectionMgr.deSelectAll();
            
            // Dispatch the Hub Changed event...
            dispatchEvent( new HubChangedEvent( HubChangedEvent.MASAS_HUB_CHANGED ) );
            
            if( _hubUrl )
            {
                // Start the data sources...
                _masasDataMgr.start();
            }
        }
        
        public function get hubAccessUrl():ProxyUrl
        {
            return _hubAccessUrl;
        }
        
        public function set hubAccessUrl(value:ProxyUrl):void
        {
            _hubAccessUrl = value;
        }
        
        public function get configXML():XML
        {
            return _configXML;
        }
        
        public function set configXML(value:XML):void
        {
            _configXML = value;
        }
        
        public function get token() : String
        {
            return _token;
        }
        
        public function get activeUser() : Account
        {
            return _activeUser;
        }
        
        public function get isConnected() : Boolean
        {
            return ( _activeUser != null && _hubUrl );
        }
        
        // Constructor...
        public function MASAS()
        {
            
        }
        
        // Public Methods...
        
        public function init() : void
        {
            _symbolMgr		= new SymbologyManager( this, configXML );
            _publicationMgr	= new PublicationManager( this );
            
            _masasDataMgr	= new MasasFeed( loadHubSettings(), this );
            
            _entryModelMgr	= new EntryModelManager( this, _masasDataMgr );
            
            _selectionMgr	= new SelectionManager( this );
            
            loadHubConfig( configXML.MASAS.hubConfigURL );
            
            _masasDataMgr.addEventListener( FeedEvent.FEED_ERROR_HTTP, masasDataMgr_feedEventError );
        }
        
        public function signIn( token : String, success : Function = null, failure: Function = null ) : Boolean
        {
            var retValue : Boolean = false;
            
            // Valid token?
            if( token != null )
            {
                // Request the User Account...
                GetUserAccountByToken( token, 
                    function( result : String ) : void
                    { 
                        // success
                        _activeUser = new Account( result, _availableHubs );
                        
                        // Keep the token on hand...
                        _token = token;
                        
                        // Dispatch the Signed In event...
                        dispatchEvent( new SignedInEvent( SignedInEvent.MASAS_SIGNED_IN ) );
                        
                        // Call the callback...
                        if( success != null ) {
                            success();
                        }
                    },
                    function( result : Event ) : void
                    {
                        // failure
                        
                        // Call the callback...
                        if( failure != null ) {
                            failure( result );
                        }
                    } ); 
                
                retValue = true;
            }
            
            return retValue;
        }
        
        public function signOut() : Boolean
        {
            var retValue : Boolean = false;
            
            if( dispatchEvent( new SignedOutEvent( SignedOutEvent.MASAS_SIGNED_OUT ) ) )
            {
                // Clear the selections...
                _selectionMgr.deSelectAll();
                
                // Stop the data sources...
                _masasDataMgr.stop();
                
                // Clear the internal data...
                _entryModelMgr.removeAll();
                
                // Clear the token...
                _token = null;
                
                // Clear the user...
                _activeUser = null;
                
                // Clear the hub...
                hubUrl = null;
                
                retValue = true;
            }
            
            return retValue;
        }
        
        public function refreshDataSources( delayRequest : Boolean = false ) : Boolean
        {
            var retValue : Boolean = false;
            
            if( isConnected )
            {
                var timeout : Number = 0;
                if( delayRequest ) {
                    timeout = 1000;
                }
                
                setTimeout( function() : void {
                    _masasDataMgr.requestAtomFeed( onRequestAtomFeedSuccess, onRequestAtomFeedFailed );
                }, timeout );
                
                retValue = true;
            }
            
            return retValue;
        }
        
        private function onRequestAtomFeedSuccess() : void
        {
        }
        
        private function onRequestAtomFeedFailed( errorMessage : String ) : void
        {
            
            // Show an error!
            mx.controls.Alert.show( errorMessage, "Feed Request Error" );
            
            //			var errObject:Object = event.data;
            //			
            //			var errorMsg:String = "Feed HTTP ERROR \n\n"
            //			errorMsg += "Server: " + errObject.server + "\n\n";
            //			errorMsg += "Response Uri: " + errObject.responseUri + "\n\n";
            //			errorMsg += "Message: " + errObject.message + "\n\n";
            //			errorMsg += "Source: " + errObject.source + "\n\n";
            //			errorMsg += "Status Code: " + errObject.statusCode + "\n\n";
            //			errorMsg += "Status Description: " + errObject.statusDesc;
            //			showAlert(errorMsg);
        }
        
        private function masasDataMgr_feedEventError( feedEvent : FeedEvent ) : void
        {
            // Show an error!
            if( feedEvent.data ) {
                mx.controls.Alert.show( feedEvent.data.toString(), "Feed Request Error" );
            }
            else {
                mx.controls.Alert.show( feedEvent.toString(), "Feed Request Error" );
            }
        }	
        
        private function loadHubConfig( hubConfigURL : String ) : void
        {
            var http:HTTPService = new HTTPService();
            
            // Set the service properties...
            http.url = hubConfigURL;
            http.method = HTTPRequestMessage.GET_METHOD;
            http.resultFormat = "e4x";
            
            // Hook up the events...
            http.addEventListener( ResultEvent.RESULT, 
                function( event : ResultEvent ) : void
                {
                    var hubConfig : * = event.result;
                    
                    try
                    {
                        var defaultHubId : String;
                        if( hubConfig.@defaultHubId != undefined ) {
                            defaultHubId = hubConfig.@defaultHubId; 
                        }
                        
                        for each ( var hub : XML in hubConfig.hub )
                        {
                            var newHub : Hub = new Hub();
                            newHub.fromXML( hub );
                            _availableHubs[ newHub.url ] = newHub;
                            
                            if( newHub.identifier == defaultHubId ) {
                                _defaultHub = newHub;
                            }
                        }
                        
                        _availableHubsConfigState = EnumLoadingState.LOADED;
                    }
                    catch( err : Error ) {
                        _availableHubsConfigState = EnumLoadingState.ERROR;
                        mx.controls.Alert.show( "Error: Could not process the Hub Config file" );
                    }
                    
                } );
            
            http.addEventListener( FaultEvent.FAULT,
                function( event : FaultEvent ) : void
                {
                    _availableHubsConfigState = EnumLoadingState.ERROR;
                    mx.controls.Alert.show( "Error: Could not retrieve the Hub Config file" );
                } );
            
            // Send the request...
            _availableHubsConfigState = EnumLoadingState.LOADING;
            http.send();
        }
        
        private function loadHubSettings() : HubSettings
        {
            var hubSettings : HubSettings = new HubSettings();
            
            if( _configXML.MASAS != undefined )
            {
                var settings : * = _configXML.MASAS;
                
                if( settings.autoRefresh != undefined )
                {
                    if( settings.autoRefresh.@enable && settings.autoRefresh.@enable == "true" ) {
                        hubSettings.autoRefreshEnabled = true;	
                    }
                    
                    if( settings.autoRefresh.@rate != undefined ) {
                        hubSettings.autoRefreshRate = parseInt( settings.autoRefresh.@rate, 10 );	
                    }
                }
                
                if( settings.parameters != undefined )
                {
                    var params : * = _configXML.MASAS.parameters;
                    
                    if( params.dateTime  != undefined )
                    {
                        hubSettings.useDateParameter = params.dateTime.@active;
                        if( params.dateTime.range != undefined )
                        {
                            if( params.dateTime.range.start != undefined ) {
                                hubSettings.dtStart = DateUtil.parseW3CDTF( params.dateTime.range.start );
                            }	
                            if( params.dateTime.range.end != undefined ) {
                                hubSettings.dtEnd = DateUtil.parseW3CDTF( params.dateTime.range.end );
                            }
                        }
                        if( params.dateTime.since != undefined ) {
                            hubSettings.dtSince = DateUtil.parseW3CDTF( params.dateTime.since );	
                        }
                    }
                }
            }
            
            return hubSettings;
        }
        
        private function GetUserAccountByToken( token : String, success : Function = null, failure: Function = null ) : void
        {
            if( !_hubAccessUrl )
            {
                throw new MASASError( "The Hub Access URL has not been defined!", MASASError.MASAS_ERROR_HUBACCESS_URL );
            }
            
            var urlVar: URLVariables = null;
            
            if( authenticationMethod == AuthenticationMethodEnum.useDefault )
            {
                urlVar = new URLVariables();
                urlVar.query_secret = token;
                urlVar.secret = token;
            }
            
            GetDataFromHub( _hubAccessUrl, urlVar, success, failure );
        }
        
        private function GetDataFromHub( proxyUrl : ProxyUrl, urlVar: URLVariables = null, success : Function = null, failure: Function = null ) : void
        {
            
            var http:HTTPService = new HTTPService();
            
            // Set the service properties...
            http.url = proxyUrl.url;
            if( authenticationMethod == AuthenticationMethodEnum.useDefault ) {
                http.url += "?" + urlVar.toString();	
            }
            
            http.method = HTTPRequestMessage.GET_METHOD;
            http.resultFormat = "text";
            http.contentType = "application/json";
            
            // This gets removed on a "GET", so it can't be done... yet.
            //			http.headers = { Authorization : "MASAS-Secret " + token };
            
            // Hook up the events...
            http.addEventListener( ResultEvent.RESULT, 
                function( event : ResultEvent ) : void
                {
                    // We may get an invalid response, so take a look at the response quickly...
                    if( isResultValid( event ) )
                    {
                        if( success != null ) {
                            success( event.result );
                        }   
                    }
                    else
                    {
                        if( failure != null ) {
                            failure( event );
                        }
                    }
                    
                } );
            
            http.addEventListener( FaultEvent.FAULT,
                function( event : FaultEvent ) : void {
                    if( failure != null ) {
                        failure( event );
                    }
                } );
            
            // Send the request...
            http.send();
        }
        
        private function isResultValid( event : ResultEvent ) : Boolean
        {
            var retValue : Boolean = true;
            
            var resultObj : * = JSON.parse( event.result.toString() );
            var statusCode : Number = resultObj["statusCode"];
            
            if( statusCode && statusCode >= 400 )
            {
                retValue = false;
            }
            
            return retValue;
        }
        
        public function selectHub( newHub : Hub ) : void
        {
            if( newHub != null ) {
                hubUrl = new ProxyUrl( newHub.url, proxy );
            }
        }
        
        public function requestLink_direct( url : String ) : void
        {
            navigateToURL( new URLRequest( url ) );
        }
        
        public function requestLink( url : String, useToken : Boolean = false ) : void
        {
            var proxyUrl : ProxyUrl = new ProxyUrl( url, _proxy );
            var requestUrl : String = proxyUrl.url;
            
            if( useToken )
            {
                if( authenticationMethod == AuthenticationMethodEnum.useDefault ) {
                    requestUrl += "?secret=" + _token;
                }
            }
            
            navigateToURL( new URLRequest( requestUrl ) );
        }
        
        public function requestAlert( alertUrl : String, successCallback : Function, failedCallback : Function ) : void
        {
            var http:HTTPService = new HTTPService();
            
            // Proxy/URL...
            var proxyURL : ProxyUrl = new ProxyUrl( alertUrl, _hubUrl.proxyUrl );
            
            // Set the service properties...
            http.url = proxyURL.url;
            
            if( authenticationMethod == AuthenticationMethodEnum.useDefault ) {
                http.url += "?secret=" + _token;
            }
            
            http.method = HTTPRequestMessage.GET_METHOD;
            http.resultFormat = "e4x";
            
            // Hook up the events...
            http.addEventListener( ResultEvent.RESULT, 
                function( event : ResultEvent ) : void
                {
                    var xml : XML;
                    var alert : MASAS.feedlib.cap.Alert = null;
                    
                    try
                    {
                        //create an 'Alert' using its 'fromXML' method  
                        xml = XML( event.result );
                        alert = MASAS.feedlib.cap.Alert.fromXML( xml );
                        
                        if( successCallback != null ) {
                            successCallback( alert );
                        }
                    }
                    catch( error : Error )
                    {
                        if( failedCallback != null ) {
                            failedCallback( error );
                        }
                        
                        mx.controls.Alert.show( "The CAP XML Could not be converted to an Alert:\n\n" + error.message, "Error!" );
                    }
                    
                } );
            
            http.addEventListener( FaultEvent.FAULT,
                function( event : FaultEvent ) : void
                {
                    if( failedCallback != null ) {
                        failedCallback( new Error( "Could not retrieve the Alert." ) );
                    }
                    
                    mx.controls.Alert.show( "Error: Could not retrieve the Alert." );
                } );
            
            // Send the request...
            http.send();
        }
        
    }
    
}