package MASAS.common
{
	
	public class ProxyUrl
	{
		
		// Private members...
		private var _baseUrl	: String;
		private var _proxyUrl	: String;
		
		// Accessors...
		public function get baseUrl() : String
		{
			return _baseUrl;
		}
		
		public function set baseUrl( baseUrl : String ) : void
		{
			_baseUrl = baseUrl;
		}
		
		public function get proxyUrl() : String
		{
			return _proxyUrl;
		}
		
		public function set proxyUrl( proxyUrl : String ) : void
		{
			_proxyUrl = proxyUrl;
		}
		
		public function get url() : String
		{
			var url : String = _baseUrl;
			
			if( _proxyUrl ) {
				url = _proxyUrl + "?" + url;
			}

			return url;
		}
		
		// Constructor...
		public function ProxyUrl( baseUrl : String, proxyUrl : String = null )
		{
			_baseUrl	= baseUrl;
			_proxyUrl	= proxyUrl;
		}
		
		public function toString() : String
		{
			return url;
		}

	}
	
}