package MASAS.Publish
{
    
    import com.adobe.utils.DateUtil;
    
    import MASAS.MASAS;
    import MASAS.feedlib.cap.Alert;
    import MASAS.feedlib.cap.AlertInfo;
    import MASAS.feedlib.cap.enumerations.AlertMsgType;
    import MASAS.model.EntryModel;
    
    public class PublicationManager
    {
        
        // Private Members...
        
        private var _masas : MASAS = null;
        
        public function PublicationManager( masas : MASAS )
        {
            _masas = masas;	
        }
        
        // Cancel the currently selected entry's alert. We do not need to notify the Publish widget to cancel an Alert
        public function cancelEntry( entryModel : EntryModel, reason : String = "" ) : void
        {
            if( entryModel )
            {
                if( entryModel.containsAlert && entryModel.alert )
                {
                    var alertToCancel : Alert = entryModel.alert;
                    
                    // References - must be sender,identifier,sent
                    var references:String = entryModel.alert.sender + "," + entryModel.alert.identifier + "," + DateUtil.toW3CDTF(entryModel.alert.sent).replace("+00:00", "-00:00");
                    
                    if ( entryModel.alert.referenceIDs.length > 0)
                    {
                        references += " " + entryModel.alert.referenceIDs;
                    }
                    
                    // Handling codes - mark as CAP-CP ver. 0.3
                    
                    // Check if Alert alraedy has a CAP-CP handling code
                    if ( entryModel.alert.handlingCode  )
                    {
                        var containsCode:Boolean = false;
                        for each (var code:String in entryModel.alert.handlingCode)
                        {
                            if (code == "profile:CAP-CP:0.3")
                            {
                                containsCode = true;
                            }
                        }
                        // If it doesn't, then add it
                        if (!containsCode)
                        {
                            alertToCancel.handlingCode.push("profile:CAP-CP:0.3");
                        }
                    }
                    else
                    {
                        alertToCancel.handlingCode = new Vector.<String>();
                        alertToCancel.handlingCode.push("profile:CAP-CP:0.3");
                    }
                    
                    alertToCancel.identifier = entryModel.alert.sender + new Date().time.toString();
                    alertToCancel.sent = new Date();
                    alertToCancel.alertMsgType = AlertMsgType.Cancel;
                    alertToCancel.referenceIDs = references;
                    
                    // Add cancellation reason:
                    for each( var info : AlertInfo in alertToCancel.alertInfo )
                    {
                        var newDescription : String = createReasonContent( reason );
                        
                        if( info.description ) {
                            newDescription += info.description;
                        }
                        
                        info.description = newDescription;
                    }
                    
                    // Apply the update...
                    _masas.masasDataMgr.update( entryModel.entryData, alertToCancel );
                }
                else
                {
                    // Add cancellation reason...
                    entryModel.entryData.content = createReasonContent( reason ) + entryModel.entryData.content;
                    
                    // Expire the Entry...
                    entryModel.entryData.expires = new Date();
                    
                    // Apply the update...
                    _masas.masasDataMgr.update( entryModel.entryData );
                }
                
                _masas.refreshDataSources( true );
            }
        }
        
        public function createReasonContent( reason : String ) : String
        {
            var newContent : String;
            
            newContent = "Cancellation reason: " +
                ( ( reason.length > 0 ) ? reason : "No reason provided" ) +
                "\n\n----------\n\n";
            
            return newContent;
        }
        
    }
}