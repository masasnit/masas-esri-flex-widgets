package MASAS.events
{
	
	import flash.events.Event;
		
	public class HubChangedEvent extends Event
	{
		
		public static const MASAS_HUB_CHANGED:String = "masasHubChanged";
		
		public function HubChangedEvent( type : String )
		{
			super( type );
		}
		
		public override function clone():Event
		{
			return new HubChangedEvent( this.type );
		}
		
	}
	
}