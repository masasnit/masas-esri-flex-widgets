package MASAS.events
{
	
	import flash.events.Event;
		
	public class RefreshingFeedEvent extends Event
	{
		
		public static const MASAS_REFRESHING_FEED:String = "masasRefreshingFeed";
		
		public function RefreshingFeedEvent( type : String )
		{
			super( type );
		}
		
		public override function clone():Event
		{
			return new RefreshingFeedEvent( this.type );
		}
		
	}
	
}