package MASAS.events
{
	
	import flash.events.Event;
		
	public class RefreshedFeedEvent extends Event
	{
		
		public static const MASAS_REFRESHED_FEED:String = "masasRefreshedFeed";
		
		public function RefreshedFeedEvent( type : String )
		{
			super( type );
		}
		
		public override function clone():Event
		{
			return new RefreshedFeedEvent( this.type );
		}
		
	}
	
}