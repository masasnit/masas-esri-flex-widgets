package MASAS.events
{
	
	import flash.events.Event;
		
	public class SignedInEvent extends Event
	{
		
		public static const MASAS_SIGNED_IN:String = "masasSignedIn";
		
		public function SignedInEvent( type : String )
		{
			super( type );
		}
		
		public override function clone():Event
		{
			return new SignedInEvent( this.type );
		}
		
	}
	
}