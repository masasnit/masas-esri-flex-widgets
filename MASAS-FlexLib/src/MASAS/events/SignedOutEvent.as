package MASAS.events
{
	
	import flash.events.Event;
		
	public class SignedOutEvent extends Event
	{
		
		public static const MASAS_SIGNED_OUT:String = "masasSignedOut";
		
		public function SignedOutEvent( type : String )
		{
			super( type );
		}
		
		public override function clone():Event
		{
			return new SignedOutEvent( this.type );
		}
		
	}
	
}