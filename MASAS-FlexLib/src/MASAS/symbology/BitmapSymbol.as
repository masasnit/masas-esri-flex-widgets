package MASAS.symbology
{
    
    import com.esri.ags.Map;
    import com.esri.ags.geometry.Geometry;
    import com.esri.ags.geometry.MapPoint;
    import com.esri.ags.symbols.Symbol;
    
    import flash.display.Bitmap;
    import flash.display.BitmapData;
    import flash.display.Loader;
    import flash.display.LoaderInfo;
    import flash.display.Sprite;
    import flash.events.Event;
    import flash.events.IOErrorEvent;
    import flash.net.URLRequest;
    import flash.system.LoaderContext;
    import flash.utils.Dictionary;
    
    import mx.core.UIComponent;
    
    import spark.components.HGroup;
    import spark.components.Image;
    
    public class BitmapSymbol extends Symbol
    {
        
        [Embed(source="assets/images/error.png")]
        private static var ERROR_CLASS:Class;
        
        private static const bitmapDataDict : Dictionary = new Dictionary();	
        
        public var href : String;
        
        public function BitmapSymbol()
        {
            super();
        }
        
        override public function clear( sprite : Sprite ) : void
        {
            // clear and position the sprite
            sprite.graphics.clear();
            
            sprite.x = 0;
            sprite.y = 0;
        }
        
        override public function destroy( sprite : Sprite ) : void
        {
            sprite.graphics.clear();
        }
        
        override public function createSwatch( width : Number = 50, height : Number = 50, shape : String = null ) : UIComponent
        {
            var swatch  : UIComponent   = new UIComponent();
            var group   : HGroup        = new HGroup();
            var image   : Image         = new Image();
            
            image.source    = href;
            image.scaleMode = "letterbox";
            image.fillMode  = "scale";
            
            group.width             = width;
            group.height            = height;
            group.horizontalAlign   = "center";
            group.verticalCenter    = "middle";
            group.addElement( image );
            
            swatch.addChild( group );
            
            return swatch;
        }
        
        override public function draw( sprite : Sprite, geometry : Geometry, attributes : Object, map : Map ) : void
        {
            const mapPoint      : MapPoint      = geometry as MapPoint;
            const bitmapData    : BitmapData    = bitmapDataDict[href];
            
            sprite.x = toScreenX( map, mapPoint.x );
            sprite.y = toScreenY( map, mapPoint.y );
            
            if( bitmapData )
            {
                drawBitmap( sprite, bitmapData );
            }
            else
            {
                const urlRequest    : URLRequest    = new URLRequest( href );                
                const loaderContext : LoaderContext = new LoaderContext();
                const loader        : Loader        = new Loader();
                
                loaderContext.checkPolicyFile = true;
                
                loader.contentLoaderInfo.addEventListener( Event.COMPLETE, completeHandler );
                loader.contentLoaderInfo.addEventListener( IOErrorEvent.IO_ERROR, ioErrorHandler );
                loader.load( urlRequest, loaderContext );
                
                function completeHandler( event : Event ) : void
                {
                    const loaderInfo    : LoaderInfo    = event.target as LoaderInfo;
                    const content       : Bitmap        = loaderInfo.content as Bitmap;

                    bitmapDataDict[href] = content.bitmapData;
                    
                    drawBitmap( sprite, content.bitmapData );
                }
                
                function ioErrorHandler( event : IOErrorEvent ) : void
                {
                    const content : Bitmap = new ERROR_CLASS();
                    bitmapDataDict[href] = content.bitmapData;
                    
                    drawBitmap( sprite, content.bitmapData );
                }
                
            }
        }
        
        private function drawBitmap( sprite : Sprite, bitmapData : BitmapData ) : void
        {
            sprite.x -= ( bitmapData.width / 2.0 );
            sprite.y -= ( bitmapData.height / 2.0 );
            
            sprite.graphics.beginBitmapFill( bitmapData, null, false, true );
            sprite.graphics.drawRect( 0, 0, bitmapData.width, bitmapData.height );
            sprite.graphics.endFill();
        }
        
    }
    
}