package MASAS.symbology
{
    
    import com.esri.ags.geometry.Geometry;
    import com.esri.ags.symbols.CompositeSymbol;
    import com.esri.ags.symbols.PictureMarkerSymbol;
    import com.esri.ags.symbols.SimpleFillSymbol;
    import com.esri.ags.symbols.SimpleLineSymbol;
    import com.esri.ags.symbols.Symbol;
    import com.esri.ags.tools.DrawTool;
    
    import mx.collections.ArrayCollection;
    import mx.controls.Alert;
    import mx.rpc.events.FaultEvent;
    import mx.rpc.events.ResultEvent;
    import mx.rpc.http.HTTPService;
    
    import MASAS.MASAS;
    import MASAS.feedlib.masas.supportclasses.MasasEntryColourEnum;
    import MASAS.feedlib.masas.supportclasses.MasasSymbolSet;
    
    public class SymbologyManager
    {
        
        // Private Members...
        private var _masasMgr   : MASAS;            // MASAS Manager...
        private var _symbolSet  : MasasSymbolSet;   // Symbol Set...
        
        // Default Symbols...
        private var _useDefaultIcon     : Boolean = false;
        private var _defaultIconUrl     : String;
        
        // Fills...
        private var _entryFillSymbol    : Symbol;
        private var _alertFillSymbol    : Symbol;
        private var _capanFillSymbol    : Symbol;
        
        // Lines...
        private var _entryLineSymbol    : Symbol;
        private var _capanLineSymbol    : Symbol;
        
        private var _resultMarkerSymbol : Symbol;
        private var _rssMarkerSymbol    : PictureMarkerSymbol;
        
        
        // Public Accessors...
        
        public function get useDefaultIcon() : Boolean
        {
            return _useDefaultIcon;
        }
        
        public function get rssMarkerSymbol() : PictureMarkerSymbol
        {
            return _rssMarkerSymbol;
        }
        
        public function get resultMarkerSymbol() : Symbol
        {
            return _resultMarkerSymbol;
        }
        
        public function get entryLineSymbol() : Symbol
        {
            return _entryLineSymbol;
        }
        
        public function get capanLineSymbol() : Symbol
        {
            return _capanLineSymbol;
        }
        
        public function get entryFillSymbol() : Symbol
        {
            return _entryFillSymbol;
        }
        
        public function get alertFillSymbol() : Symbol
        {
            return _alertFillSymbol;
        }
        
        public function get capanFillSymbol() : Symbol
        {
            return _capanFillSymbol;
        }
        
        // Constructor...
        public function SymbologyManager( masasMgr : MASAS, configXML : XML )
        {
            _masasMgr = masasMgr;
            
            // Event Code URLs...
            var eventCodeUrl:String = configXML.symbologyManager.eventcodes.@url;  
            var eventCodeProxyUrl:String = configXML.symbologyManager.eventcodes.@proxy;
            
            // Default Symbology...
            createDefaultSymbols( configXML );
            
            // Load the event code file...
            var eventCodesService : HTTPService = new HTTPService();
            
            if( eventCodeUrl )
            {
                eventCodesService.resultFormat = "e4x";
                
                // Theoretically, the event code file could be on a different sever and need to be proxied
                if( eventCodeProxyUrl ) {
                    eventCodesService.url = eventCodeProxyUrl + "?" + eventCodeUrl;
                }		
                else {
                    eventCodesService.url = eventCodeUrl;
                }
                
                eventCodesService.addEventListener( FaultEvent.FAULT, eventCodesService_FaultHandler );
                eventCodesService.addEventListener( ResultEvent.RESULT, eventCodesService_ReadyHandler );
                eventCodesService.send();
            }
        }
        
        public function getSymbol( eventCode : String ) : Symbol
        {
            var defaultEventCode : String = eventCode;
            var symbol : Symbol = null;
            
            if( !defaultEventCode ) {
                defaultEventCode = "ems/other/other";		
            }
            
            symbol = _symbolSet.GetSymbol( defaultEventCode );
            if (!symbol)
            {
                // If symbol not found in symbol set, apply default
                var href:String = _defaultIconUrl; 
                var defaultSymbol : BitmapSymbol = new BitmapSymbol();
                
                defaultSymbol.href = href; 
                symbol = defaultSymbol as Symbol; 
            }
            
            if( _useDefaultIcon )
            {
                var symbolCollection:ArrayCollection = new ArrayCollection();
                
                var pictureMarkupSymbol:PictureMarkerSymbol = new PictureMarkerSymbol();
                pictureMarkupSymbol.source = _defaultIconUrl;
                
                symbolCollection.addItem(pictureMarkupSymbol);		
                symbolCollection.addItem(_rssMarkerSymbol);
                
                symbol = new CompositeSymbol(symbolCollection);
            }
            
            return symbol;
        }
        
        public function getSymbolHREF( eventCode : String ) : String
        {
            var href:String = _defaultIconUrl;
            var defaultEventCode : String = eventCode;
            var symbol : Symbol = null;
            
            if( !defaultEventCode ) {
                defaultEventCode = "ems/other/other";		
            }
            
            symbol = _symbolSet.GetSymbol( defaultEventCode );
            
            if( symbol && !_useDefaultIcon )
            {
                // If symbol not found in symbol set, apply default
                href = (symbol as BitmapSymbol).href;
            }
            
            return href;
        }
        
        public function getEntrySymbol( eventCode : String, colourCode : MasasEntryColourEnum = null ) : Symbol
        {
            var markerSymbol        : Symbol            = null;
            var symbolCollection    : ArrayCollection   = new ArrayCollection();
            var fullSymbol          : CompositeSymbol   = new CompositeSymbol();
            
            markerSymbol = getSymbol( eventCode );
            
            if( colourCode ) {
                var colourSymbol : ColourCodeSymbol = new ColourCodeSymbol( colourCode );
                symbolCollection.addItem( colourSymbol );    
            }
            
            symbolCollection.addItem( markerSymbol );
            
            fullSymbol.symbols = symbolCollection;
            
            return fullSymbol;
        }
        
        public function getEntryAreaSymbol( geometryType : String, colourCode : MasasEntryColourEnum = null ) : Symbol
        {
            var symbol : Symbol = null;
            
            if( colourCode )
            {
                switch( geometryType ) {
                    case Geometry.POLYLINE:
                    case DrawTool.POLYLINE:
                    {
                        symbol = new SimpleLineSymbol( SimpleLineSymbol.STYLE_SOLID, colourCode.toRGB(), 0.8, 2 );
                        break;
                    }
                    case Geometry.POLYGON:
                    case Geometry.EXTENT:
                    case DrawTool.POLYGON:
                    case DrawTool.EXTENT:
                    {
                        symbol = new SimpleFillSymbol( SimpleFillSymbol.STYLE_SOLID, colourCode.toRGB(), 0.25,
                                                        new SimpleLineSymbol( SimpleLineSymbol.STYLE_SOLID, colourCode.toRGB(), 0.8, 2 ) );
                        break;
                    }
                }
            }
            else
            {
                switch( geometryType ) {
                    case Geometry.POLYLINE:
                    case DrawTool.POLYLINE:
                    {						
                        symbol = _masasMgr.symbolManager.entryLineSymbol;
                        break;
                    }
                    case Geometry.POLYGON:
                    case Geometry.EXTENT:
                    case DrawTool.POLYGON:
                    case DrawTool.EXTENT:
                    {
                        symbol = _masasMgr.symbolManager.entryFillSymbol;
                        break;
                    }
                }
            }
            
            return symbol;
        }
        
        private function eventCodesService_ReadyHandler( event : ResultEvent ) : void
        {
            // Create a new symbol set based on the loaded symbology XML
            if (event.result is XML)
            {
                var symbologyXML : XML = XML( event.result );
                _symbolSet = new MasasSymbolSet( symbologyXML, _masasMgr.language ); 
            }
        }
        
        private function eventCodesService_FaultHandler( event : FaultEvent ) : void
        {
            var errorMsg:String = "Symbol File ERROR \n\n"
            errorMsg += "Event Target: " + event.target + "\n\n";
            errorMsg += "Event Type: " + event.type + "\n\n";
            errorMsg += "Fault Code: " + event.fault.faultCode + "\n\n";
            errorMsg += "Fault Info: " + event.fault.faultString;
            
            mx.controls.Alert.show( errorMsg, "Symbology Service" );
        }
        
        private function createDefaultSymbols( configXML : XML ):void 
        {
            _defaultIconUrl = configXML.symbologyManager.defaults.defaultIcon;
            _useDefaultIcon = ( configXML.symbologyManager.defaults.defaultIcon.@enable === "true" ) ? true : false;
            
            // marker symbol
            const resultMarkerSymbolURL:String=configXML.symbologyManager.defaults.picturemarkersymbol.@source || _defaultIconUrl;
            const resultMarkerSymbolHeight:Number=configXML.symbologyManager.defaults.picturemarkersymbol.@height || 30;
            const resultMarkerSymbolWidth:Number=configXML.symbologyManager.defaults.picturemarkersymbol.@width || 30;
            const resultMarkerSymbolXOffset:Number=configXML.symbologyManager.defaults.picturemarkersymbol.@xoffset || 0;
            const resultMarkerSymbolYOffset:Number=configXML.symbologyManager.defaults.picturemarkersymbol.@yoffset || 0;
            _resultMarkerSymbol = new PictureMarkerSymbol(resultMarkerSymbolURL, resultMarkerSymbolWidth, resultMarkerSymbolHeight, resultMarkerSymbolXOffset, resultMarkerSymbolYOffset);
            
            // rss symbol
            _rssMarkerSymbol = new PictureMarkerSymbol(_defaultIconUrl, 12, 12, 8, 8);
            
            // line symbol
            _entryLineSymbol = createSimpleLineSymbol( configXML.symbologyManager.defaults.entrylinesymbol[0], SimpleLineSymbol.STYLE_SOLID, 0xFF0000, 0.8, 2 );
            _capanLineSymbol = createSimpleLineSymbol( configXML.symbologyManager.defaults.capanlinesymbol[0], SimpleLineSymbol.STYLE_SOLID, 0xFF0000, 0.8, 2 );
            
            // fill symbols...			
            _entryFillSymbol = createSimpleFillSymbol( configXML.symbologyManager.defaults.entryfillsymbol[0], SimpleFillSymbol.STYLE_SOLID, 0xFF0000, 0.25, SimpleLineSymbol.STYLE_SOLID, 0xFF0000, 0.8, 2 );
            _alertFillSymbol = createSimpleFillSymbol( configXML.symbologyManager.defaults.alertfillsymbol[0], SimpleFillSymbol.STYLE_SOLID, 0xFF0000, 0.25, SimpleLineSymbol.STYLE_SOLID, 0xFF0000, 0.8, 2 );
            _capanFillSymbol = createSimpleFillSymbol( configXML.symbologyManager.defaults.capanfillsymbol[0], SimpleFillSymbol.STYLE_SOLID, 0xFF0000, 0.25, SimpleLineSymbol.STYLE_SOLID, 0xFF0000, 0.8, 2 );
        }
        
        private function createSimpleFillSymbol( fillSymbolXML : XML,
                                                 defaultStyle : String,
                                                 defaultColor : Number, defaultAlpha : Number,
                                                 defaultOutlineStyle : String,
                                                 defaultOutlineColor : Number, defaultOutlineAlpha : Number, 
                                                 defaultOutlineWidth : Number) : SimpleFillSymbol
        {
            var style           : String = defaultStyle;
            var color           : Number = defaultColor;
            var alpha           : Number = defaultAlpha;
            var outlineStyle    : String = defaultOutlineStyle;
            var outlineColor    : Number = defaultOutlineColor;
            var outlineAlpha    : Number = defaultOutlineAlpha;
            var outlineWidth    : Number = defaultOutlineWidth;
            
            if( fillSymbolXML != null )
            {
                style           = ( fillSymbolXML.@style != undefined ) ? fillSymbolXML.@style : defaultStyle;
                color           = ( fillSymbolXML.@color != undefined ) ? fillSymbolXML.@color : defaultColor; 
                alpha           = ( fillSymbolXML.@alpha != undefined ) ? fillSymbolXML.@alpha : defaultAlpha;
                outlineStyle    = ( fillSymbolXML.@outlineStyle != undefined ) ? fillSymbolXML.@outlineStyle : defaultOutlineStyle;
                outlineColor    = ( fillSymbolXML.@outlineColor != undefined ) ? fillSymbolXML.@outlineColor : defaultOutlineColor;
                outlineAlpha    = ( fillSymbolXML.@outlineAlpha != undefined ) ? fillSymbolXML.@outlineAlpha : defaultOutlineAlpha;
                outlineWidth    = ( fillSymbolXML.@outlineWidth != undefined ) ? fillSymbolXML.@outlineWidth : defaultOutlineWidth;
            }
            
            return new SimpleFillSymbol( style, color, alpha, new SimpleLineSymbol( outlineStyle, outlineColor, outlineAlpha, outlineWidth ) );
        }
        
        private function createSimpleLineSymbol( lineSymbolXML : XML, 
                                                 defaultStyle : String,
                                                 defaultColor : Number, defaultAlpha : Number, 
                                                 defaultWidth : Number) : SimpleLineSymbol
        {
            var style : String = defaultStyle;
            var color : Number = defaultColor;
            var alpha : Number = defaultAlpha;
            var width : Number = defaultWidth;
            
            if( lineSymbolXML != null )
            {
                style = ( lineSymbolXML.@style != undefined ) ? lineSymbolXML.@style : defaultStyle;
                color = ( lineSymbolXML.@color != undefined ) ? lineSymbolXML.@color : defaultColor; 
                alpha = ( lineSymbolXML.@alpha != undefined ) ? lineSymbolXML.@alpha : defaultAlpha;
                width = ( lineSymbolXML.@width != undefined ) ? lineSymbolXML.@width : defaultWidth;
            }
            
            return new SimpleLineSymbol( style, color, alpha, width );
        }
        
        
    }
    
}