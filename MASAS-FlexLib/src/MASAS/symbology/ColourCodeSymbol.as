package MASAS.symbology
{
    
    import com.esri.ags.Map;
    import com.esri.ags.geometry.Geometry;
    import com.esri.ags.geometry.MapPoint;
    import com.esri.ags.symbols.Symbol;
    
    import flash.display.GradientType;
    import flash.display.Graphics;
    import flash.display.InterpolationMethod;
    import flash.display.SpreadMethod;
    import flash.display.Sprite;
    import flash.geom.Matrix;
    
    import MASAS.feedlib.masas.supportclasses.MasasEntryColourEnum;
    
    public final class ColourCodeSymbol extends Symbol
    {
        
        // Private members...
        private var _fillColor : int = 0x0;
        
        // Constructor...
        public function ColourCodeSymbol( color : MasasEntryColourEnum )
        {
            super();
            
            _fillColor = color.toRGB();
        }
        
        // Public methods...
        
        override public function clear( sprite : Sprite ) : void
        {
            // clear and position the sprite
            sprite.graphics.clear();
            
            sprite.x = 0;
            sprite.y = 0;
        }
        
        override public function draw( sprite : Sprite, geometry : Geometry, attributes : Object, map : Map ) : void
        {
            if( geometry.type == Geometry.MAPPOINT )
            {
                // project the point on screen
                sprite.x = toScreenX( map, (geometry as MapPoint).x );
                sprite.y = toScreenY( map, (geometry as MapPoint).y );
                
                drawPoint( sprite.graphics, geometry as MapPoint, map );
            }
        }
        
        override public function destroy( sprite : Sprite ) : void
        {
            sprite.graphics.clear();
        }
        
        private function drawPoint( g : Graphics, mapPoint : MapPoint, map : Map ) : void
        {
            var matrix : Matrix = new Matrix();
            var radius : Number = 25;
            
            matrix.createGradientBox( radius * 2, radius * 2, 0, - radius, - radius);
            
            g.beginGradientFill( GradientType.RADIAL, 
                                 [_fillColor, _fillColor, _fillColor], /* Colors */ 
                                 [0.75, .5, 0], /* Alphas */ 
                                 [0, 192, 255], /* Ratios */ 
                                 matrix, SpreadMethod.PAD, InterpolationMethod.LINEAR_RGB, 0 );
            
            g.drawCircle( 0, 0, radius );
            g.endFill();
        }
        
    }
    
}