package MASAS.data
{
	
	import MASAS.data.HubSettingsDateParam;
	
	public class HubSettings
	{
		
		// Private members...
		
		private var _autoRefreshEnabled : Boolean = false;
		private var _autoRefreshRate : Number = 5;	// In minutes...
		private var _dtStart : Date;
		private var _dtEnd	: Date;
		private var _dtSince : Date;
		private var _useDateParameter : String = HubSettingsDateParam.NONE;
		
		// Public accessors...
		
		public function get autoRefreshEnabled() : Boolean
		{
			return _autoRefreshEnabled;
		}

		public function set autoRefreshEnabled( value : Boolean ) : void
		{
			_autoRefreshEnabled = value;
		}

		public function get autoRefreshRate() : Number
		{
			return _autoRefreshRate;
		}

		public function set autoRefreshRate( value : Number ) : void
		{
			_autoRefreshRate = value;
		}

		public function get dtStart() : Date
		{
			return _dtStart;
		}

		public function set dtStart( value : Date ) : void
		{
			_dtStart = value;
		}

		public function get dtEnd() : Date
		{
			return _dtEnd;
		}

		public function set dtEnd( value : Date ) : void
		{
			_dtEnd = value;
		}

		public function get dtSince() : Date
		{
			return _dtSince;
		}

		public function set dtSince( value : Date ) : void
		{
			_dtSince = value;
		}

		public function get useDateParameter() : String
		{
			return _useDateParameter;
		}

		public function set useDateParameter( value : String ) : void
		{
			switch( value )
			{
				case HubSettingsDateParam.RANGE:
					_useDateParameter = HubSettingsDateParam.RANGE;
					break;
				case HubSettingsDateParam.SINCE:
					_useDateParameter = HubSettingsDateParam.SINCE;
					break;
				default:
					_useDateParameter = HubSettingsDateParam.NONE;
					break;
			}
		}
		
		public function HubSettings()
		{
			// Setup the the default values...
			
			// Now...
			_dtEnd = new Date();
			
			// 14 days from now rounded to midnight...
			_dtStart = new Date();
			_dtStart.setDate( _dtStart.getDate() - 14 );
			_dtStart.setHours( 0, 0, 0, 0 );
			
			// 14 days from now rounded to midnight...
			_dtSince = new Date();
			_dtSince.setDate( _dtSince.getDate() - 14 );
			_dtSince.setHours( 0, 0, 0, 0 );
		}

	}
	
}