package MASAS.data
{
	
	public final class HubSettingsDateParam
	{

		public static const NONE:String		= "none";
		public static const RANGE:String	= "range";
		public static const SINCE:String	= "since";
		
	}
	
}