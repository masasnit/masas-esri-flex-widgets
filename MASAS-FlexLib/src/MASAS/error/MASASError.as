package MASAS.error
{
	
	public class MASASError extends Error
	{
		
		public static const MASAS_ERROR_HUBACCESS_URL : int = 100;
		
		public function MASASError( message : String, errorId : int = 0 )
		{
			super( message, errorId );
		}
		
	}
	
}