package MASAS.View
{
	
	import MASAS.model.EntryModel;
	
	public interface IMasasWidget
	{
		
		function get swfUrl() : String
		
		function get imagesUrl() : String
		
		function get assetsUrl() : String
		
		function zoomToEntry( entry : EntryModel ) : void
			
		function toggleCallout( entry : EntryModel ) : void

		function showCallout( entry : EntryModel = null ) : void
			
		function showDetails( entry : EntryModel ) : void
			
		function showEdit( entry : EntryModel ) : void
		
	}
}