package MASAS.View
{
	
	import MASAS.model.EntryModel;
	
	public interface IEntryDetailsWidget
	{
		
		function get entry() : EntryModel
		
		function set entry( value : EntryModel ) : void

	}
}