package MASAS.feedlib.feeds
{

	import flash.events.EventDispatcher;
	
	//public class BaseFeed extends EventDispatcher implements IBaseFeed
	public class BaseFeed extends EventDispatcher 
	{		
		[Event(name=FeedEvent.FEED_SUCCESS, type="com.esricanada.MASAS.feedlib.events.FeedEvent")]
		[Event(name=FeedEvent.FEED_ERROR_HTTP, type="com.esricanada.MASAS.feedlib.events.FeedEvent")]
		[Event(name=FeedEvent.FEED_ERROR_PARSE, type="com.esricanada.MASAS.feedlib.events.FeedEvent")]
		
		public function BaseFeed()
		{
			super();
		}
	}
}