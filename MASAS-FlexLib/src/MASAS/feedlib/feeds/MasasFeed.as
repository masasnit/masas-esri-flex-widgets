package MASAS.feedlib.feeds
{
    import com.adobe.utils.DateUtil;
    import com.esri.ags.geometry.Extent;
    import com.esri.ags.geometry.Geometry;
    import com.esri.ags.geometry.MapPoint;
    import com.esri.ags.geometry.Polygon;
    import com.esri.ags.geometry.Polyline;
    
    import flash.events.Event;
    import flash.events.HTTPStatusEvent;
    import flash.events.IOErrorEvent;
    import flash.events.TimerEvent;
    import flash.net.URLLoader;
    import flash.net.URLLoaderDataFormat;
    import flash.net.URLRequest;
    import flash.net.URLRequestHeader;
    import flash.net.URLRequestMethod;
    import flash.utils.Timer;
    
    import mx.collections.ArrayCollection;
    import mx.rpc.events.FaultEvent;
    import mx.rpc.events.ResultEvent;
    import mx.rpc.http.HTTPService;
    import mx.utils.Base64Encoder;
    
    import MASAS.MASAS;
    import MASAS.common.ProxyUrl;
    import MASAS.core.AuthenticationMethodEnum;
    import MASAS.data.HubSettings;
    import MASAS.events.RefreshedFeedEvent;
    import MASAS.events.RefreshingFeedEvent;
    import MASAS.feedlib.cap.Alert;
    import MASAS.feedlib.cap.enumerations.AlertInfoCategory;
    import MASAS.feedlib.cap.enumerations.AlertInfoSeverity;
    import MASAS.feedlib.events.FeedEvent;
    import MASAS.feedlib.events.PublishEvent;
    import MASAS.feedlib.masas.supportclasses.MasasAtomEntry;
    import MASAS.feedlib.masas.supportclasses.MasasEntryEnclosure;
    import MASAS.feedlib.utilities.ErrorWrapper;
    import MASAS.feedlib.utilities.GeoRSSUtil;
    import MASAS.feedlib.utilities.Namespaces;
    import MASAS.feedlib.utilities.ResourceHelper;
    import MASAS.feedlib.utilities.SupportedLocale;
    
    public class MasasFeed extends BaseFeed
    {
        private static const GEORSS:Namespace=Namespaces.GEORSS_NS
        private static const ATOM:Namespace=Namespaces.ATOM_NS;
        private static const XHTML:Namespace=Namespaces.XHTML_NS;
        private static const XMLNS:Namespace=new Namespace("http://www.w3.org/XML/1998/namespace");
        
        private var _lastFeedResult:ArrayCollection
        private var _lastPublishResult:MasasAtomEntry;
        
        private var _hubSettings : HubSettings;
        private var _masas : MASAS;
        
        private var _refreshTimer : Timer;
        
        //Note, the base class exposes events, too 
        [Event(name=CapEvent.CAP_SUCCESS, type="com.esricanada.MASAS.feedlib.events.CapEvent")]
        [Event(name=CapEvent.CAP_ERROR_HTTP, type="com.esricanada.MASAS.feedlib.events.CapEvent")]
        [Event(name=CapEvent.CAP_ERROR_PARSE, type="com.esricanada.MASAS.feedlib.events.CapEvent")]
        
        [Event(name=PublishEvent.PUBLISH_SUCCESS, type="com.esricanada.MASAS.feedlib.events.PublishEvent")]
        [Event(name=PublishEvent.PUBLISH_ERROR_HTTP, type="com.esricanada.MASAS.feedlib.events.PublishEvent")]
        [Event(name=PublishEvent.PUBLISH_ERROR_PARSE, type="com.esricanada.MASAS.feedlib.events.PublishEvent")]
        
        [Event(name=RefreshingFeedEvent.MASAS_REFRESHING_FEED, type="masas.events.RefreshingFeedEvent")]
        [Event(name=RefreshedFeedEvent.MASAS_REFRESHED_FEED, type="masas.events.RefreshedFeedEvent")]
        
        public function get hubSettings():HubSettings
        {
            return _hubSettings;
        }
        
        public function MasasFeed( hubSettings : HubSettings, masas : MASAS )
        {
            _masas = masas;
            
            _hubSettings = hubSettings;
            if( _hubSettings == null ) {
                _hubSettings = new HubSettings();
            }
            
        }
        
        public function start() : void
        {
            startRefreshTimer();
        }
        
        public function stop() : void
        {
            stopRefreshTimer();
        }
        
        public function update( masasEntry : MasasAtomEntry, capAlert : Alert = null, 
                                successCallback : Function = null, failureCallback : Function = null ) : void
        {
            try
            {
                //Always have to pass in a masasEntry for Atom or Cap update b/c is contains the 
                //linkEdit (Atom) and the linkEditMedia (CAP) properties
                
                //Check for null passed in 
                if( !masasEntry ) {
                    throw new Error( ResourceHelper.getErrorString( 1, ResourceHelper.getCurrentLocale, ["masasObject","MasasFeed::publish"] ) );
                }
                
                var updateCAP : Boolean = false;
                var proxyUrl : ProxyUrl = null;
                
                var url             : String;
                var publishString   : String;
                
                if( capAlert ) {
                    updateCAP = true;  //we have a CAP alert, we're updating CAP
                }  
                
                //Get the url - linkEdit (Atom), linkEditMedia (CAP) 
                if( updateCAP ) {
                    proxyUrl = new ProxyUrl( masasEntry.linkEditMedia, _masas.hubUrl.proxyUrl );
                }
                else {	
                    // update Atom
                    proxyUrl = new ProxyUrl( masasEntry.linkEdit, _masas.hubUrl.proxyUrl );
                }
                
                url = proxyUrl.url;
                if( _masas.authenticationMethod == AuthenticationMethodEnum.useDefault ) {
                    url += "?secret=" + _masas.token;	
                }
                
                var urlRequest : URLRequest = new URLRequest( url );
                
                //We're going to send content to the hub as a string ... 
                if( updateCAP )
                {
                    urlRequest.requestHeaders.push( new URLRequestHeader( "Content-Type","application/common-alerting-protocol+xml" ) );
                    publishString = getCapPublishString( capAlert, _masas.language );
                }
                else  //update Atom
                {
                    urlRequest.requestHeaders.push( new URLRequestHeader( "Content-Type", "application/atom+xml" ) );
                    publishString = getAtomPublishString( masasEntry, _masas.language );
                }
                
                //Note: If your firewall does not allow PUT, then do an HTTP POST and set the method
                //override header as follows: XHTTPMethodOverride:PUT
                urlRequest.requestHeaders.push(new URLRequestHeader("X-HTTP-Method-Override","PUT"));
                
                //We'll always use POST 
                urlRequest.method = URLRequestMethod.POST;
                urlRequest.data = publishString;
                
                postToHub( urlRequest, successCallback, failureCallback ); 
            }
            catch (e:Error)
            {
                //Unexpected error. 
                throw new ErrorWrapper(ResourceHelper.getErrorString(9, ResourceHelper.getCurrentLocale, [e.message.toString(), "MasasFeed::update"]));
            }
        }
        
        public function updateAttachments( masasEntry : MasasAtomEntry,
                                           successCallback : Function = null, failureCallback : Function = null ) : void
        {
            try
            {
                //Check for null passed in 
                if( !masasEntry ) {
                    throw new Error( ResourceHelper.getErrorString( 1, ResourceHelper.getCurrentLocale, ["masasObject","MasasFeed::publish"] ) );
                }
                
                if( masasEntry.linkEnclosures && masasEntry.linkEnclosures.length > 0 )
                {
                    var proxyUrl        : ProxyUrl  = null;
                    var url             : String    = "";
                    var publishValue    : *         = null;
                    
                    proxyUrl = new ProxyUrl( masasEntry.linkEdit, _masas.hubUrl.proxyUrl );
                    
                    url = proxyUrl.url + "/content";
                    if( _masas.authenticationMethod == AuthenticationMethodEnum.useDefault ) {
                        url += "?secret=" + _masas.token;	
                    }
                    
                    var urlRequest : URLRequest = new URLRequest( url );
                    
                    if( masasEntry.linkEnclosures.length == 1 )
                    {
                        publishValue = masasEntry.linkEnclosures[0].data;
                        urlRequest.requestHeaders.push( new URLRequestHeader( "Content-Type", masasEntry.linkEnclosures[0].type ) );
                        urlRequest.requestHeaders.push( new URLRequestHeader( "Slug", masasEntry.linkEnclosures[0].title ) );
                    }
                    else {
                        publishValue = encodeEntryWithAttachments( null, masasEntry.linkEnclosures );
                        urlRequest.requestHeaders.push( new URLRequestHeader( "Content-Type", "multipart/related; boundary=0.a.unique.value.0" ) );
                    }
                    
                    //Note: If your firewall does not allow PUT, then do an HTTP POST and set the method
                    //override header as follows: XHTTPMethodOverride:PUT
                    urlRequest.requestHeaders.push(new URLRequestHeader("X-HTTP-Method-Override","PUT"));
                    
                    //We'll always use POST 
                    urlRequest.method = URLRequestMethod.POST;
                    urlRequest.data = publishValue;
                    
                    postToHub( urlRequest, successCallback, failureCallback );
                }
                
            }
            catch (e:Error)
            {
                //Unexpected error. 
                throw new ErrorWrapper(ResourceHelper.getErrorString(9, ResourceHelper.getCurrentLocale, [e.message.toString(), "MasasFeed::update"]));
            }
        }
        
        //Pass in either MasasAtomEntry or MASAS.feedlib.cap.Alert
        public function publish( masasObject : Object, 
                                 successCallback : Function = null, failureCallback : Function = null ) : void
        {
            try
            {
                //Check for null passed in 
                if (!masasObject)
                    throw new Error(ResourceHelper.getErrorString(1, ResourceHelper.getCurrentLocale, ["masasObject","MasasFeed::publish"]));
                
                if ((!masasObject is Alert) && (!masasObject is MasasAtomEntry))
                    throw new Error(ResourceHelper.getErrorString(4, ResourceHelper.getCurrentLocale, ["masasObject","MasasFeed::publish"]));
                
                var publishString:String; 
                
                if (masasObject is MasasAtomEntry)
                {
                    var atomEntry : MasasAtomEntry = ( masasObject as MasasAtomEntry );
                    publishString = getAtomPublishString( atomEntry, _masas.language );
                    
                    if( atomEntry.linkEnclosures && atomEntry.linkEnclosures.length > 0 ) {
                        publishString = encodeEntryWithAttachments( publishString, atomEntry.linkEnclosures );
                        postNewEntry( publishString, 'multipart/related; boundary=0.a.unique.value.0' );
                    }
                    else {
                        postNewEntry( publishString, "application/atom+xml" );
                    }
                }
                else  //CAP Alert					
                {
                    publishString = getCapPublishString(masasObject as Alert, _masas.language );
                    postEntry( URLRequestMethod.POST, _masas.hubUrl.url + "/feed", publishString, "application/common-alerting-protocol+xml", 
                        successCallback, failureCallback );
                }
            }
            catch (e:Error)
            {
                //Unexpected error. 
                throw new ErrorWrapper(ResourceHelper.getErrorString(9, ResourceHelper.getCurrentLocale, [e.message.toString(), "MasasFeed::publish"]));
            }
        }
        
        private function postEntry( method : String, url : String, entryData : String, contentType : String,
                                    successCallback : Function = null, failureCallback : Function = null ) : void
        {
            var urlRequest : URLRequest;
            
            if( _masas.authenticationMethod == AuthenticationMethodEnum.useDefault ) {
                urlRequest = new URLRequest( url + "?secret=" + _masas.token );	
            }
            else {
                urlRequest = new URLRequest( url );	
            }
            
            urlRequest.method = method;
            
            urlRequest.requestHeaders.push( new URLRequestHeader( "Content-Type", contentType ) );
            urlRequest.data = entryData;
            postToHub( urlRequest, successCallback, failureCallback );
        }
        
        private function postNewEntry( entryData : String, contentType : String,
                                       successCallback : Function = null, failureCallback : Function = null ) : void
        {
            postEntry( URLRequestMethod.POST, _masas.hubUrl.url + "/feed", entryData, contentType,
                successCallback , failureCallback )
        }
        
        private function encodeEntryWithAttachments( entryData : String, attachments : Vector.<MasasEntryEnclosure> ) : String
        {
            var b64enc : Base64Encoder = new Base64Encoder();
            
            var newData : String = '';
            
            if( entryData && entryData.length > 0 )
            {
                newData  = '--0.a.unique.value.0\r\n';
                newData += 'Content-Disposition: attachment; name="entry"; filename="entry.xml"\r\n';
                newData += 'Content-Type: application/atom+xml\r\n';
                newData += '\r\n';
                newData += entryData;
                newData += '\r\n';
            }
            
            for( var i : Number = 0; i < attachments.length; i++ )
            {
                b64enc.encodeBytes( attachments[i].data, 0, attachments[i].length );
                
                newData += '--0.a.unique.value.0\r\n';
                newData += 'Content-Disposition: attachment; name="attachment"; filename="' + attachments[i].title + '"\r\n';
                newData += 'Content-Type: ' + attachments[i].type + '\r\n';
                //newData += 'Content-Description: ' + attachments[i].description + '\r\n';
                newData += 'Content-Transfer-Encoding: base64\r\n';
                newData += '\r\n';
                newData += b64enc.toString();
                newData += '\r\n';
            }
            newData += '--0.a.unique.value.0--\r\n';
            
            return newData;
        }
        
        private function postToHub( urlRequest : URLRequest, successCallback : Function = null, failureCallback : Function = null ) : void
        {
            try
            {
                //Set up a URLLoader and send the request ... 
                var urlLoader:URLLoader = new URLLoader();
                urlLoader.dataFormat = URLLoaderDataFormat.TEXT; 	
                urlLoader.addEventListener(Event.COMPLETE, postSuccessHandler);
                urlLoader.addEventListener(IOErrorEvent.IO_ERROR, postFailHandler);
                urlLoader.addEventListener(HTTPStatusEvent.HTTP_STATUS, httpStatusHandler);
                urlLoader.load(urlRequest);   
                
            }
            catch (e:Error)
            {
                //Unexpected error. 
                throw new Error(ResourceHelper.getErrorString(9, ResourceHelper.getCurrentLocale, [e.message.toString(), "MasasFeed::postToHub"]));
            }
            
            
            function postSuccessHandler( event : Event ) : void
            {
                //If everything was good, we will get back an XML object we can parse. 
                
                //Try to create a JSON object of the result. If successful, a proxied HTTP request
                //to the Hub failed and the proxy returned us http 200, with a JSON payload. 
                var proxyFailure : Object = null; 
                try {
                    proxyFailure = JSON.parse( urlLoader.data.toString() ); 
                }
                catch( e : Error ) {
                    //Do nothing. The Hub returned us back good xml. 
                }
                
                if( proxyFailure ) 
                {
                    // Dispatch an HTTP error and return the object representation of the JSON 
                    if( failureCallback != null ) {
                        failureCallback( new PublishEvent( PublishEvent.PUBLISH_ERROR_HTTP, proxyFailure ) );
                    }
                    else {
                        // For now, dispatch an event since no callback was provided...
                        dispatchEvent( new PublishEvent( PublishEvent.PUBLISH_ERROR_HTTP, proxyFailure ) );
                    }
                }
                else 
                {
                    try
                    {					
                        //Regardless of what we published (atome entry or cap alert), the hub sends us back an atom entry 
                        //Try to convert to MasasAtomEntry. If this fails, then we did not get back a MasasAtomEntry XML
                        //and need to see if was an error passed back by the proxy. 
                        var x:XML=XML(urlLoader.data);
                        var entry:MasasAtomEntry = MasasAtomEntry.fromXML( x, _masas.language ); 		
                        
                        _lastPublishResult = entry ; 
                        
                        //Dispatch the 'publish success' event 
                        if( successCallback != null ) {
                            successCallback( new PublishEvent( PublishEvent.PUBLISH_SUCCESS, entry ) );
                        }
                        else {
                            // For now, dispatch an event since no callback was provided...
                            dispatchEvent( new PublishEvent( PublishEvent.PUBLISH_SUCCESS, entry ) );
                        }
                        
                    }
                    catch( objError : Error )
                    {
                        _lastPublishResult = null; 
                        var s : String = PublishEvent.PUBLISH_ERROR_PARSE.toString() + ": " + objError.message.toString();
                        
                        if( failureCallback != null ) {
                            failureCallback( new PublishEvent( PublishEvent.PUBLISH_ERROR_PARSE, s ) );
                        }
                        else {
                            // For now, dispatch an event since no callback was provided...
                            dispatchEvent( new PublishEvent( PublishEvent.PUBLISH_ERROR_PARSE,  s ) );
                        }
                    }				
                }
            }
            
            function postFailHandler(event:IOErrorEvent):void 
            {
                var errorStr : String = "";
                errorStr = event.text;
                if( event.currentTarget && event.currentTarget.data )
                {
                    errorStr += "\n";
                    errorStr = errorStr + event.currentTarget.data;	
                }
                
                if( failureCallback != null ) {
                    failureCallback( new PublishEvent( PublishEvent.PUBLISH_ERROR_HTTP.toString(), errorStr ) );
                }
                else {
                    // For now, dispatch an event since no callback was provided...
                    dispatchEvent( new PublishEvent( PublishEvent.PUBLISH_ERROR_HTTP.toString(), errorStr ) );
                }
            }
            
            function httpStatusHandler(event:HTTPStatusEvent):void 
            {
                //Do nothing
            }
            
        }
        
        private function getAtomPublishString(atomEntry:MasasAtomEntry, lang:SupportedLocale):String
        {	
            /* The Atom content that is being posted to the Hub must not provide the <id>, <author>,
            <published>, <updated>, or <edited> elements. According to the Atom entry format
            defined in Hub Format, the other required elements such as <title>, <content>, <point>,
            <expires>, and <category> must be present. XHTML, with all associated languages, for
            <title> and <content> must be provided. The Atom content may provide <link> elements
            of rel=”related” only. If there are processing errors with the Atom content, an error will be
            returned.*/		
            
            //NOTE: Even though the following creates xml-like string, you cannot use it for 
            //MasasAtomEntry.fromXML, because it does not contain some fields that are required
            //to be a valid MasasAtomEntry (e.g. id, author, etc). 
            
            try
            {
                const SCHEME_CAP_STATUS:String='masas:category:status'
                const SCHEME_CAP_SEVERITY:String='masas:category:severity'
                const SCHEME_CAP_ICON:String='masas:category:icon'
                const SCHEME_CAP_CATEGORY:String ='masas:category:category'
                
                const LANG_EN:String ="xml:lang='en'"; 
                const LANG_FR:String ="xml:lang='fr'";
                
                var atomString:String; 			
                var status:String = atomEntry.category.status.toString(); 
                var icon:String = atomEntry.eventCode; 
                
                // TODO: FIX THIS - should be taken care elsewhere
                if( icon == "other/other" ) {
                    icon = "other"
                }
                
                var i:int; 
                var severityArray:Array = new Array(); 
                var severityVector:Vector.<AlertInfoSeverity> = atomEntry.category.severity; 
                for (i=0; i < severityVector.length; i++)
                {
                    var severity:AlertInfoSeverity = severityVector[i]; 
                    severityArray.push(severity.toString()); 
                }
                var categoryArray:Array = new Array(); 
                var categoryVector:Vector.<AlertInfoCategory> = atomEntry.category.category; 
                for (i=0; i < categoryVector.length; i++)
                {
                    var category:AlertInfoCategory = categoryVector[i]; 
                    categoryArray.push(category.toString()); 
                }	
                var relatedLinkArray:Array = new Array();
                if (atomEntry.linkRelated)
                {
                    var relatedLinkVector:Vector.<String> = atomEntry.linkRelated;
                    for (i=0; i < relatedLinkVector.length; i++)
                    {
                        var relatedLink:String = relatedLinkVector[i]; 
                        relatedLinkArray.push(relatedLink);
                    }	
                }
                var geom:Geometry = atomEntry.geometry; 
                var geoString:String;
                switch (geom.type)
                {
                    case Geometry.MAPPOINT:
                    {
                        var pt:MapPoint = geom as MapPoint;
                        geoString = pt.y.toString() + " " + pt.x.toString(); //A point contains a single latitude-longitude pair, separated by whitespace. 
                        break;
                    }
                    case Geometry.POLYLINE:
                    {
                        var polyline:Polyline = geom as Polyline;
                        geoString = GeoRSSUtil.toString(polyline);
                        break;
                    }
                    case Geometry.POLYGON:
                    {
                        var polygon:Polygon = geom as Polygon;
                        geoString = GeoRSSUtil.toString(polygon);
                        break;
                    }
                    case Geometry.EXTENT:
                    {
                        var extent:Extent = geom as Extent;
                        geoString = GeoRSSUtil.toString(extent);
                        break;
                    }
                    default:
                        //Throw an error. Not supported by hub yet
                        throw new Error(ResourceHelper.getErrorString(1, ResourceHelper.getCurrentLocale, [geom.type, "MasasFeed::getAtomPublishString"]));
                }
                
                
                //Build the xml-like string one piece at a time.
                //atomString = "<?xml version='1.0' encoding='UTF-8'?>"; 
                atomString = "<entry xmlns='" + Namespaces.ATOM_NS.toString() + "'>";
                atomString += "<category label='Status' scheme='" + SCHEME_CAP_STATUS + "' term='" + status + "' />";
                for (i=0; i < severityArray.length; i++)
                {
                    atomString += "<category label='Severity' scheme='" + SCHEME_CAP_SEVERITY + "' term='" + severityArray[i] + "' />";
                }
                atomString += "<category label='Icon' scheme='" + SCHEME_CAP_ICON + "' term='" + icon + "' />";
                for (i=0; i < categoryArray.length; i++)
                {
                    atomString += "<category label='Category' scheme='" + SCHEME_CAP_CATEGORY + "' term='" + categoryArray[i] + "' />";
                }
                
                if( atomEntry.category.colourCode ) {
                    atomString += atomEntry.category.colourCode.toXMLString();
                }
                
                atomString += "<content type='xhtml'>";
                atomString += "<div xmlns='" + Namespaces.XHTML_NS.toString() + "'>";
                if (lang.toString() == SupportedLocale.fr_CA.toString())
                    atomString += "<div xml:lang='fr'>" + atomEntry.content  + "</div>";
                else 
                    atomString += "<div xml:lang='en'>" + atomEntry.content  + "</div>";
                atomString += "</div>";
                atomString += "</content>";			
                for(i=0; i < relatedLinkArray.length; i++)
                {
                    atomString += "<link href='" + relatedLinkArray[i] + "' rel='related'></link>";
                }
                atomString += "<title type='xhtml'>"
                atomString += "<div xmlns='" + Namespaces.XHTML_NS.toString() + "'>";
                if (lang.toString() == SupportedLocale.fr_CA.toString())
                    atomString += "<div xml:lang='fr'>" + atomEntry.title  + "</div>";
                else 
                    atomString += "<div xml:lang='en'>" + atomEntry.title  + "</div>";			
                atomString += "</div>";
                atomString += "</title>";
                
                switch (geom.type)
                {
                    case Geometry.MAPPOINT:
                        atomString += "<point xmlns='" + Namespaces.GEORSS_NS.toString() +  "'>" + geoString + "</point>";
                        break;
                    case Geometry.POLYLINE:
                        atomString += "<line xmlns='" + Namespaces.GEORSS_NS.toString() +  "'>" + geoString + "</line>";
                        break;
                    case Geometry.POLYGON:
                        atomString += "<polygon xmlns='" + Namespaces.GEORSS_NS.toString() +  "'>" + geoString + "</polygon>";
                        break;
                    case Geometry.EXTENT:
                        atomString += "<box xmlns='" + Namespaces.GEORSS_NS.toString() +  "'>" + geoString + "</box>";
                        break;
                    default:
                        //Unexpected error. 
                        throw new Error(ResourceHelper.getErrorString(1, ResourceHelper.getCurrentLocale, [geom.type, "MasasFeed::getAtomPublishString"]));
                }
                
                var utc:String=utc = DateUtil.toW3CDTF(atomEntry.expires).replace("-00:00", "Z").replace("+00:00", "Z");; 
                atomString += "<expires xmlns='" + Namespaces.ATOM_PUB_DATE.toString() + "'>" + utc + "</expires>";
                
                if( atomEntry.effective ) {
                    utc = DateUtil.toW3CDTF( atomEntry.effective ).replace( "-00:00", "Z" ).replace( "+00:00", "Z" );
                    atomString += "<effective xmlns='" + Namespaces.MET_NS.toString() + "'>" + utc + "</effective>";
                }
                
                // Permissions: Update
                if( atomEntry.permissionAllowUpdatesByUsers.length > 0 )
                {
                    atomString += "<control xmlns='http://www.w3.org/2007/app'>";
                    atomString += "<update xmlns='masas:extension:control'>";
                    for( i = 0; i < atomEntry.permissionAllowUpdatesByUsers.length; i++ )
                    {
                        if( i > 0 ) {
                            atomString += " ";
                        }
                        
                        atomString += atomEntry.permissionAllowUpdatesByUsers[i];
                    }
                    atomString += "</update>";
                    atomString += "</control>";
                }
                
                atomString += "</entry>" ;
                atomString.replace("'", "\"");
            }
            catch (e:Error)
            {
                //Unexpected error. 
                throw new ErrorWrapper(ResourceHelper.getErrorString(9, ResourceHelper.getCurrentLocale, [e.message.toString(), "MasasFeed::getAtomPublishString"]));
            }
            
            return atomString; 
        }
        
        private function getCapPublishString(alert:Alert, lang:SupportedLocale):String
        {
            try
            {
                var capString:String = alert.toXMLString();		
            }
            catch (e:Error)
            {
                //Unexpected error. 
                throw new ErrorWrapper(ResourceHelper.getErrorString(9, ResourceHelper.getCurrentLocale, [e.message.toString(), "MasasFeed::getCapPublishString"]));
            }
            
            return capString; 
        }
        
        public function requestAtomFeed( successCallback : Function, failureCallback : Function ) : void
        {		
            if( _masas.isConnected )
            {
                // We need to be properly signed in
                
                try
                {
                    var url : String = _masas.hubUrl.url  + "/feed";
                    if( _masas.authenticationMethod == AuthenticationMethodEnum.useDefault ) {
                        url += "?secret=" + _masas.token;	
                    }
                    
                    //We're going to always send a request for French or English (never both)
                    if( _masas.language == SupportedLocale.fr_CA ) {  
                        url += "&lang=fr" 
                    }
                    else { 
                        url += "&lang=en"
                    }
                    
                    //For the rest of these, it's up to the user to set them in the config file or at runtime
                    //				if ((masasConfig.author) && (masasConfig.author.length > 0))  
                    //					url+="&author="+ masasConfig.author; 
                    //				
                    //				url+="&limit=" + masasConfig.maxEntries.toString(); 
                    //				
                    //				if (masasConfig.boundingBox)
                    //				{
                    //					var bbox:Extent = masasConfig.boundingBox; 
                    //					url+="&bbox=" + bbox.xmin.toString() + "," + bbox.ymin.toString() + "," + bbox.xmax.toString() + "," + bbox.ymax.toString(); 
                    //				}
                    //				
                    switch( _hubSettings.useDateParameter )
                    {
                        case "range":
                            var strStart : String = DateUtil.toW3CDTF( _hubSettings.dtStart );
                            var strEnd : String = DateUtil.toW3CDTF( _hubSettings.dtEnd );
                            strStart = strStart.replace("-00:00", "Z").replace("+00:00", "Z");;
                            strEnd = strEnd.replace("-00:00", "Z").replace("+00:00", "Z");;
                            
                            url += "&dtstart=" + strStart + "&dtend=" + strEnd;
                            break;
                        case "since":
                            var strSince : String = DateUtil.toW3CDTF( _hubSettings.dtSince );
                            strSince = strSince.replace("-00:00", "Z").replace("+00:00", "Z");;
                            
                            url += "&dtsince=" + strSince;
                            break;
                    }
                    
                    // Add a unique identifier to the end of the URL
                    // IE has caching problems with GETs, adding the unique identifier solves this.
                    var curDate : Date = new Date();
                    url += "&_dc=" + curDate.getTime().toString();
                    
                    dispatchEvent( new RefreshingFeedEvent( RefreshingFeedEvent.MASAS_REFRESHING_FEED ) );
                    
                    var feedService : HTTPService = new HTTPService();
                    feedService.addEventListener( ResultEvent.RESULT, feedSuccessHandler );
                    feedService.addEventListener( FaultEvent.FAULT, feedFaultHandler ); //should not happen if user is using proxy.ashx  
                    feedService.url = url ;
                    feedService.method = "GET";
                    feedService.resultFormat = "e4x";
                    feedService.send();		
                }
                catch( e : Error )
                {
                    //Unexpected error. 
                    throw new ErrorWrapper( ResourceHelper.getErrorString( 9, ResourceHelper.getCurrentLocale, [e.message.toString(), "MasasFeed::requestAtomFeed"] ) );
                }
            }
            
            function feedSuccessHandler(event:ResultEvent):void
            {
                //If everything was good, we will get back an XML object we can parse. 
                
                //Try to create a JSON object of the result. If successful, a proxied HTTP request
                //to the Hub failed and the proxy returned us http 200, with a JSON payload. 
                var proxyFailure:Object = null; 
                try
                {
                    proxyFailure = JSON.parse(event.result.toString()); 
                }
                catch (e:Error)
                {
                    //Do nothing. The Hub returned us back good xml. 
                }
                
                if (proxyFailure)
                {
                    handleFeedFailure( s, failureCallback );
                }
                else 
                {
                    try
                    {									
                        //create an array collection of MasasAtomEntry
                        var x:XML=XML(event.result);
                        var ac:ArrayCollection=new ArrayCollection();
                        
                        for each (var entryXML:XML in x.ATOM::entry)
                        {
                            var atomEntry:MasasAtomEntry;
                            try
                            {
                                atomEntry = MasasAtomEntry.fromXML( entryXML, _masas.language );
                            }
                            // Unable to create an entry from the XML.  This can occur if the XML does not conform
                            // to CAP or CAP-CP standards...
                            catch (e:Error)
                            {
                                atomEntry = null;
                                //TODO: Add logic to handle rejected XML
                            }
                            
                            // If we didn't create a valid MasasAtomEntry, then don't add it...
                            if (atomEntry)
                            {
                                ac.addItem(atomEntry);	
                            }					
                        }
                        
                        //Keep track of the result in case someone wants it without going to the hub to get it
                        _lastFeedResult = ac; 
                        
                        //Dispatch the 'feed ready' event 
                        dispatchEvent(new FeedEvent(FeedEvent.FEED_SUCCESS, ac));
                        
                        // Dispatch the Feed Refreshed event...
                        dispatchEvent( new RefreshedFeedEvent( RefreshedFeedEvent.MASAS_REFRESHED_FEED ) );
                        
                        // Call the success callback...
                        if( successCallback != null ) {
                            successCallback();
                        }
                    }
                    catch (objError:Error)
                    {
                        var s:String = "An error occured parsing the feed: " + objError.message.toString();
                        handleFeedFailure( s, failureCallback );
                    }				
                }	
            }
            
            function feedFaultHandler(event:mx.rpc.events.FaultEvent):void
            {
                handleFeedFailure( event.message.toString(), failureCallback );
            }
            
        }
        
        private function handleFeedFailure( message : String, failureCallback : Function ) : void
        {
            if( failureCallback != null ) {
                failureCallback( message );
            }
            else {
                dispatchEvent( new FeedEvent( FeedEvent.FEED_ERROR_HTTP, message ) );
            }
            
            dispatchEvent( new RefreshedFeedEvent( RefreshedFeedEvent.MASAS_REFRESHED_FEED ) );
        }
        
        public function get lastFeedResult():ArrayCollection
        {
            return _lastFeedResult; 
        }
        
        private function startRefreshTimer() : void
        {
            if( _hubSettings.autoRefreshEnabled )
            {
                var rate : Number = _hubSettings.autoRefreshRate * 60000;
                
                // Create a new timer...
                if( !_refreshTimer )
                {
                    _refreshTimer = new Timer( rate );
                    _refreshTimer.addEventListener(TimerEvent.TIMER, refreshTimer_TickHandler);
                }
                else
                {
                    // Stop the timer if needed...
                    stopRefreshTimer();
                    
                    // Reset the timer...
                    _refreshTimer.reset();
                }
                
                _refreshTimer.delay = rate;
                _refreshTimer.start();
            }
        }
        
        private function stopRefreshTimer() : void
        {
            if( _refreshTimer != null && _refreshTimer.running )
            {
                _refreshTimer.stop();
            }
        }
        
        private function refreshTimer_TickHandler( event : TimerEvent ) : void
        {
            requestAtomFeed( null, null );
        }
        
    }
}