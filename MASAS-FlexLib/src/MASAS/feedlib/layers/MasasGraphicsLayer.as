package MASAS.feedlib.layers
{
	import com.esri.ags.Graphic;
	import com.esri.ags.layers.GraphicsLayer;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IList;
	import mx.utils.NameUtil;
	
	import MASAS.feedlib.graphics.AtomGraphic;
	import MASAS.feedlib.graphics.CapGraphic;
	import MASAS.feedlib.graphics.CapanGraphic;
	import MASAS.model.EntryModel;
	
	public class MasasGraphicsLayer extends FeedGraphicsLayer
	{	
		public function MasasGraphicsLayer(layerName:String)
		{
			super(layerName); //throws an error if null is passed
		}
		
		override public function add(graphic:Graphic):String
		{
			//Give a unique name  
			graphic.id=NameUtil.createUniqueName(graphic);
			
			if( !(graphic is AtomGraphic) && !(graphic is CapGraphic) && !(graphic is CapanGraphic) ) {
				//This type of layer only supports AtomGraphic and CapGraphic
				throw new Error("Invalid graphic type. AtomGraphic/CapGraphic only."); //should localize this string
			}
			
			super.add(graphic); 
			dispatchEvent( new Event( "graphicsLayerChange" ) );
			
			//Return the unique name 
			return graphic.id;
		}
		
		override public function remove(graphic:Graphic):void
		{		
			if( !(graphic is AtomGraphic) && !(graphic is CapGraphic) && !(graphic is CapanGraphic) ) {
				//This type of layer only supports AtomGraphic and CapGraphic
				throw new Error("Invalid graphic type. AtomGraphic/CapGraphic only."); //should localize this string
			}
			
			//Remove the graphic from the graphics collection 
			super.remove(graphic); 
			dispatchEvent( new Event( "graphicsLayerChange" ) );
		}
		
		override public function clear():void
		{
			super.clear();
			
			dispatchEvent( new Event( "graphicsLayerChange" ) );
		}
		
		// Returns the number of AtomGraphic objects in the graphics layer
		[Bindable(event="graphicsLayerChange")]
		public function get numAtomGraphics():int
		{
			return numGraphicsOfType(AtomGraphic);
		}
		
		// Returns the number of CapGraphic objects in the graphics layer
		[Bindable(event="graphicsLayerChange")]
		public function get numCapGraphics():int
		{
			return numGraphicsOfType(CapGraphic);
		}
		
		// Returns the number of CapanGraphic objects in the graphics layer
		[Bindable(event="graphicsLayerChange")]
		public function get numCapanGraphics():int
		{
			return numGraphicsOfType(CapanGraphic);
		}
		
		public function clearAtomGraphics():void
		{
			clearGraphics(AtomGraphic);
		}
		
		public function clearCapGraphics( entry : EntryModel = null ) : void
		{
			if( entry ) {
				clearEntryGraphics( CapGraphic, entry );
			}
			else {
				clearGraphics(CapGraphic);
			}
		}
		
		public function clearCapanGraphics( entry : EntryModel = null ) : void
		{
			if( entry ) {
				clearEntryGraphics( CapanGraphic, entry );
			}
			else {
				clearGraphics(CapanGraphic);
			}
		}
		
		// Returns the number of graphics of a given type from the graphics layer
		private function numGraphicsOfType(classType:Class):int
		{
			var returnNum:int = 0;
			if (this.numGraphics > 0)
			{
				var ac:ArrayCollection = this.graphicProvider as ArrayCollection; 
				if (ac)
				{
					var arr:Array = ac.toArray(); 
					for (var i:int=0; i < arr.length; i++)
					{
						if (arr[i] is classType)
						{
							returnNum++;
						}				
					}
				}
			}
			return returnNum;
		}
		
		public function clearAllGraphics() : void
		{			
			(graphicProvider as ArrayCollection).list.removeAll();
			dispatchEvent( new Event( "graphicsLayerChange" ) );
		}
		
		// Removes the graphics of a given type from the graphics layer
		private function clearGraphics(classType:Class):void
		{
			if (this.numGraphics > 0)
			{
				var ac:ArrayCollection = this.graphicProvider as ArrayCollection; 
				if (!ac) return; 
				var arr:Array = ac.toArray(); 
				for (var i:int=0; i < arr.length; i++)
				{
					if (arr[i] is classType)
					{
						this.remove(arr[i])  
						dispatchEvent( new Event( "graphicsLayerChange" ) );
					}			
				}
			}
		}
		
		private function clearEntryGraphics( classType : Class, entry : EntryModel ) : void
		{
			if( numGraphics > 0 )
			{
				var ac : ArrayCollection = graphicProvider as ArrayCollection; 
				if( !ac ) {
					return;
				}
				
				var arr : Array = ac.toArray(); 
				for( var i : int = 0; i < arr.length; i++ )
				{
					if( arr[i] is classType )
					{
						if( (arr[i] as classType).entry == entry )
						{
							this.remove(arr[i])  
							dispatchEvent( new Event( "graphicsLayerChange" ) );
							//return;
						}
					}			
				}
			}
		}
	}
}