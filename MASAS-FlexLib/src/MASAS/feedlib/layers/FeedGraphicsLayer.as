package MASAS.feedlib.layers
{
	import com.esri.ags.layers.GraphicsLayer;
	
	//There is nothing special about FeedGraphicsLayer. It is just used for type checking.
	public class FeedGraphicsLayer extends GraphicsLayer
	{
		public function FeedGraphicsLayer(layerName:String)
		{
			super();
			
			if (layerName)
			{
				super.name=layerName;
			}
			else
			{
				throw new Error("Invalid parameter: layerName"); //should be localized 
			}  		
		}
	}
}