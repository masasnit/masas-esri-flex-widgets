package MASAS.feedlib.graphics
{
	//There is nothing special about an AtomGraphic. It is just used for type checking.
	import com.esri.ags.Graphic;
	import com.esri.ags.geometry.Geometry;
	import com.esri.ags.symbols.Symbol;
	
	import MASAS.model.EntryModel;
	
	public class AtomGraphic extends com.esri.ags.Graphic
	{
		
		private var _entryModel : EntryModel;

		public function get entryModel() : EntryModel
		{
			return _entryModel;
		}

		public function AtomGraphic( entryModel : EntryModel, geometry : Geometry = null, symbol : Symbol = null, attributes : Object = null )
		{
			super( geometry, symbol, attributes ); 
			
			_entryModel = entryModel;
		}
	}
}