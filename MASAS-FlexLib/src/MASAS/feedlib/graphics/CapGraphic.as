package MASAS.feedlib.graphics
{
	
	//There is nothing special about a CapGraphic. It is just used for type checking.
	import com.esri.ags.Graphic;
	import com.esri.ags.geometry.Geometry;
	import com.esri.ags.symbols.Symbol;
	
	import MASAS.model.EntryModel;
	
	public class CapGraphic extends com.esri.ags.Graphic
	{
		
		private var _entry : EntryModel;
		
		public function get entry() : EntryModel
		{
			return _entry;
		}
		
		public function CapGraphic( entry : EntryModel = null, geometry : Geometry = null, symbol : Symbol = null, attributes : Object = null )
		{
			super(geometry, symbol, attributes);
			
			_entry = entry;
		}
		
	}
	
}