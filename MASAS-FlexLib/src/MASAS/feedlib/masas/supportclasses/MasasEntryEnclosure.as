package MASAS.feedlib.masas.supportclasses
{
	import flash.utils.ByteArray;
	
	public class MasasEntryEnclosure
	{
		
		private var _href	: String;
		private var _type	: String; 
		private var _length	: Number; 
		private var _title	: String;
		private var _data	: ByteArray = null;
		
		public function MasasEntryEnclosure( href : String = "", type : String = "", length : Number = 0, title : String = "" )
		{
			_href	= href;
			_type	= type; 
			_length	= length; 
			_title	= title;
		}
		
		public function get data():ByteArray
		{
			return _data;
		}

		public function set data(value:ByteArray):void
		{
			_data = value;
		}

		public function get title():String
		{
			return _title;
		}

		public function set title(value:String):void
		{
			_title = value;
		}

		public function get length():Number
		{
			return _length;
		}

		public function set length(value:Number):void
		{
			_length = value;
		}

		public function get type():String
		{
			return _type;
		}

		public function set type(value:String):void
		{
			_type = value;
		}

		public function get href():String
		{
			return _href;
		}

		public function set href(value:String):void
		{
			_href = value;
		}

	}
	
}