package MASAS.feedlib.masas.supportclasses
{
    import MASAS.feedlib.cap.enumerations.AlertInfoCategory;
    import MASAS.feedlib.cap.enumerations.AlertInfoSeverity;
    import MASAS.feedlib.cap.enumerations.AlertStatus;
    
    [Bindable]
    public class MasasEntryCategory
    {
        
        // Private members...
        private var _category   : Vector.<AlertInfoCategory> = new Vector.<AlertInfoCategory>();
        private var _status     : AlertStatus;
        private var _severity   : Vector.<AlertInfoSeverity> = new Vector.<AlertInfoSeverity>();
        private var _eventCode  : String;
        private var _colourCode : MasasEntryColourCode;
        
        // Accessors...
        
        public function get category() : Vector.<AlertInfoCategory>
        {
            return _category; 
        }
        
        public function set category( value : Vector.<AlertInfoCategory> ) : void
        {
            _category = value; 
        }	
        
        public function get status() : AlertStatus
        {
            return _status; 
        }
        
        public function set status( value : AlertStatus ) : void
        {
            _status = value; 
        }			
        
        public function get severity() : Vector.<AlertInfoSeverity>
        {
            return _severity; 
        }
        
        public function set severity( value : Vector.<AlertInfoSeverity>) : void
        {
            _severity = value; 
        }			

        public function get eventCode() : String
        {
            return _eventCode; 
        }
        
        public function set eventCode( value : String ) : void
        {
            _eventCode = value; 
        }
        
        public function get colourCode() : MasasEntryColourCode
        {
            return _colourCode; 
        }
        
        public function set colourCode( value : MasasEntryColourCode ) : void
        {
            _colourCode = value; 
        }
        
        // Constructor...
        public function MasasEntryCategory()
        {
        }
        
    }
    
}