package MASAS.feedlib.masas.supportclasses
{
    
    import com.esri.ags.renderers.supportClasses.UniqueValueInfo;
    import com.esri.ags.symbols.Symbol;
    
    import MASAS.feedlib.utilities.Hashtable;
    import MASAS.feedlib.utilities.ResourceHelper;
    import MASAS.feedlib.utilities.SupportedLocale;
    import MASAS.feedlib.utilities.SymbolSet;
    import MASAS.symbology.BitmapSymbol;
    
    public class MasasSymbolSet extends SymbolSet
    {
        
        public function MasasSymbolSet(masasEventCodes:XML, locale:SupportedLocale)
        {
            if (!masasEventCodes)
                throw new Error(ResourceHelper.getErrorString(1, ResourceHelper.getCurrentLocale, ["masasEventCodes", "MasasSymbolSet()"]));
            if (!locale)
                throw new Error(ResourceHelper.getErrorString(1, ResourceHelper.getCurrentLocale, ["locale", "MasasSymbolSet()"]));
            
            createSymbols(masasEventCodes, locale);
        }
        
        private var _defaultSymbol :BitmapSymbol;
        private var _href:String;
        private var _infoTable:Hashtable;
        private var _symbolTable:Hashtable;
        
        public function DefaultSymbol(href:String):BitmapSymbol
        {
            if (!href)
                throw new Error(ResourceHelper.getErrorString(1, ResourceHelper.getCurrentLocale, ["href", , "MasasSymbolSet::DefaultSymbol"]));
            
            if (href == _href)
                return _defaultSymbol;
            else 
            {
                _defaultSymbol = new BitmapSymbol(); 
                _defaultSymbol.href = href;
                return _defaultSymbol;				
            }
        }
        
        override public  function GetSymbol(id:String):Symbol
        {
            id = id.toLowerCase(); 
            if (_symbolTable.containsKey(id))
            {
                return _symbolTable.find(id); 
            }
            else
            {
                return null; 
            }
        }
        
        public  function GetUniqueValueInfo(id:String):Symbol
        {
            id = id.toLowerCase(); 
            if (_infoTable.containsKey(id))
            {
                return _infoTable.find(id); 
            }
            else
            {
                return null; 
            }
        }
        
        private function createSymbols(symbolXML:XML, locale:SupportedLocale):void
        {
            _symbolTable = new Hashtable(); 
            _infoTable = new Hashtable(); 
            
            //Process cap-cp event codes 
            var symbolSet:XMLList = symbolXML.symbolset.(@name=='cap-cp'); 
            var eventCodes:XMLList=symbolSet..event_code;
            var flareSymbol:BitmapSymbol; //instead of picture marker symbol 
            
            for each (var eventCode:XML in eventCodes)
            {
                var id:String=eventCode.@id;	
                if (_symbolTable.containsKey(id))
                    throw new Error(ResourceHelper.getErrorString(2, ResourceHelper.getCurrentLocale, [id, , "MasasSymbolSet::createSymbols"]));
                id = id.substr( (id.lastIndexOf("/") + 1)); //special handling of cap-cp event codes 
                id=id.toLocaleLowerCase(); 
                var source:String=eventCode.symbology.picturemarkersymbol.@source;
                if (source)
                {
                    flareSymbol = new BitmapSymbol();
                    flareSymbol.href = source;
                    var info:UniqueValueInfo=new UniqueValueInfo(  flareSymbol,id );
                    info.label = eventCode.@id; //the full text (it case it's been trimmed by being in cap-cp symbolset
                    if (locale.toString() == SupportedLocale.fr_CA.toString())
                        info.description = eventCode.fr_CA; 
                    else 
                        info.description = eventCode.en_CA; 
                    _symbolTable.add(id,flareSymbol);
                    _infoTable.add(id,info); 
                }
            }
            
            //Process non cap-cp event codes 
            symbolSet = symbolXML.symbolset.(@name!='cap-cp'); 
            eventCodes=symbolSet..event_code;
            for each (eventCode in eventCodes)
            {
                id=eventCode.@id;	
                id=id.toLocaleLowerCase();  
                if (_symbolTable.containsKey(id))
                    throw new Error(ResourceHelper.getErrorString(2, ResourceHelper.getCurrentLocale, [id, "MasasSymbolSet::createSymbols"]));
                source=eventCode.symbology.picturemarkersymbol.@source;
                if (source)
                {
                    flareSymbol = new BitmapSymbol(); 
                    flareSymbol.href = source; 
                    
                    info=new UniqueValueInfo(  flareSymbol,id );
                    info.label = eventCode.@id; //the full text (it case it's been trimmed by being in cap-cp symbolset
                    if (locale.toString() == SupportedLocale.fr_CA.toString())
                        info.description = eventCode.fr_CA; 
                    else 
                        info.description = eventCode.en_CA; 
                    _symbolTable.add(id,flareSymbol);
                    _infoTable.add(id,info); 					
                }
            }		
        }
    }
}