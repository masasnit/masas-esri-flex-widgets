package MASAS.feedlib.masas.supportclasses
{

    import MASAS.feedlib.utilities.Namespaces;

    public final class MasasEntryColourCode
    {

        private static const ATOM_NS    : Namespace = Namespaces.ATOM_NS;
        private static const MEA_NS     : Namespace = Namespaces.MEA_NS;
        
        private static const SCHEME_MASAS_COLOURCODE : String = 'masas:category:colour';
        
        // Private Members...
        
        private var _colour     : MasasEntryColourEnum;
        private var _context    : String;

        // Accessors...
        
        public function get colour() : MasasEntryColourEnum
        {
            return _colour;
        }
        
        public function set colour( value : MasasEntryColourEnum ) : void
        {
            _colour = value;
        }
        
        public function get context() : String
        {
            return _context;
        }
        
        public function set context( value : String ) : void
        {
            _context = value;
        }

        // Constuctor...
        
        public function MasasEntryColourCode()
        {
            _colour = MasasEntryColourEnum.Gray;
            _context = "";
        }
        
        // Public static methods...

        public static function fromXML( value : XML ) : MasasEntryColourCode
        {
            var returnValue : MasasEntryColourCode = null;
            var colourScheme : String = MasasEntryColourCode.SCHEME_MASAS_COLOURCODE;
            var colourCategory : * = value.ATOM_NS::category.(@scheme == colourScheme);
            
            if( colourCategory != undefined )
            {
                returnValue = new MasasEntryColourCode();
                returnValue.colour = MasasEntryColourEnum.fromString( colourCategory.@term.toString() );

                if( colourCategory.@MEA_NS::context != undefined ) {                    
                    returnValue.context = (colourCategory.@MEA_NS::context).toString();
                }
            }

            return returnValue;
        }
        
        // Public methods...
        
        public function toXMLString() : String
        {
            var returnValue : String = null;
            
            returnValue = "<category label='Colour Code' scheme='" + SCHEME_MASAS_COLOURCODE + "' term='" + this._colour.toString() + "'";
            
            if( _context && _context.length > 0 ) {
                returnValue += " xmlns:" + Namespaces.MEA_NS.prefix.toString()+ "='" + Namespaces.MEA_NS.uri + "' " +
                               "mea:context='" + _context + "'";
            }
            
            returnValue += " />";
            
            return returnValue;
        }

    }
    
}