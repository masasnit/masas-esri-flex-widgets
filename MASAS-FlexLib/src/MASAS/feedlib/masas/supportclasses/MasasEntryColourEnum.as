package MASAS.feedlib.masas.supportclasses
{

    import mx.utils.StringUtil;

    public final class MasasEntryColourEnum
    {

        // Public Constants...
        
        public static const Red     : MasasEntryColourEnum = new MasasEntryColourEnum( "Red",       0xC80000 );
        public static const Yellow  : MasasEntryColourEnum = new MasasEntryColourEnum( "Yellow",    0xE1DC00 );
        public static const Green   : MasasEntryColourEnum = new MasasEntryColourEnum( "Green",     0x00A000 );
        public static const Gray    : MasasEntryColourEnum = new MasasEntryColourEnum( "Gray",      0x808080 );
        public static const Black   : MasasEntryColourEnum = new MasasEntryColourEnum( "Black",     0x000000 );
        public static const Blue    : MasasEntryColourEnum = new MasasEntryColourEnum( "Blue",      0x0000FF );
        public static const Purple  : MasasEntryColourEnum = new MasasEntryColourEnum( "Purple",    0x800080 );

        // Private Members...
        
        private var _strColour : String;
        private var _rgbColour : uint;

        // Accessors...
        
        public function get colourString() : String
        {
            return _strColour;
        }
        
        public function get colourRGB() : uint
        {
            return _rgbColour;
        }

        // Constuctor...
        
        public function MasasEntryColourEnum( strColour : String, rgbColour : uint )
        {
            _strColour = strColour;
            _rgbColour = rgbColour;
        }
        
        // Public static methods...

        public static function get values() : Vector.<MasasEntryColourEnum>
        {
            var colorValues : Vector.<MasasEntryColourEnum> = new Vector.<MasasEntryColourEnum>();
            colorValues.push( MasasEntryColourEnum.Red, MasasEntryColourEnum.Yellow, MasasEntryColourEnum.Green, 
                              MasasEntryColourEnum.Gray, MasasEntryColourEnum.Black, MasasEntryColourEnum.Blue,
                              MasasEntryColourEnum.Purple );

            return colorValues;
        }
        
        public static function fromString( value : String ) : MasasEntryColourEnum
        {
            var returnValue : MasasEntryColourEnum = null;
            
            if( !value ) {
                throw new ArgumentError( 'The argument "value" cannot be undefined' );
            }

            var colours : Vector.<MasasEntryColourEnum> = values;
            
            for each( var colour : MasasEntryColourEnum in colours )
            {
                if( colour.colourString.toLowerCase() == StringUtil.trim( value ).toLowerCase() )
                {
                    returnValue = colour;
                    break;
                }
            }

            return returnValue;
        }
        
        // Public methods...
        
        public function toString() : String
        {
         
            var returnValue : String = null;
            var colours : Vector.<MasasEntryColourEnum> = values;
            
            for each( var colour : MasasEntryColourEnum in colours )
            {
                if( colour == this )
                {
                    returnValue = this.colourString;
                    break;
                }
            }
            
            return returnValue;
        }
        
        public function toRGB() : uint
        {
            var returnValue : uint = 0x0;
            var colours : Vector.<MasasEntryColourEnum> = values;
            
            for each( var colour : MasasEntryColourEnum in colours )
            {
                if( colour == this )
                {
                    returnValue = this.colourRGB;
                    break;
                }
            }
            
            return returnValue;
        }
        
    }
    
}