package MASAS.feedlib.masas.supportclasses
{
    import com.adobe.utils.DateUtil;
    
    import mx.utils.StringUtil;
    
    import MASAS.feedlib.cap.enumerations.AlertInfoCategory;
    import MASAS.feedlib.cap.enumerations.AlertInfoSeverity;
    import MASAS.feedlib.cap.enumerations.AlertStatus;
    import MASAS.feedlib.utilities.ErrorWrapper;
    import MASAS.feedlib.utilities.GeoRSSUtil;
    import MASAS.feedlib.utilities.Namespaces;
    import MASAS.feedlib.utilities.ResourceHelper;
    import MASAS.feedlib.utilities.SupportedLocale;
    
    [Bindable]
    public class MasasAtomEntry
    {
        import com.esri.ags.geometry.*;
        
        //Some static const that reference values in Namespaces
        private static const GEORSS : Namespace = Namespaces.GEORSS_NS
        private static const ATOM   : Namespace = Namespaces.ATOM_NS;
        private static const XHTML  : Namespace = Namespaces.XHTML_NS;
        private static const XMLNS  : Namespace = new Namespace( "http://www.w3.org/XML/1998/namespace" );
        private static const PRL    : Namespace = new Namespace( "http://purl.org/atompub/age/1.0" );
        private static const W3     : Namespace = new Namespace( "http://www.w3.org/2007/app" );
        private static const APP    : Namespace = Namespaces.APP_NS;
        private static const MEC    : Namespace = Namespaces.MEC_NS;
        private static const MET_NS : Namespace = Namespaces.MET_NS;
        
        public function MasasAtomEntry(id:String)
        {
            if ((!id) || (id.length == 0) )
                throw new Error(ResourceHelper.getErrorString(0, ResourceHelper.getCurrentLocale, ["id", "MasasAtomEntry()"]));
            _id = id; 	
        }
        
        public function toXMLString(lang:SupportedLocale):String
        {	
            if (!lang) lang = SupportedLocale.en_CA; 
            
            //Validate 'mandatory' fields are present
            if ((!this.ID) || (this.ID.length == 0))
                throw new Error(ResourceHelper.getErrorString(12, ResourceHelper.getCurrentLocale, ["ID", "MasasAtomEntry::toXMLString"]));
            if (!this.author)
                throw new Error(ResourceHelper.getErrorString(12, ResourceHelper.getCurrentLocale, ["author", "MasasAtomEntry::toXMLString"]));
            if ((!this.title) || (this.title.length == 0))
                throw new Error(ResourceHelper.getErrorString(12, ResourceHelper.getCurrentLocale, ["title", "MasasAtomEntry::toXMLString"]));
            if ((!this.content) || (this.content.length == 0))
                throw new Error(ResourceHelper.getErrorString(12, ResourceHelper.getCurrentLocale, ["content", "MasasAtomEntry::toXMLString"]));
            if ((!this.linkEdit) || (this.linkEdit.length == 0))
                throw new Error(ResourceHelper.getErrorString(12, ResourceHelper.getCurrentLocale, ["linkEdit", "MasasAtomEntry::toXMLString"]));
            if (!this.published)
                throw new Error(ResourceHelper.getErrorString(12, ResourceHelper.getCurrentLocale, ["published", "MasasAtomEntry::toXMLString"]));
            if (!this.lastUpdated)
                throw new Error(ResourceHelper.getErrorString(12, ResourceHelper.getCurrentLocale, ["lastUpdated", "MasasAtomEntry::toXMLString"]));
            if (!this.expires)
                throw new Error(ResourceHelper.getErrorString(12, ResourceHelper.getCurrentLocale, ["expires", "MasasAtomEntry::toXMLString"]));
            if (!this.geometry)
                throw new Error(ResourceHelper.getErrorString(12, ResourceHelper.getCurrentLocale, ["geometry", "MasasAtomEntry::toXMLString"]));
            if (!this.category)
                throw new Error(ResourceHelper.getErrorString(12, ResourceHelper.getCurrentLocale, ["category", "MasasAtomEntry::toXMLString"]));
            
            try
            {
                const SCHEME_CAP_STATUS         : String = 'masas:category:status';
                const SCHEME_CAP_SEVERITY       : String = 'masas:category:severity';
                const SCHEME_CAP_ICON           : String = 'masas:category:icon';
                const SCHEME_CAP_CATEGORY       : String = 'masas:category:category';
                
                const LANG_EN:String ="xml:lang='en'"; 
                const LANG_FR:String ="xml:lang='fr'";
                
                var atomString:String; 			
                var utc:String; 
                var status:String = this.category.status.toString(); 
                var icon:String = this.eventCode; 
                var i:int; 
                var severityArray:Array = new Array(); 
                var severityVector:Vector.<AlertInfoSeverity> = this.category.severity; 
                for (i=0; i < severityVector.length; i++)
                {
                    var severity:AlertInfoSeverity = severityVector[i]; 
                    severityArray.push(severity.toString()); 
                }
                var categoryArray:Array = new Array(); 
                var categoryVector:Vector.<AlertInfoCategory> = this.category.category; 
                for (i=0; i < categoryVector.length; i++)
                {
                    var category:AlertInfoCategory = categoryVector[i]; 
                    categoryArray.push(category.toString()); 
                }			
                var geom:Geometry = this.geometry; 
                var geoString:String;
                switch (geom.type)
                {
                    case Geometry.MAPPOINT:
                    {
                        var pt:MapPoint = geom as MapPoint;
                        geoString = pt.y.toString() + " " + pt.x.toString(); //A point contains a single latitude-longitude pair, separated by whitespace. 
                        break;
                    }
                    case Geometry.POLYLINE:
                    {
                        var polyline:Polyline = geom as Polyline;
                        geoString = GeoRSSUtil.toString(polyline);
                        break;
                    }
                    case Geometry.POLYGON:
                    {
                        var polygon:Polygon = geom as Polygon;
                        geoString = GeoRSSUtil.toString(polygon);
                        break;
                    }
                    case Geometry.EXTENT:
                    {
                        var extent:Extent = geom as Extent;
                        geoString = GeoRSSUtil.toString(extent);
                        break;
                    }
                    default:
                        //Throw an error. Not supported by hub yet
                        throw new Error(ResourceHelper.getErrorString(1, ResourceHelper.getCurrentLocale, [geom.type, "MasasFeed::getAtomPublishString"]));
                }
                
                //Build the xml-like string one piece at a time.
                atomString = "<entry xmlns='" + Namespaces.ATOM_NS.toString() + "'>";
                
                //Entry ID (mandatory)
                var id:String = this.ID; 
                atomString += "<id>" + id + "</id>";
                
                //Entry Author (mandatory)
                var author:MasasEntryAuthor = this.author; 
                atomString += "<author>";
                atomString += "<name>" + author.name + "</name>";
                atomString += "<uri>" + author.uri + "</uri>";
                atomString += "<email>" + author.email + "</email>";
                atomString += "</author>";
                
                //Entry Title (mandatory)
                atomString += "<title type='xhtml'>"
                atomString += "<div xmlns='" + Namespaces.XHTML_NS.toString() + "'>";
                if (lang.toString() == SupportedLocale.fr_CA.toString())
                    atomString += "<div xml:lang='fr'>" + this.title  + "</div>";
                else 
                    atomString += "<div xml:lang='en'>" + this.title  + "</div>";			
                atomString += "</div>";
                atomString += "</title>";				
                
                //Entry Content (mandatory)
                atomString += "<content type='xhtml'>";
                atomString += "<div xmlns='" + Namespaces.XHTML_NS.toString() + "'>";
                if (lang.toString() == SupportedLocale.fr_CA.toString())
                    atomString += "<div xml:lang='fr'>" + this.content  + "</div>";
                else 
                    atomString += "<div xml:lang='en'>" + this.content  + "</div>";
                atomString += "</div>";
                atomString += "</content>";
                
                //Entry Summary (optional)
                var summary:String = this.summary;
                if ((summary) && (summary.length > 0))
                {
                    atomString += "<summary type='xhtml'>";
                    atomString += "<div xmlns='" + Namespaces.XHTML_NS.toString() + "'>";
                    if (lang.toString() == SupportedLocale.fr_CA.toString())
                        atomString += "<div xml:lang='fr'>" + this.summary  + "</div>";
                    else 
                        atomString += "<div xml:lang='en'>" + this.summary  + "</div>";
                    atomString += "</div>";
                    atomString += "</summary>";				
                }
                
                //Entry Rights (optional)
                var rights:String = this.rights;
                if ((rights) && (rights.length > 0))
                {
                    atomString += "<rights type='xhtml'>";
                    atomString += "<div xmlns='" + Namespaces.XHTML_NS.toString() + "'>";
                    if (lang.toString() == SupportedLocale.fr_CA.toString())
                        atomString += "<div xml:lang='fr'>" + this.rights  + "</div>";
                    else 
                        atomString += "<div xml:lang='en'>" + this.rights  + "</div>";
                    atomString += "</div>";
                    atomString += "</rights>";				
                }				
                
                //Entry Link Edit (mandatory)
                var linkEdit:String = this.linkEdit; 
                atomString += "<link href='" + linkEdit + "' rel='edit' />";
                
                //Entry Link Edit-Media (optional)
                var linkEditMedia:String = this.linkEditMedia;
                if ((linkEditMedia) && (linkEditMedia.length > 0 ))
                    atomString += "<link href='" + linkEditMedia + "' rel='edit-media' />";
                
                //Entry Link Revisions (optional)
                var linkRevisions:String = this.linkRevisions;
                if ((linkRevisions) && (linkRevisions.length > 0 ))
                    atomString += "<link href='" + linkRevisions + "' rel='revisions' />";			
                
                //Entry Link Enclosure(s) (optional)
                var linkEnclosures:Vector.<MasasEntryEnclosure> = this.linkEnclosures;
                if ((linkEnclosures) && (linkEnclosures.length > 0 ))
                    for (i=0; i < linkEnclosures.length; i++)
                    {
                        var e:MasasEntryEnclosure = linkEnclosures[i]; 
                        atomString += "<link href='" + e.href + "' length='" + e.length + "' rel='enclosures' type ='" + e.type + "' />";			
                    }
                
                //Entry Link Related (optional)
                var linkRelated:Vector.<String> = this.linkRelated;
                if ((linkRelated) && (linkRelated.length > 0 ))
                    for (i=0; i < linkRelated.length; i++)
                    {
                        atomString += "<link href='" + linkRelated[0] + "' rel='related' />"; 						
                    }
                
                //Entry Published Date (mandatory)
                utc = DateUtil.toW3CDTF(this.published).replace("-00:00", "Z").replace("+00:00", "Z"); 
                atomString += "<published xmlns='" + Namespaces.ATOM_PUB_DATE.toString() + "'>" + utc + "</published>";
                
                //Entry Last Update Date (mandatory)
                utc = DateUtil.toW3CDTF(this.lastUpdated).replace("-00:00", "Z").replace("+00:00", "Z");
                atomString += "<updated xmlns='" + Namespaces.ATOM_PUB_DATE.toString() + "'>" + utc + "</updated>";
                
                //Entry Last Edit Date (optional)
                if (this.lastEdited)
                {
                    utc = DateUtil.toW3CDTF(this.lastEdited).replace("-00:00", "Z").replace("+00:00", "Z");
                    atomString += "<edited xmlns='" + Namespaces.ATOM_PUB_DATE.toString() + "'>" + utc + "</edited>";
                }
                
                //Entry Expiration Date (mandatory)
                utc = DateUtil.toW3CDTF(this.expires).replace("-00:00", "Z").replace("+00:00", "Z");
                atomString += "<expires xmlns='" + Namespaces.ATOM_PUB_DATE.toString() + "'>" + utc + "</expires>";
                
                // Entry Effective Date (optional)
                if( this.effective )
                {
                    utc = DateUtil.toW3CDTF( this.effective ).replace( "-00:00", "Z" ).replace( "+00:00", "Z" );
                    atomString += "<effective xmlns='" + Namespaces.MET_NS.toString() + "'>" + utc + "</effective>";
                }
                
                //Entry GeoRSS Simple Point (mandatory)
                switch (geom.type)
                {
                    case Geometry.MAPPOINT:
                        atomString += "<point xmlns='" + Namespaces.GEORSS_NS.toString() +  "'>" + geoString + "</point>";
                        break;
                    case Geometry.POLYLINE:
                        atomString += "<line xmlns='" + Namespaces.GEORSS_NS.toString() +  "'>" + geoString + "</line>";
                        break;
                    case Geometry.POLYGON:
                        atomString += "<polygon xmlns='" + Namespaces.GEORSS_NS.toString() +  "'>" + geoString + "</polygon>";
                        break;
                    case Geometry.EXTENT:
                        atomString += "<box xmlns='" + Namespaces.GEORSS_NS.toString() +  "'>" + geoString + "</box>";
                        break;
                    default:
                        //Unexpected error. 
                        throw new Error(ResourceHelper.getErrorString(1, ResourceHelper.getCurrentLocale, [geom.type, "MasasFeed::getAtomPublishString"]));
                }
                
                //Entry Category (mandatory)
                atomString += "<category label='Status' scheme='" + SCHEME_CAP_STATUS + "' term='" + status + "' />";
                for (i=0; i < severityArray.length; i++)
                {
                    atomString += "<category label='Severity' scheme='" + SCHEME_CAP_SEVERITY + "' term='" + severityArray[i] + "' />";
                }
                atomString += "<category label='Icon' scheme='" + SCHEME_CAP_ICON + "' term='" + icon + "' />";
                for (i=0; i < categoryArray.length; i++)
                {
                    atomString += "<category label='Category' scheme='" + SCHEME_CAP_CATEGORY + "' term='" + categoryArray[i] + "' />";
                }
                
                // Entry Color Code (Optional)...
                if( this.category.colourCode ) {
                    atomString += this.category.colourCode.toXMLString();
                }
                
                // Permissions: Update
                if( permissionAllowUpdatesByUsers.length > 0 )
                {
                    atomString += "<control xmlns='http://www.w3.org/2007/app'>";
                    atomString += "<update xmlns='masas:extension:control'>";
                    for( i = 0; i < permissionAllowUpdatesByUsers.length; i++ )
                    {
                        if( i > 0 ) {
                            atomString += " ";
                        }
                        
                        atomString += permissionAllowUpdatesByUsers[i];
                    }
                    atomString += "</update>";
                    atomString += "</control>";
                }
                
                atomString += "</entry>" ;
                atomString.replace("'", "\"");
            }
            catch (e:Error)
            {
                //Unexpected error. 
                throw new ErrorWrapper(ResourceHelper.getErrorString(9, ResourceHelper.getCurrentLocale, [e.message.toString(), "MasasAtomEntry::toXMLString"]));
            }
            
            return atomString; 
        }
        
        private var _id:String;
        public function get ID():String 
        {
            return _id; 
        }
        
        private var _author:MasasEntryAuthor;
        public function get author():MasasEntryAuthor
        {
            return _author; 
        }
        
        public function set author(value:MasasEntryAuthor):void
        {
            _author = value; 
        }
        
        private var _title:String;
        public function get title():String
        {
            return _title; 
        }
        
        public function set title(value:String):void
        {
            _title = value; 
        }
        
        private var _content:String
        public function get content():String
        {
            return _content; 
        }
        
        public function set content(value:String):void
        {
            _content = value; 
        }
        
        private var _summary:String
        public function get summary():String
        {
            return _summary; 
        }
        
        public function set summary(value:String):void
        {
            _summary = value; 
        }
        
        private var _rights:String
        public function get rights():String
        {
            return _rights; 
        }
        
        public function set rights(value:String):void
        {
            _rights = value; 
        }
        
        private var _linkEdit:String
        public function get linkEdit():String
        {
            return _linkEdit; 
        }
        
        public function set linkEdit(value:String):void
        {
            _linkEdit = value; 
        }
        
        private var _linkEditMedia:String
        public function get linkEditMedia():String
        {
            return _linkEditMedia; 
        }
        
        public function set linkEditMedia(value:String):void
        {
            _linkEditMedia = value; 
        }
        
        private var _linkRevisions:String
        public function get linkRevisions():String
        {
            return _linkRevisions; 
        }
        
        public function set linkRevisions(value:String):void
        {
            _linkRevisions = value; 
        }	
        
        private var _linkEnclosures:Vector.<MasasEntryEnclosure> = new Vector.<MasasEntryEnclosure>();
        public function get linkEnclosures():Vector.<MasasEntryEnclosure>
        {
            return _linkEnclosures; 
        }
        
        public function set linkEnclosures(value:Vector.<MasasEntryEnclosure>):void
        {
            _linkEnclosures = value; 
        }
        
        private var _linkRelated:Vector.<String> 
        public function get linkRelated():Vector.<String> 
        {
            return _linkRelated; 
        }
        
        public function set linkRelated(value:Vector.<String> ):void
        {
            _linkRelated = value; 
        }
        
        private var _published:Date;
        public function get published():Date
        {
            return _published; 
        }
        
        public function set published(value:Date):void
        {
            _published = value; 
        }		
        
        private var _lastUpdated:Date;
        public function get lastUpdated():Date
        {
            return _lastUpdated; 
        }
        
        public function set lastUpdated(value:Date):void
        {
            _lastUpdated = value; 
        }	
        
        private var _lastEdited:Date;
        public function get lastEdited():Date
        {
            return _lastEdited; 
        }
        
        public function set lastEdited(value:Date):void
        {
            _lastEdited = value; 
        }	
        
        private var _expiration:Date;
        public function get expires():Date
        {
            return _expiration; 
        }
        
        public function set expires(value:Date):void
        {
            _expiration = value; 
        }
        
        private var _effective : Date;
        public function get effective() : Date
        {
            return _effective; 
        }
        
        public function set effective( value : Date ) : void
        {
            _effective = value; 
        }
        
        private var _geometry:Geometry;
        public function get geometry():Geometry
        {
            return _geometry; 
        }
        
        public function set geometry(value:Geometry):void
        {
            _geometry = value; 
        }		
        
        private var _category:MasasEntryCategory;
        public function get category():MasasEntryCategory
        {
            return _category; 
        }
        
        public function set category(value:MasasEntryCategory):void
        {
            _category = value; 
        }		
        
        public function get eventCode():String
        {
            return this.category.eventCode; 
        }
        
        public function get capAlertLink():String
        {
            var enclosures:Vector.<MasasEntryEnclosure> = this.linkEnclosures;
            if ((enclosures) && (enclosures.length > 0))
            {
                for (var i:int=0; i < enclosures.length; i++)
                {
                    var e:MasasEntryEnclosure = enclosures[i]; 
                    if (e.type.toLowerCase() == 'application/common-alerting-protocol+xml') return e.href;   
                }			
            }
            
            //If got this far, return null
            return null; 
        }
        
        private var _permissionAllowUpdatesByUsers:Vector.<String> = new Vector.<String>(); 
        public function get permissionAllowUpdatesByUsers() : Vector.<String>
        {
            return _permissionAllowUpdatesByUsers; 
        }
        
        public function set permissionAllowUpdatesByUsers( value : Vector.<String> ) : void
        {
            _permissionAllowUpdatesByUsers = value; 
        }
        
        //Create a MasasAtomEntry
        public static function fromXML(entryXML:XML, preferredLocale:SupportedLocale):MasasAtomEntry
        {
            var entry:MasasAtomEntry = new MasasAtomEntry(getId(entryXML)); 
            
            try
            {
                if ((!entryXML) || (entryXML.length == 0) )
                    throw new Error(ResourceHelper.getErrorString(1, ResourceHelper.getCurrentLocale, ["entryXML", "MasasAtomEntry::fromXML"]));
                
                //Set the author
                var author:MasasEntryAuthor = new MasasEntryAuthor();
                author.name = getAuthorInfo(entryXML, "name");
                author.email= getAuthorInfo(entryXML, "email")
                author.uri = getAuthorInfo(entryXML, "uri");  
                entry.author = author; 
                
                //Set the category 
                var masasCategory : MasasEntryCategory = new MasasEntryCategory(); 
                
                //loop through the strCategory to create a Vector.<AlertInfoCategory>
                var vecAlertInfoSeverity:Vector.<AlertInfoSeverity> = new Vector.<AlertInfoSeverity>(); 
                var strSeverity:String = getCategory(entryXML, "severity"); //What is this set to????
                
                // As of version 2, severity is no longer a required element
                if (strSeverity.length != 0)
                {
                    var arr:Array = strSeverity.split(","); 
                    for (var i:int=0; i < arr.length; i++)
                    {
                        vecAlertInfoSeverity.push(AlertInfoSeverity.fromString(arr[i])); 
                    }
                }
                
                masasCategory.severity = vecAlertInfoSeverity;
                
                //Status
                masasCategory.status = AlertStatus.fromString(getCategory(entryXML, "status"));
                
                //Eventcode
                masasCategory.eventCode = getCategory(entryXML, "icon");
                
                //loop through the strCategory to create a Vector.<AlertInfoCategory>
                var vecAlertInfoCategory:Vector.<AlertInfoCategory> = new Vector.<AlertInfoCategory>(); 
                var strCategory:String = getCategory(entryXML, "category"); //What is this set to????
                
                // As of version 2, category is no longer a required element
                if ( strCategory.length != 0)
                {
                    arr = strCategory.split(","); 
                    for (i=0; i < arr.length; i++)
                    {
                        vecAlertInfoCategory.push(AlertInfoCategory.fromString(arr[i])); 
                    }			
                }
                
                masasCategory.category = vecAlertInfoCategory;
                
                // The colour category...
                masasCategory.colourCode = MasasEntryColourCode.fromXML( entryXML );
                
                entry.category = masasCategory; 		
                entry.published=getDate(entryXML, "published");
                entry.lastUpdated=getDate(entryXML, "updated");
                entry.lastEdited=getDate(entryXML, "edited");
                entry.expires=getDate(entryXML, "expires");
                entry.effective = getDate( entryXML, "effective" );
                entry.geometry = getGeometry(entryXML);
                entry.linkEdit= getLink(entryXML, "edit");
                entry.linkEditMedia= getLink(entryXML, "edit-media");	
                entry.linkRevisions= getLink(entryXML, "revisions");	
                entry.linkEnclosures = getEnclosures(entryXML); 
                entry.linkRelated = getRelated(entryXML); 
                entry.title=getTitle(entryXML, preferredLocale);
                entry.content=getContent(entryXML, preferredLocale);
                entry.rights=getRights(entryXML, preferredLocale);
                entry.summary=getSummary(entryXML, preferredLocale);
                entry.permissionAllowUpdatesByUsers = getPermissionAllowUpdatesByUsers( entryXML );
            }
            catch (e:Error)
            {
                //Unexpected error. 
                throw new ErrorWrapper(ResourceHelper.getErrorString(9, ResourceHelper.getCurrentLocale, [e.message.toString(), "MasasAtomEntry::fromXML"]));
            }
            
            //Do some sanity checks to make sure all fields required by MASAS hub are present
            if ((!entry.ID) || (entry.ID.length == 0))
                throw new ErrorWrapper(ResourceHelper.getErrorString(11, ResourceHelper.getCurrentLocale, ["Entry ID", "MasasAtomEntry::fromXML"]));
            else if (!entry.author)
                throw new ErrorWrapper(ResourceHelper.getErrorString(11, ResourceHelper.getCurrentLocale, ["Entry Author", "MasasAtomEntry::fromXML"]));
                //else if ((!entry.title) || (entry.title.length == 0))
            else if ( entry.title == null )
                throw new ErrorWrapper(ResourceHelper.getErrorString(11, ResourceHelper.getCurrentLocale, ["Entry Title", "MasasAtomEntry::fromXML"]));
                //else if ((!entry.content) || (entry.content.length == 0))
            else if ( entry.content == null )
                throw new ErrorWrapper(ResourceHelper.getErrorString(11, ResourceHelper.getCurrentLocale, ["Entry Content", "MasasAtomEntry::fromXML"]));
            else if ((!entry.linkEdit) || (entry.linkEdit.length == 0))
                throw new ErrorWrapper(ResourceHelper.getErrorString(11, ResourceHelper.getCurrentLocale, ["Entry Link Edit", "MasasAtomEntry::fromXML"]));
            else if (!entry.published)
                throw new ErrorWrapper(ResourceHelper.getErrorString(11, ResourceHelper.getCurrentLocale, ["Entry Published Date", "MasasAtomEntry::fromXML"]));
            else if (!entry.lastUpdated)
                throw new ErrorWrapper(ResourceHelper.getErrorString(11, ResourceHelper.getCurrentLocale, ["Entry Last Update Date", "MasasAtomEntry::fromXML"]));
            else if (!entry.geometry)
                throw new ErrorWrapper(ResourceHelper.getErrorString(11, ResourceHelper.getCurrentLocale, ["Entry GeoRSS Simple Point", "MasasAtomEntry::fromXML"]));
            else if (!entry.category)
                throw new ErrorWrapper(ResourceHelper.getErrorString(11, ResourceHelper.getCurrentLocale, ["Entry Category", "MasasAtomEntry::fromXML"]));
            
            return entry; 
        }
        
        private static function getId(x:XML):String
        {
            var returnString:String=null;
            
            try
            {
                returnString=String(x.ATOM::id);
            }
            catch (objError:Error)
            {
            }
            
            return returnString;
        }
        
        private static function getAuthorInfo(x:XML, infoField:String):String
        {
            var returnString:String=null;
            
            try
            {
                var authorBlock:XML=XML(x.ATOM::author);
                
                if (infoField == "name")
                {
                    returnString=authorBlock.ATOM::name.toString();
                }
                else if (infoField == "email")
                {
                    returnString=authorBlock.ATOM::email.toString();
                }
                else if (infoField == "uri")
                {
                    returnString=authorBlock.ATOM::uri.toString();
                }
            }
            catch (objError:Error)
            {
            }
            
            return returnString;
        }
        
        private static function getCategory( x : XML, categoryField : String ) : String
        {
            var returnString : String = "";
            
            try
            {
                var categoryList : XMLList = XMLList( x.ATOM::category );
                var arr : Array = new Array(); 
                for each( var category:XML in categoryList )
                {
                    if( category.@scheme.toString() == "masas:category:" + categoryField )
                    {
                        arr.push( category.@term.toString() ); 
                    }					  
                    else if( category.@scheme.toString() == "masas:category:" + categoryField )
                    {
                        arr.push( category.@term.toString() ); 
                    }
                }
                return arr.toString(); 
            }
            catch( objError:Error )
            {
            }
            
            return returnString;
        }
        
        private static function getTitle(x:XML, local:SupportedLocale):String
        {
            var returnString:String=null;
            try
            {
                
                //If the user specified a language filter when requesting a feed,
                //the returned xml will be type='text'. Else type='xhtml'.
                var titleBlock:XML=XML(x.ATOM::title);			
                if (titleBlock.@type.toString().toLowerCase() == 'text')
                {
                    return titleBlock.toString(); 
                }
                
                //type = xhtml
                var titleDiv:XML=XML(titleBlock.XHTML::div);
                var titleList:XMLList=XMLList(titleDiv.XHTML::div);
                var titles:XMLList;
                
                if (local.toString() == SupportedLocale.fr_CA.toString())
                {
                    titles=XMLList(titleList.(@XMLNS::lang == "fr").toString());
                    if (titles.length() == 0) return getTitle(x, SupportedLocale.en_CA); 
                }
                else 
                {
                    titles=XMLList(titleList.(@XMLNS::lang == "en").toString());
                }
                
                // Sometimes there is more than 1 title - take the first one...
                returnString=titles[0].toString();
                
            }
            catch (objError:Error)
            {
            }
            
            return returnString;
        }
        
        private static function getContent(x:XML, local:SupportedLocale):String
        {
            var returnString : String = "";
            
            try
            {
                //If the user specified a language filter when requesting a feed,
                //the returned xml will be type='text'. Else type='xhtml'.
                var contentBlock:XML=XML(x.ATOM::content);
                if (contentBlock.@type.toString().toLowerCase() == 'text')
                {
                    return contentBlock.toString(); 
                }
                
                //type = 'xhtml'
                var contentDiv:XML=XML(contentBlock.XHTML::div);
                var contentList:XMLList=XMLList(contentDiv.XHTML::div);
                var contents:XMLList;
                
                if (local.toString() == SupportedLocale.fr_CA.toString())
                {
                    contents=XMLList(contentList.(@XMLNS::lang == "fr").toString());
                    if (contents.length() == 0) return getTitle(x, SupportedLocale.en_CA); 
                }
                else 
                {
                    contents=XMLList(contentList.(@XMLNS::lang == "en").toString());
                }
                
                // Sometimes there is more than 1 title - take the first one...
                returnString=contents[0].toString();
            }
            catch (objError:Error)
            {
            }
            
            return returnString;
        }
        
        private static function getRights(x:XML, local:SupportedLocale):String
        {
            var returnString:String=null;
            
            try
            {
                //If the user specified a language filter when requesting a feed,
                //the returned xml will be type='text'. Else type='xhtml'.
                var rightsBlock:XML=XML(x.ATOM::rights);
                if (rightsBlock.@type.toString().toLowerCase() == 'text')
                {
                    return rightsBlock.toString(); 
                }
                
                //type = 'xhtml'
                var rightsDiv:XML=XML(rightsBlock.XHTML::div);
                var rightsList:XMLList=XMLList(rightsDiv.XHTML::div);
                var rights:XMLList; 
                
                if (local.toString() == SupportedLocale.fr_CA.toString())
                {
                    rights=XMLList(rightsList.(@XMLNS::lang == "fr").toString());
                    if (rights.length() == 0) return getTitle(x, SupportedLocale.en_CA); 
                }
                else 
                {
                    rights=XMLList(rightsList.(@XMLNS::lang == "en").toString());
                }
                
                // Sometimes there is more than 1 title - take the first one...
                returnString=rights[0].toString();
                
                
            }
            catch (objError:Error)
            {
            }
            
            return returnString;
        }
        
        private static function getSummary(x:XML, local:SupportedLocale):String
        {
            var returnString:String=null;
            
            try
            {
                //If the user specified a language filter when requesting a feed,
                //the returned xml will be type='text'. Else type='xhtml'.
                var summaryBlock:XML=XML(x.ATOM::summary);
                if (summaryBlock.@type.toString().toLowerCase() == 'text')
                {
                    return summaryBlock.toString(); 
                }
                
                //type = 'xhtml'
                var summaryDiv:XML=XML(summaryBlock.XHTML::div);
                var summaryList:XMLList=XMLList(summaryDiv.XHTML::div);
                var summary:XMLList; 
                
                if (local.toString() == SupportedLocale.fr_CA.toString())
                {
                    summary=XMLList(summaryList.(@XMLNS::lang == "fr").toString());
                    if (summary.length() == 0) return getTitle(x, SupportedLocale.en_CA); 
                }
                else 
                {
                    summary=XMLList(summaryList.(@XMLNS::lang == "en").toString());
                }
                
                // Sometimes there is more than 1 title - take the first one...
                returnString=summary[0].toString();
            }
            catch (objError:Error)
            {
            }
            
            return returnString;
        }
        
        private static function getDate(x:XML, dateField:String):Date
        {
            var returnDate:Date=new Date();
            
            try
            {
                if (dateField == "published")
                {
                    returnDate=DateUtil.parseW3CDTF(String(x.ATOM::published));
                }
                else if (dateField == "updated")
                {
                    returnDate=DateUtil.parseW3CDTF(String(x.ATOM::updated));
                }
                else if (dateField == "expires")
                {
                    returnDate=DateUtil.parseW3CDTF(String(x.PRL::expires));
                }
                else if (dateField == "edited")
                {
                    returnDate=DateUtil.parseW3CDTF(String(x.W3::edited));
                }
                else if( dateField == "effective" )
                {
                    returnDate = null;
                    returnDate = DateUtil.parseW3CDTF( String ( x.MET_NS::effective ) );
                }
            }
            catch (objError:Error)
            {
            }
            
            return returnDate;
        }
        
        private static function getGeometry(x:XML):Geometry
        {
            var geometry:Geometry = GeoRSSUtil.toGeometry(x);
            
            trace(geometry.type);
            
            return geometry;
        }
        
        private static function getLink(x:XML, linkField:String):String 
        {
            var returnString:String=null;
            
            try
            {
                var linkList:XMLList=XMLList(x.ATOM::link);
                returnString=linkList.(attribute("rel") == linkField).attribute("href").toString();
                
            }
            catch (objError:Error)
            {
            }
            
            if (returnString)
                if (returnString.length == 0)
                    returnString=null;
            return returnString;
        }
        
        private static function getEnclosures(x:XML):Vector.<MasasEntryEnclosure>  
        {
            var returnVec:Vector.<MasasEntryEnclosure>= new Vector.<MasasEntryEnclosure>; 
            try
            {
                use namespace ATOM; 
                var linkList:XMLList = x.ATOM::link.(@rel=='enclosure');	
                for each (var link:XML in linkList)
                {
                    var href:String = link.@href;
                    var type:String = link.@type; 
                    var len:String = link.@length;
                    var title:String = link.@title;
                    if ((!len) || (len.length == 0)) len = "0"; 
                    if ((href.length > 0) && (type.length > 0))
                    {
                        var enclosure:MasasEntryEnclosure = new MasasEntryEnclosure(href,type, parseInt( len ), title); 
                        returnVec.push(enclosure); 	
                    }
                }		
            }
            catch (objError:Error)
            {
            }
            
            if (returnVec.length == 0)
                returnVec=null;
            return returnVec;
        }
        
        private static function getRelated(x:XML):Vector.<String> 
        {
            var returnVec:Vector.<String>= new Vector.<String>; 
            try
            {
                use namespace ATOM; 
                var linkList:XMLList=x.ATOM::link.(@rel=='related');
                for each (var link:XML in linkList)
                {
                    var s:String = link.@href; 
                    if (s.length > 0) 
                        returnVec.push(s); 		
                }		
            }
            catch (objError:Error)
            {
            }
            
            if (returnVec.length == 0)
                returnVec=null;
            return returnVec;
        }
        
        private static function getPermissionAllowUpdatesByUsers( x : XML ) : Vector.<String>
        {
            var returnVec : Vector.<String> = new Vector.<String>;
            
            try
            {
                var controlXML : XML = XML(x.APP::control);
                var updateXML : XML = XML(controlXML.MEC::update);
                var users : Array = updateXML.toString().split( " " );
                
                for each (var user:String in users) {
                    returnVec.push( user ); 		
                }		
            }
            catch( objError : Error )
            {
            }
            
            return returnVec;
        }
        
    }
}