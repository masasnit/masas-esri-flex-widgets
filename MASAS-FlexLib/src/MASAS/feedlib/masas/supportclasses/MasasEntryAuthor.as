package MASAS.feedlib.masas.supportclasses
{
	[Bindable]
	public class MasasEntryAuthor
	{
		public function MasasEntryAuthor(){}
		
		private var _name:String;
		public function get name():String
		{
			return _name; 
		}
		
		public function set name(value:String):void
		{
			_name = value; 
		}
		
		private var _email:String;
		public function get email():String
		{
			return _email; 
		}
		
		public function set email(value:String):void
		{
			_email = value; 
		}
		
		private var _uri:String;
		public function get uri():String
		{
			return _uri; 
		}
		
		public function set uri(value:String):void
		{
			_uri = value; 
		}
	}
}