package MASAS.feedlib.events
{
	import flash.events.Event;
	
	public class FeedEvent extends Event
	{
		private var _data:Object;
		private var _callback:Function;
		
		//The 'data' object for FEED_ERROR_HTTP will be an object as follows
		//  var data:Object =
		//     {
		//       httpCode:String,
		//	     description:String
		//	   }
		public static const FEED_ERROR_HTTP:String="feedErrorHttp";
		
		//FEED_ERROR_PARSE to be used when client side code fails to parse response from server 
		public static const FEED_ERROR_PARSE:String="feedErrorParse";
		
		//The 'data' object for FEED_LOADED will be an object as follows
		//  var data:Object =
		//     {
		//       featureLayer:FeatureLayer,
		//	     ??? other ???
		//	   }
		public static const FEED_SUCCESS:String="feedSuccess";
		
		public function FeedEvent(type:String, data:Object=null, callback:Function=null)
		{
			super(type);
			_data=data;
			_callback=callback;
		}
		
		//The data will be passed via the event. It allows the event dispatcher to publish
		//data to event listener(s).
		public function get data():Object
		{
			return _data;
		}
		
		public function set data(value:Object):void
		{
			_data=value;
		}
		
		//The callback function associated with this event.
		public function get callback():Function
		{
			return _callback;
		}
		
		public function set callback(value:Function):void
		{
			_callback=value;
		}
		
		public override function clone():Event
		{
			return new FeedEvent(this.type, this.data);
		}
	}
}