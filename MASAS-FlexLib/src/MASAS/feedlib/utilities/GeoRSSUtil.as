package MASAS.feedlib.utilities
{
	import com.esri.ags.geometry.Extent;
	import com.esri.ags.geometry.Geometry;
	import com.esri.ags.geometry.MapPoint;
	import com.esri.ags.geometry.Polygon;
	import com.esri.ags.geometry.Polyline;
	
	import mx.utils.StringUtil;
	
	public class GeoRSSUtil
	{
		private static const GEORSS:Namespace=Namespaces.GEORSS_NS;
		private static const GEO:Namespace=Namespaces.GEO_NS;
		private static const GML:Namespace=Namespaces.GML_NS;
		private static const GEOLL:Namespace=Namespaces.GEO_LL;
		
		// Earth's radius in meters
		private static const EARTHRADIUS:Number = 6371000.0;
		
		public function GeoRSSUtil(singletonEnforcer:SingletonEnforcer){}
		
		public static function toString(geometry:Geometry):String
		{
			switch (geometry.type)
			{
				case Geometry.POLYLINE:
					return polylineToGeoRSS(geometry as Polyline);
					break;
				case Geometry.POLYGON:
					return polygonToGeoRSS(geometry as Polygon);
					break;
				case Geometry.EXTENT:
					return extentToGeoRSS(geometry as Extent);
					break;
				default:
					return null;
			}
		}
		
		public static function toGeometry(x:XML):Geometry
		{
			var georssPoint:String=String(x.GEORSS::point);
			if (georssPoint)
			{
				return parseGeoRSSPoint(georssPoint);
			}
			var georssLine:String=String(x.GEORSS::line);
			if (georssLine)
			{
				return parseGeoRSSLine(georssLine);
			}
			var georssPolygon:String=String(x.GEORSS::polygon);
			if (georssPolygon)
			{
				return parseGeoRSSPolygon(georssPolygon);
			}
			var georssBox:String=String(x.GEORSS::box);
			if (georssBox)
			{
				return parseGeoRSSBox(georssBox);
			}
			var georssCircle:String=String(x.GEORSS::circle);
			if (georssCircle)
			{
				return parseGeoRSSCircle(georssCircle);
			}
			var pointList:XMLList=x.GEO::point;
			if (pointList && pointList.length() > 0)
			{
				var geoPoint:XML=pointList[0];
				var getLat:Number=Number(geoPoint.GEO::lat);
				var geoLon:Number=Number(geoPoint.GEO::long);
				return new MapPoint(geoLon, getLat);
			}
			var whereList:XMLList=x.GEORSS::where;
			if (whereList && whereList.length() > 0)
			{
				var pos:String=whereList[0].GML::Point[0].GML::pos[0];
				var arr:Array=pos.split(" ");
				var gmlLat:Number=Number(arr[0]);
				var gmlLon:Number=Number(arr[1]);
				return new MapPoint(gmlLon, gmlLat);
			}
			var georssLat:String=String(x.GEOLL::lat);
			var georssLong:Number=Number(x.GEOLL::long);
			if ((georssLong) && (georssLat))
			{
				return new MapPoint(Number(georssLong), Number(georssLat));
			}
			return null;
		}
		
		private static function parseGeoRSSWhere(x:XML):Geometry
		{
			return null;
		}
		
		private static function parseGeoRSSPoint(text:String):Geometry
		{
			var tokens:Array=StringUtil.trim(text).split(" ");
			var lat:Number=Number(tokens[0]);
			var lon:Number=Number(tokens[1]);
			return new MapPoint(lon, lat);
		}
		
		private static function parseGeoRSSLine(text:String):Geometry
		{
			var path:Array=[];
			var tokens:Array=StringUtil.trim(text).split(" ");
			if (tokens.length > 3)
			{
				for (var i:int=0, j:int=1; j < tokens.length; i+=2, j+=2)
				{
					var lat:Number=Number(tokens[i]);
					var lon:Number=Number(tokens[j]);
					path.push(new MapPoint(lon, lat));
				}
			}
			return new Polyline([path]);
		}
		
		private static function parseGeoRSSPolygon(text:String):Geometry
		{
			var path:Array=[];
			var tokens:Array=StringUtil.trim(text).split(" ");
			for (var i:int=0, j:int=1; j < tokens.length; i+=2, j+=2)
			{
				var lat:Number=Number(tokens[i]);
				var lon:Number=Number(tokens[j]);
				path.push(new MapPoint(lon, lat));
			}
			return new Polygon([path]);
		}
		
		// Example 46.56 -76.18 25000
		private static function parseGeoRSSCircle(text:String):Geometry
		{
			var path:Array=[];
			
			var tokens:Array=StringUtil.trim(text).split(" ");
			
			var lat:Number=Number(tokens[0]);
			var lon:Number=Number(tokens[1]);
			var radius:Number=Number(tokens[2]);
			
			// Push 36 points, one every 10 degrees, to create the circle...
			// Could also use geometry service
			for (var i:int=0; i < 360; i=i + 10)
			{
				path.push(calculateDestinationPoint(lat, lon, radius, i));
			}
			
			return new Polygon([path]);
		}
		
		private static function parseGeoRSSBox(text:String):Geometry
		{
			var tokens:Array=StringUtil.trim(text).split(" ");
			
			return new Extent(tokens[1], tokens[0], tokens[3], tokens[2]);
		}
		
		private static function polylineToGeoRSS(polyline:Polyline):String
		{
			var returnString:String = "";
			
			for each(var path:Array in polyline.paths)
			{
				for each (var geometry:Geometry in path)
				{
					if (returnString.length != 0)
					{
						returnString += " "
					}
					
					var myPoint:MapPoint = geometry as MapPoint;
					returnString += myPoint.y + " " + myPoint.x;
				}
			}
			
			return returnString;
		}
		
		private static function polygonToGeoRSS(polygon:Polygon):String
		{
			var returnString:String = "";
			
			for each(var ring:Array in polygon.rings)
			{
				for each (var geometry:Geometry in ring)
				{
					if (returnString.length != 0)
					{
						returnString += " "
					}
					
					var myPoint:MapPoint = geometry as MapPoint;
					returnString += myPoint.y + " " + myPoint.x;
				}
			}
			
			return returnString;
		}
		
		private static function extentToGeoRSS(extent:Extent):String
		{
			return extent.ymin + " " + extent.xmin + " " + extent.ymax + " " + extent.xmax;
		}
		
		/**
		 * Returns the destination point from a point having travelled the given distance (in m) on the
		 * given initial bearing (bearing may vary before destination is reached)
		 *
		 *   see http://williams.best.vwh.net/avform.htm#LL
		 *   see http://www.movable-type.co.uk/scripts/latlong.html
		 *
		 * @param   {Number} lat: Initial latitude
		 * @param   {Number} lon: Initial longitude
		 * @param   {Number} bearing: Initial bearing in degrees
		 * @param   {Number} distance: Distance in m
		 * @returns {MapPoint} Destination point
		 */
		private static function calculateDestinationPoint(lat:Number, lon:Number, distance:Number, bearing:Number):MapPoint
		{
			var dist:Number= distance / EARTHRADIUS;
			var brng:Number=toRadians(bearing);
			
			var lat1:Number=toRadians(lat);
			var lon1:Number=toRadians(lon);
			
			var lat2:Number=Math.asin(Math.sin(lat1) * Math.cos(dist) + Math.cos(lat1) * Math.sin(dist) * Math.cos(brng));
			var lon2:Number=lon1 + Math.atan2(Math.sin(brng) * Math.sin(dist) * Math.cos(lat1), Math.cos(dist) - Math.sin(lat1) * Math.sin(lat2));
			
			lon2=(lon2 + 3 * Math.PI) % (2 * Math.PI) - Math.PI; // normalise to -180...+180
			
			return new MapPoint(toDegrees(lon2), toDegrees(lat2));
		}
		
		private static function toRadians(number:Number):Number
		{
			return number * Math.PI / 180;
		}
		
		private static function toDegrees(number:Number):Number
		{
			return number * 180 / Math.PI;
		}
	}
}

class SingletonEnforcer{}