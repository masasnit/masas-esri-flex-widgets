package MASAS.feedlib.utilities
{
	import com.adobe.utils.DateUtil;
	import com.esri.ags.geometry.Geometry;
	import com.esri.ags.geometry.MapPoint;
	import com.esri.ags.geometry.Polygon;
	import com.esri.ags.geometry.Polyline;

	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.utils.StringUtil;
	
	public class CapUtil
	{
		
		private static const GEORSS:Namespace=Namespaces.GEORSS_NS
		private static const ATOM:Namespace=Namespaces.ATOM_NS;
		private static const XHTML:Namespace=Namespaces.XHTML_NS;
		private static const XMLNS:Namespace=new Namespace("http://www.w3.org/XML/1998/namespace");
		private static const CAPv10:Namespace=new Namespace("http://www.incident.com/cap/1.0");
		private static const CAPv11:Namespace=new Namespace("urn:oasis:names:tc:emergency:cap:1.1");
		private static const CAPv12:Namespace=new Namespace("urn:oasis:names:tc:emergency:cap:1.2");
		
		private static const PRL:Namespace=new Namespace("http://purl.org/atompub/age/1.0");
		
		private static const GEO:Namespace=Namespaces.GEO_NS;
		private static const GML:Namespace=Namespaces.GML_NS;
		private static const GEOLL:Namespace=Namespaces.GEO_LL;
		
		private static const EARTHRADIUS:Number=6371.0;
		
		public function CapUtil(singletonEnforcer:SingletonEnforcer){}
		
		public static function toGeometry(x:XML):ArrayCollection
		{
			var returnCollection:ArrayCollection=new ArrayCollection();
			
			var infoList:XMLList=XMLList(x.CAPv11::info);
			
			if (infoList && infoList.length() > 0)
			{
				// Parameters for CAPAN Event location layer
				
				var paramList:XMLList=XMLList(infoList[0].CAPv11::parameter);
				
				for (var h:int=0; h < paramList.length(); h++)
				{
					var paramValueName:String=String(paramList[h].CAPv11::valueName);
					var paramValue:String=String(paramList[h].CAPv11::value);
					
					if (paramValue)
					{
						if (paramValueName == "layer:CAPAN:eventLocation:point")
						{
							returnCollection.addItem(parseGeoRSSPoint(paramValue));
						}
						else if (paramValueName == "layer:CAPAN:eventLocation:line")
						{
							returnCollection.addItem(parseGeoRSSLine(paramValue));
						}
						else if (paramValueName == "layer:CAPAN:eventLocation:polygon")
						{
							returnCollection.addItem(parseGeoRSSPolygon(paramValue));
						}
						else if (paramValueName == "layer:CAPAN:eventLocation:circle")
						{
							returnCollection.addItem(parseGeoRSSCircle(paramValue));
						}
					}
				}
				
				// Area Block
				
				var areaList:XMLList=XMLList(infoList[0].CAPv11::area);
				
				for (var i:int=0; i < areaList.length(); i++)
				{
					// Circles
					
					var circleList:XMLList=XMLList(areaList[i].CAPv11::circle);
					
					for (var j:int=0; j < circleList.length(); j++)
					{
						var georssCircle:String=circleList[j].toString();
						
						if (georssCircle)
						{
							returnCollection.addItem(parseGeoRSSCircle(georssCircle));
						}
					}
					
					// Polygons
					
					var polyList:XMLList=XMLList(areaList[i].CAPv11::polygon);
					
					for (var k:int=0; k < polyList.length(); k++)
					{
						var georssPolygon:String=polyList[k].toString();
						
						if (georssPolygon)
						{
							returnCollection.addItem(parseGeoRSSPolygon(georssPolygon));
						}
					}
				}
			}
			
			return returnCollection;
		}
		
		// Example 46.56,-76.18 250
		public static function parseGeoRSSCircle(text:String):Geometry
		{
			var path:Array=[];
			
			var tokens:Array=StringUtil.trim(text).split(" ");
			var coordinates:Array=StringUtil.trim(tokens[0]).split(",");
			
			var radius:Number=Number(tokens[1]);
			var lat:Number=Number(coordinates[0]);
			var lon:Number=Number(coordinates[1]);
			
			// Push 36 points, one every 10 degrees, to create the circle...
			// Could also use geometry service
			for (var i:int=0; i < 360; i=i + 10)
			{
				path.push(calculateDestinationPoint(lat, lon, radius, i));
			}
			
			return new Polygon([path]);
		}
		
		/**
		 * Returns the destination point from a point having travelled the given distance (in km) on the
		 * given initial bearing (bearing may vary before destination is reached)
		 *
		 *   see http://williams.best.vwh.net/avform.htm#LL
		 *   see http://www.movable-type.co.uk/scripts/latlong.html
		 *
		 * @param   {Number} lat: Initial latitude
		 * @param   {Number} lon: Initial longitude
		 * @param   {Number} bearing: Initial bearing in degrees
		 * @param   {Number} distance: Distance in km
		 * @returns {MapPoint} Destination point
		 */
		private static function calculateDestinationPoint(lat:Number, lon:Number, distance:Number, bearing:Number):MapPoint
		{
			var dist:Number=distance / EARTHRADIUS;
			var brng:Number=toRadians(bearing);
			
			var lat1:Number=toRadians(lat);
			var lon1:Number=toRadians(lon);
			
			var lat2:Number=Math.asin(Math.sin(lat1) * Math.cos(dist) + Math.cos(lat1) * Math.sin(dist) * Math.cos(brng));
			var lon2:Number=lon1 + Math.atan2(Math.sin(brng) * Math.sin(dist) * Math.cos(lat1), Math.cos(dist) - Math.sin(lat1) * Math.sin(lat2));
			
			lon2=(lon2 + 3 * Math.PI) % (2 * Math.PI) - Math.PI; // normalise to -180...+180
			
			return new MapPoint(toDegrees(lon2), toDegrees(lat2));
		}
		
		/**
		 * Returns the distance from the start point to the end point, in km 
		 * (using Haversine formula)
		 *
		 * 		from: Haversine formula - R. W. Sinnott, "Virtues of the Haversine",
		 *       Sky and Telescope, vol 68, no 2, 1984
		 * 
		 * 		see http://www.movable-type.co.uk/scripts/latlong.html
		 *
		 * @param   {MapPoint} Start point
		 * @param   {Mappoint} End point
		 * @returns {Number} Distance in km between the start point and end point
		 */		
		private function calculateDistanceBetween(startPoint:MapPoint, endPoint:MapPoint):Number
		{
			var lat1:Number = toRadians(startPoint.y);
			var lon1:Number = toRadians(startPoint.x);
			
			var lat2:Number = toRadians(endPoint.y);
			var lon2:Number = toRadians(endPoint.x);
			
			var dLat:Number = lat2 - lat1;
			var dLon:Number = lon2 - lon1;
			
			var a:Number = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(lat1) * Math.cos(lat2) * Math.sin(dLon/2) * Math.sin(dLon/2);
			var c:Number = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
			
			var distance:Number = EARTHRADIUS * c;
			
			return distance;				
		}
		
		public static function parseGeoRSSPoint(text:String):Geometry
		{
			var tokens:Array=StringUtil.trim(text).split(",");
			var lat:Number=Number(tokens[0]);
			var lon:Number=Number(tokens[1]);
			return new MapPoint(lon, lat);
		}
		
		public static function parseGeoRSSLine(text:String):Geometry
		{
			var path:Array=[];
			var tokens:Array=StringUtil.trim(text).split(" ");
			if (tokens.length > 3)
			{
				for (var i:int=0, j:int=1; j < tokens.length; i+=2, j+=2)
				{
					var lat:Number=Number(tokens[i]);
					var lon:Number=Number(tokens[j]);
					path.push(new MapPoint(lon, lat));
				}
			}
			return new Polyline([path]);
		}
		
		public static function parseGeoRSSPolygon(text:String):Geometry
		{
			var path:Array=[];
			var tokens:Array=StringUtil.trim(text).split(" ");
			
			for (var i:int=0; i < tokens.length; i++)
			{
				var coordinates:Array=StringUtil.trim(tokens[i]).split(",");
				
				for (var j:int=0, k:int=1; j < coordinates.length; j+=2, k+=2)
				{
					var lat:Number=Number(coordinates[j]);
					var lon:Number=Number(coordinates[k]);
					path.push(new MapPoint(lon, lat));
				}
			}
			
			return new Polygon([path]);
		}
		
		private static function toRadians(number:Number):Number
		{
			return number * Math.PI / 180;
		}
		
		private static function toDegrees(number:Number):Number
		{
			return number * 180 / Math.PI;
		}
	}
}

class SingletonEnforcer{}