package MASAS.feedlib.utilities
{
	import MASAS.feedlib.cap.enumerations.CapElement;
	import MASAS.feedlib.cap.supportclasses.CapElementTranslation;
	import MASAS.feedlib.cap.supportclasses.CapTranslationValue;

	public class CapTranslator
	{
	
		
		private var _capXML:XML;
		
		//This class looks up values from an XML file of translations based on the xml file's elements and their attribute values. 
		//It is NOT a French->English / English->French translator per se.
		public function CapTranslator(capTranslations:XML)
		{
			if (!capTranslations)
				throw new Error(ResourceHelper.getErrorString(1, ResourceHelper.getCurrentLocale, ["capTranslations", "CapTranslator()"]));
			
			_capXML=capTranslations;
		}
		
		public function getCapTranslation(capElement:CapElement):CapElementTranslation
		{
			var returnValue:CapElementTranslation=new CapElementTranslation(capElement);
			returnValue.translations=getTranslationValues(capElement);
			return returnValue;
		}
		
		//For example, if you have the string 'Réception' or "Acknowledge', you can return 'Ack' for CapElement.msgType.
		//Not sure this is useful!
		public function getCapElementIDByString(trans:CapElementTranslation, searchString:String):String
		{
			if (!trans)
				throw new Error(ResourceHelper.getErrorString(1, ResourceHelper.getCurrentLocale, ["trans", "CapTranslator::getCapElementIDByString"]));
			
			var values:Vector.<CapTranslationValue>=trans.translations;
			var value:CapTranslationValue;
			for (var i:int=0; i < values.length; i++)
			{
				value=values[i];
				if ((value.valueEN.toLowerCase() == searchString.toLowerCase()) || (value.valueFR.toLowerCase() == searchString.toLowerCase()))
					return value.id;
			}
			
			//If we did not find it, return null
			return null;
		}
		
		//Gets all values for the specified CapElement in the specified language
		private function getTranslationValues(capElement:CapElement):Vector.<CapTranslationValue>
		{
			if (!capElement)
				throw new Error(ResourceHelper.getErrorString(1, ResourceHelper.getCurrentLocale, ["capElement", "CapTranslator::getTranslationValues"]));
			
			var returnValue:Vector.<CapTranslationValue>=new Vector.<CapTranslationValue>();
			
			//Get the <cap_element> based on the string value of the passed in CapElement
			var elementXML:XMLList=_capXML..cap_element.(@name == capElement.toString());
			if (elementXML)
			{
				var capValues:XMLList=elementXML..cap_value;
				var trans:CapTranslationValue;
				for each (var valueXML:XML in capValues)
				{
					trans=new CapTranslationValue();
					trans.id=(valueXML.@id);
					trans.valueEN=valueXML..en_CA.@disp;
					trans.valueFR=valueXML..fr_CA.@disp;
					trans.descriptionEN=valueXML..en_CA.@tip;
					trans.descriptionFR=valueXML..fr_CA.@tip;
					returnValue.push(trans);
				}
			}
			
			if (returnValue.length > 0)
				return returnValue;
			else
				return null;
		}
	}
}