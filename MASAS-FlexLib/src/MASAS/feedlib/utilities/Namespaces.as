package MASAS.feedlib.utilities
{
    
    public class Namespaces
    {
        
        public static const RDF_NS          : Namespace = new Namespace( "http://www.w3.org/1999/02/22-rdf-syntax-ns#" );
        public static const DC_NS           : Namespace = new Namespace( "http://purl.org/dc/elements/1.1/" );
        public static const SY_NS           : Namespace = new Namespace( "http://purl.org/rss/1.0/modules/syndication/" );
        public static const CO_NS           : Namespace = new Namespace( "http://purl.org/rss/1.0/modules/company/" );
        public static const TI_NS           : Namespace = new Namespace( "http://purl.org/rss/1.0/modules/textinput/" );
        public static const RSS_NS          : Namespace = new Namespace( "http://purl.org/rss/1.0/" );
        
        public static const ATOM_NS         : Namespace = new Namespace( "http://www.w3.org/2005/Atom" );
        public static const ATOM_03_NS      : Namespace = new Namespace( "http://purl.org/atom/ns#" );
        
        public static const ATOM_PUB_DATE   : Namespace = new Namespace( "http://purl.org/atompub/age/1.0" );
        public static const ATOM_PUB_DOC    : Namespace = new Namespace( "http://www.w3.org/2007/app" );
        
        public static const XHTML_NS        : Namespace = new Namespace( "http://www.w3.org/1999/xhtml" );
        public static const CONTENT_NS      : Namespace = new Namespace( "http://purl.org/rss/1.0/modules/content/" );
        
        public static const GEORSS_NS       : Namespace = new Namespace( "http://www.georss.org/georss" );
        
        public static const GEO_NS          : Namespace = new Namespace( "http://www.w3.org/2003/01/geo/" );
        public static const GML_NS          : Namespace = new Namespace( "http://www.opengis.net/gml" );
        public static const GEO_LL          : Namespace = new Namespace( "http://www.w3.org/2003/01/geo/wgs84_pos#" );
        
        public static const APP_NS          : Namespace = new Namespace( "app", "http://www.w3.org/2007/app" );
        
        public static const MEC_NS          : Namespace = new Namespace( "mec", "masas:extension:control" );

        public static const MEA_NS          : Namespace = new Namespace( "mea", "masas:experimental:attribute" );
        public static const MET_NS          : Namespace = new Namespace( "met", "masas:experimental:time" );
        
        public function Namespaces( singletonEnforcer : SingletonEnforcer )
        {
        }
        
    }
    
}

class SingletonEnforcer{}
