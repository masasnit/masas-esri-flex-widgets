package MASAS.feedlib.utilities
{
    import mx.resources.IResourceManager;
    import mx.resources.ResourceManager;
    
    [ResourceBundle("feedlib_errors")]
    
    public class ResourceHelper
    {
        /**
         * Singleton constructor should be private but current AS3.0 does not allow it.
         *
         * Restrict the constructor by passing a internal SingletonEnforcer class instance.
         * This internal class will be visible only to the ResourceHelper singleton class.
         *
         */
        public function ResourceHelper(singletonEnforcer:SingletonEnforcer)
        {
            // Guard against passing a null SingletonEnforcer 
            if (singletonEnforcer == null)
            {
                throw new Error("Cannot use the constructor to create an instance of ResourceHelper.");
            }
        }
        
        private static var _instance:ResourceHelper;
        
        
        public static function get getInstance():ResourceHelper
        {
            if (_instance == null)
            {
                _instance=new ResourceHelper(new SingletonEnforcer());
            }
            return _instance;
        }
        
        public static function get getCurrentLocale():SupportedLocale
        {
            var returnValue:SupportedLocale=SupportedLocale.en_US; //default 	
            var resourceManager:IResourceManager=ResourceManager.getInstance();
            switch (resourceManager.localeChain[0].toString())
            {
                case "en_CA":
                    returnValue=SupportedLocale.en_CA;
                    break;
                case "fr_CA":
                    returnValue=SupportedLocale.fr_CA;
                    break;
            }
            
            return returnValue;
        }
        
        public static function getString(value:String, lang:SupportedLocale, params:Array=null):String
        {
            var resourceManager:IResourceManager=ResourceManager.getInstance();
            var returnValue:String;
            
            switch (lang)
            {
                case SupportedLocale.fr_CA:
                    returnValue=resourceManager.getString("feedlib", value, params, "fr_CA");
                    if (!returnValue)
                    {
                        //Try to find en_US because most Flex Viewers aren't compiled to en_CA or fr_CA ... 
                        returnValue=resourceManager.getString("feedlib", value, params, "en_US");
                        if ((!returnValue) || (returnValue.toLowerCase() == "undefined"))
                        {
                            returnValue="Ressource non trouvée: " + value;
                        }
                    }
                    break;
                case SupportedLocale.en_CA:
                    returnValue=resourceManager.getString("feedlib", value, params, "en_CA");
                    if ((!returnValue) || (returnValue.toLowerCase() == "undefined"))
                    {
                        returnValue="Resource not found: " + value;
                    }
                    break;
                case SupportedLocale.en_US:
                    returnValue=resourceManager.getString("feedlib", value, params, "en_US");
                    if ((!returnValue) || (returnValue.toLowerCase() == "undefined"))
                    {
                        returnValue="Resource not found: " + value;
                    }
                    break;
                default:
                    returnValue=resourceManager.getString("feedlib", value, params, "en_CA"); //for now 
            }
            return returnValue;
        }
        
        public static function getErrorString(errorNumber:int, lang:SupportedLocale, params:Array=null):String
        {
            var resourceManager:IResourceManager=ResourceManager.getInstance();
            var returnValue:String;
            var value:String=errorNumber.toString();
            
            switch (lang)
            {
                case SupportedLocale.fr_CA:
                    returnValue=resourceManager.getString("feedlib_errors", value, params, "fr_CA");
                    if (!returnValue)
                    {
                        //Try to find en_US because most Flex Viewers aren't compiled to en_CA or fr_CA ... 
                        returnValue=resourceManager.getString("feedlib_errors", value, params, "en_US");
                        if ((!returnValue) || (returnValue.toLowerCase() == "undefined"))
                        {
                            returnValue="Numéro de message d'erreur inconnu: " + value;
                        }
                    }
                    break;
                case SupportedLocale.en_CA:
                    returnValue=resourceManager.getString("feedlib_errors", value, params, "en_CA");
                    if ((!returnValue) || (returnValue.toLowerCase() == "undefined"))
                    {
                        returnValue="Unknown error number: " + value;
                    }
                    break;
                case SupportedLocale.en_US:
                    returnValue=resourceManager.getString("feedlib_errors", value, params, "en_US");
                    if ((!returnValue) || (returnValue.toLowerCase() == "undefined"))
                    {
                        returnValue="Unknown error number: " + value;
                    }
                    break;
                default:
                    returnValue=resourceManager.getString("feedlib_errors", value, params, "en_CA"); //for now 
            }
            return returnValue;
        }
    }
}

class SingletonEnforcer{}