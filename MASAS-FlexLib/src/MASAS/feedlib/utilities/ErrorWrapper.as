package MASAS.feedlib.utilities
{
	//Pure wrapper class. No implementation. Used (for example) by AlertMsgType to get past compiler errors. 
	public class ErrorWrapper extends Error
	{
		public function ErrorWrapper(message:*="", id:*=0)
		{
			super(message, id); 
		}
	}
}