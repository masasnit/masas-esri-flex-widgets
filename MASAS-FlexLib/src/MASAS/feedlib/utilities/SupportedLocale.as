package MASAS.feedlib.utilities
{
	import mx.utils.StringUtil;
	
	public final class SupportedLocale
	{
		/*
		* The actual enumeration values. Note that the sorting order doesn't matter.
		*/
		public static const en_CA:SupportedLocale=new SupportedLocale(en_CA);
		public static const fr_CA:SupportedLocale=new SupportedLocale(fr_CA);
		public static const en_US:SupportedLocale=new SupportedLocale(en_US);
		public static const unknown:SupportedLocale=new SupportedLocale(unknown);
		
		/* Constructor */
		public function SupportedLocale(e:SupportedLocale)
		{
			/*
			* Empty fake constructor
			* The part that matter is that it accepts a 'copy' of itself
			* For the recursive assignment to work
			*/
		}
		
		public function toString():String
		{
			switch (this)
			{
				case SupportedLocale.en_CA:
					return "en-CA";
					break;
				case SupportedLocale.fr_CA:
					return "fr-CA";
					break;
				case SupportedLocale.en_US:
					return "en-US";
					break;
				case SupportedLocale.unknown:
					return "Unknown";
					break;
				default:
					return null;
			}
		}
		
		public static function fromString(value:String):SupportedLocale
		{
			var returnValue:SupportedLocale;
			try
			{
				switch (StringUtil.trim(value).toLowerCase())
				{
					case "en-ca":
					case "en_ca":
						returnValue=SupportedLocale.en_CA;
						break;
					case "fr-ca":
					case "fr_ca":
						returnValue=SupportedLocale.fr_CA;
						break;
					case "en-us":
					case "en_us":
						returnValue=SupportedLocale.en_US;
						break;				
					default:
						returnValue=null;
				}
			}
			catch (e:Error)
			{
				returnValue=null;
			}
			
			return returnValue;
		}
	}
}