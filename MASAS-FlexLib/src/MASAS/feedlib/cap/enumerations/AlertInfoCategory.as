package MASAS.feedlib.cap.enumerations
{
	import MASAS.feedlib.utilities.ResourceHelper;

	public final class AlertInfoCategory
	{
		import mx.utils.StringUtil;
		
		public static const Geo:AlertInfoCategory=new AlertInfoCategory(Geo);
		public static const Met:AlertInfoCategory=new AlertInfoCategory(Met);
		public static const Safety:AlertInfoCategory=new AlertInfoCategory(Safety);
		public static const Security:AlertInfoCategory=new AlertInfoCategory(Security);
		public static const Rescue:AlertInfoCategory=new AlertInfoCategory(Rescue);
		public static const Fire:AlertInfoCategory=new AlertInfoCategory(Fire);
		public static const Health:AlertInfoCategory=new AlertInfoCategory(Health);
		public static const Env:AlertInfoCategory=new AlertInfoCategory(Env);
		public static const Transport:AlertInfoCategory=new AlertInfoCategory(Transport);
		public static const Infra:AlertInfoCategory=new AlertInfoCategory(Infra);
		public static const CBRNE:AlertInfoCategory=new AlertInfoCategory(CBRNE);
		public static const Other:AlertInfoCategory=new AlertInfoCategory(Other);
		
		public function AlertInfoCategory(a:AlertInfoCategory){}
		
		public static function get values():Vector.<String>
		{
			var v:Vector.<String>=new Vector.<String>();
			
			v.push(AlertInfoCategory.CBRNE, AlertInfoCategory.Env, AlertInfoCategory.Fire, AlertInfoCategory.Geo, AlertInfoCategory.Health, AlertInfoCategory.Infra, AlertInfoCategory.Met, AlertInfoCategory.Other, AlertInfoCategory.Rescue, AlertInfoCategory.Safety, AlertInfoCategory.Security, AlertInfoCategory.Transport);
			
			return v;
		}
		
		public static function fromString(value:String):AlertInfoCategory
		{
			var returnValue:AlertInfoCategory;
			try
			{
				switch (StringUtil.trim(value).toLowerCase())
				{
					case "geo":
						return AlertInfoCategory.Geo;
						break;
					case "met":
						return AlertInfoCategory.Met;
						break;
					case "safety":
						return AlertInfoCategory.Safety;
						break;
					case "security":
						return AlertInfoCategory.Security;
						break;
					case "rescue":
						return AlertInfoCategory.Rescue;
						break;
					case "fire":
						return AlertInfoCategory.Fire;
						break;
					case "health":
						return AlertInfoCategory.Health;
						break;
					case "env":
						return AlertInfoCategory.Env;
						break;
					case "transport":
						return AlertInfoCategory.Transport;
						break;
					case "infra":
						return AlertInfoCategory.Infra;
						break;
					case "cbrne":
						return AlertInfoCategory.CBRNE;
						break;
					case "other":
						return AlertInfoCategory.Other;
						break;
					default:
						throw new Error(ResourceHelper.getErrorString(10, ResourceHelper.getCurrentLocale, [value, "AlertInfoCategory::fromString"]));					
				}
			}
			catch (e:Error)
			{
				throw new Error(ResourceHelper.getErrorString(9, ResourceHelper.getCurrentLocale, [e.message.toString(), "AlertInfoCategory::fromString"]));			
			}
			
			return returnValue;
		}
		
		public function toString():String
		{
			switch (this)
			{
				case AlertInfoCategory.Geo:
					return "Geo";
					break;
				case AlertInfoCategory.Met:
					return "Met";
					break;
				case AlertInfoCategory.Safety:
					return "Safety";
					break;
				case AlertInfoCategory.Security:
					return "Security";
					break;
				case AlertInfoCategory.Rescue:
					return "Rescue";
					break;
				case AlertInfoCategory.Fire:
					return "Fire";
					break;
				case AlertInfoCategory.Health:
					return "Health";
					break;
				case AlertInfoCategory.Env:
					return "Env";
					break;
				case AlertInfoCategory.Transport:
					return "Transport";
					break;
				case AlertInfoCategory.Infra:
					return "Infra";
					break;
				case AlertInfoCategory.CBRNE:
					return "CBRNE";
					break;
				case AlertInfoCategory.Other:
					return "Other";
					break;
				default:
					return null;
			}
		}
	}
}