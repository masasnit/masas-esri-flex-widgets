package MASAS.feedlib.cap.enumerations
{
	import MASAS.feedlib.utilities.ResourceHelper;
	
	public final class CapElement
	{
		import mx.utils.StringUtil;
		
		public static const certainty:CapElement=new CapElement(certainty);
		public static const msgType:CapElement=new CapElement(msgType);
		public static const responseType:CapElement=new CapElement(responseType);
		public static const urgency:CapElement=new CapElement(urgency);
		public static const category:CapElement=new CapElement(category);
		public static const scope:CapElement=new CapElement(scope);
		public static const severity:CapElement=new CapElement(severity);
		public static const status:CapElement=new CapElement(status);
		
		public function CapElement(c:CapElement){}
		
		public static function get values():Vector.<String>
		{
			var v:Vector.<String>=new Vector.<String>();
			v.push(CapElement.category, CapElement.certainty, CapElement.msgType, CapElement.responseType, CapElement.scope, CapElement.severity, CapElement.severity, CapElement.status, CapElement.urgency);
			return v;
		}
		
		public static function fromString(value:String):CapElement
		{
			var returnValue:CapElement;
			try
			{
				switch (StringUtil.trim(value).toLowerCase())
				{
					case "certainty":
						returnValue=CapElement.certainty;
						break;
					case "msgType":
						returnValue=CapElement.msgType;
						break;
					case "responseType":
						returnValue=CapElement.responseType;
						break;
					case "urgency":
						returnValue=CapElement.urgency;
						break;
					case "category":
						returnValue=CapElement.category;
						break;
					case "scope":
						returnValue=CapElement.scope;
						break;
					case "severity":
						returnValue=CapElement.severity;
						break;
					case "status":
						returnValue=CapElement.status;
						break;
					default:
						throw new Error(ResourceHelper.getErrorString(10, ResourceHelper.getCurrentLocale, [value, "CapElement::fromString"]));
				}
			}
			catch (e:Error)
			{
				throw new Error(ResourceHelper.getErrorString(9, ResourceHelper.getCurrentLocale, [e.message.toString(), "CapElement::fromString"]));
			}
			
			return returnValue;
		}
		
		public function toString():String
		{
			switch (this)
			{
				case CapElement.certainty:
					return "certainty";
					break;
				case CapElement.msgType:
					return "msgType";
					break;
				case CapElement.responseType:
					return "responseType";
					break;
				case CapElement.urgency:
					return "urgency";
					break;
				case CapElement.category:
					return "category";
					break;
				case CapElement.scope:
					return "scope";
					break;
				case CapElement.severity:
					return "severity";
					break;
				case CapElement.status:
					return "status";
					break;
				default:
					return null;
			}
		}
	}
}