package MASAS.feedlib.cap.enumerations
{
	import MASAS.feedlib.utilities.ResourceHelper;
	
	public final class AlertScope
	{
		import mx.utils.StringUtil;
		
		public static const Public:AlertScope=new AlertScope(Public);
		public static const Restricted:AlertScope=new AlertScope(Restricted);
		public static const Private:AlertScope=new AlertScope(Private);
		
		public function AlertScope(a:AlertScope){}
		
		public static function get values():Vector.<String>
		{
			var v:Vector.<String>=new Vector.<String>();
			v.push(AlertScope.Private, AlertScope.Public, AlertScope.Restricted);
			return v;
		}
		
		public static function fromString(value:String):AlertScope
		{
			var returnValue:AlertScope;
			try
			{
				switch (StringUtil.trim(value).toLowerCase())
				{
					case "public":
						returnValue=AlertScope.Public;
						break;
					case "restricted":
						returnValue=AlertScope.Restricted;
						break;
					case "private":
						returnValue=AlertScope.Private;
						break;
					default:
						throw new Error(ResourceHelper.getErrorString(10, ResourceHelper.getCurrentLocale, [value, "AlertScope::fromString"]));
				}
			}
			catch (e:Error)
			{
				throw new Error(ResourceHelper.getErrorString(9, ResourceHelper.getCurrentLocale, [e.message.toString(), "AlertScope::fromString"]));
			}
			
			return returnValue;
		}
		
		public function toString():String
		{
			switch (this)
			{
				case AlertScope.Public:
					return "Public";
					break;
				case AlertScope.Restricted:
					return "Restricted";
					break;
				case AlertScope.Private:
					return "Private";
					break;
				default:
					return null;
			}
		}
	}
}