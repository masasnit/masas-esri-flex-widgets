package MASAS.feedlib.cap.enumerations
{
	import MASAS.feedlib.utilities.ResourceHelper;
	
	public final class AlertInfoResponseType
	{
		import mx.utils.StringUtil;
		
		public static const Shelter:AlertInfoResponseType=new AlertInfoResponseType(Shelter);
		public static const Evacuate:AlertInfoResponseType=new AlertInfoResponseType(Evacuate);
		public static const Prepare:AlertInfoResponseType=new AlertInfoResponseType(Prepare);
		public static const Execute:AlertInfoResponseType=new AlertInfoResponseType(Execute);
		public static const Avoid:AlertInfoResponseType=new AlertInfoResponseType(Avoid);
		public static const Monitor:AlertInfoResponseType=new AlertInfoResponseType(Monitor);
		public static const Assess:AlertInfoResponseType=new AlertInfoResponseType(Assess);
		public static const AllClear:AlertInfoResponseType=new AlertInfoResponseType(AllClear);
		public static const None:AlertInfoResponseType=new AlertInfoResponseType(None);
		
		public function AlertInfoResponseType(a:AlertInfoResponseType){}
		
		public static function get values():Vector.<String>
		{
			var v:Vector.<String>=new Vector.<String>();
			v.push(AlertInfoResponseType.AllClear, AlertInfoResponseType.Assess, AlertInfoResponseType.Avoid, AlertInfoResponseType.Evacuate, AlertInfoResponseType.Execute, AlertInfoResponseType.Monitor, AlertInfoResponseType.None, AlertInfoResponseType.Prepare, AlertInfoResponseType.Prepare, AlertInfoResponseType.Shelter);
			return v;
		}
		
		public static function fromString(value:String):AlertInfoResponseType
		{
			var returnValue:AlertInfoResponseType;
			try
			{
				switch (StringUtil.trim(value).toLowerCase())
				{
					case "shelter":
						return AlertInfoResponseType.Shelter;
						break;
					case "evacuate":
						return AlertInfoResponseType.Evacuate;
						break;
					case "prepare":
						return AlertInfoResponseType.Prepare;
						break;
					case "execute":
						return AlertInfoResponseType.Execute;
						break;
					case "avoid":
						return AlertInfoResponseType.Avoid;
						break;
					case "monitor":
						return AlertInfoResponseType.Monitor;
						break;
					case "assess":
						return AlertInfoResponseType.Assess;
						break;
					case "allclear":
						return AlertInfoResponseType.AllClear;
						break;
					case "none":
						return AlertInfoResponseType.None;
						break;
					default:
						throw new Error(ResourceHelper.getErrorString(10, ResourceHelper.getCurrentLocale, [value, "AlertInfoResponseType::fromString"]));
				}
			}
			catch (e:Error)
			{
				throw new Error(ResourceHelper.getErrorString(9, ResourceHelper.getCurrentLocale, [e.message.toString(), "AlertInfoResponseType::fromString"]));
			}
			
			return returnValue;
		}
		
		public function toString():String
		{
			switch (this)
			{
				case AlertInfoResponseType.Shelter:
					return "Shelter";
					break;
				case AlertInfoResponseType.Evacuate:
					return "Evacuate";
					break;
				case AlertInfoResponseType.Prepare:
					return "Prepare";
					break;
				case AlertInfoResponseType.Execute:
					return "Execute";
					break;
				case AlertInfoResponseType.Avoid:
					return "Avoid";
					break;
				case AlertInfoResponseType.Monitor:
					return "Monitor";
					break;
				case AlertInfoResponseType.Assess:
					return "Assess";
					break;
				case AlertInfoResponseType.AllClear:
					return "AllClear";
					break;
				case AlertInfoResponseType.None:
					return "None";
					break;
				default:
					return null;
			}
		}
	}
}