package MASAS.feedlib.cap.enumerations
{
	import MASAS.feedlib.utilities.ResourceHelper;
	
	public final class AlertInfoSeverity
	{
		import mx.utils.StringUtil;
		
		public static const Extreme:AlertInfoSeverity=new AlertInfoSeverity(Extreme);
		public static const Severe:AlertInfoSeverity=new AlertInfoSeverity(Severe);
		public static const Moderate:AlertInfoSeverity=new AlertInfoSeverity(Moderate);
		public static const Minor:AlertInfoSeverity=new AlertInfoSeverity(Minor);
		public static const Unknown:AlertInfoSeverity=new AlertInfoSeverity(Unknown);
		
		public function AlertInfoSeverity(a:AlertInfoSeverity){}
		
		public static function get values():Vector.<String>
		{
			var v:Vector.<String>=new Vector.<String>();
			v.push(AlertInfoSeverity.Extreme, AlertInfoSeverity.Severe, AlertInfoSeverity.Moderate, AlertInfoSeverity.Minor, AlertInfoSeverity.Unknown);
			return v;
		}
		
		public static function fromString(value:String):AlertInfoSeverity
		{
			var returnValue:AlertInfoSeverity;
			try
			{
				switch (StringUtil.trim(value).toLowerCase())
				{
					case "extreme":
						return AlertInfoSeverity.Extreme;
						break;
					case "severe":
						return AlertInfoSeverity.Severe;
						break;
					case "moderate":
						return AlertInfoSeverity.Moderate;
						break;
					case "minor":
						return AlertInfoSeverity.Minor;
						break;
					case "unknown":
						return AlertInfoSeverity.Unknown;
						break;
					default:
						throw new Error(ResourceHelper.getErrorString(10, ResourceHelper.getCurrentLocale, [value, "AlertInfoSeverity::fromString"]));
				}
			}
			catch (e:Error)
			{
				throw new Error(ResourceHelper.getErrorString(9, ResourceHelper.getCurrentLocale, [e.message.toString(), "AlertInfoSeverity::fromString"]));
			}
			
			return returnValue;
		}
		
		public function toString():String
		{
			switch (this)
			{
				case AlertInfoSeverity.Extreme:
					return "Extreme";
					break;
				case AlertInfoSeverity.Severe:
					return "Severe";
					break;
				case AlertInfoSeverity.Moderate:
					return "Moderate";
					break;
				case AlertInfoSeverity.Minor:
					return "Minor";
					break;
				case AlertInfoSeverity.Unknown:
					return "Unknown";
					break;
				default:
					return null;
			}
		}
	}
}