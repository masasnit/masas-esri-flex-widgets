package MASAS.feedlib.cap.enumerations
{
	import MASAS.feedlib.utilities.ResourceHelper;
	
	public final class AlertStatus
	{
		import mx.utils.StringUtil;
		
		public static const Actual:AlertStatus=new AlertStatus(Actual);
		public static const Exercise:AlertStatus=new AlertStatus(Exercise);
		public static const System:AlertStatus=new AlertStatus(System);
		public static const Test:AlertStatus=new AlertStatus(Test);
		public static const Draft:AlertStatus=new AlertStatus(Draft);
		
		public function AlertStatus(a:AlertStatus){}
		
		public static function get values():Vector.<String>
		{
			var v:Vector.<String>=new Vector.<String>();
			v.push(AlertStatus.Actual, AlertStatus.Draft, AlertStatus.Exercise, AlertStatus.System, AlertStatus.Test);
			return v;
		}
		
		public static function fromString(value:String):AlertStatus
		{
			var returnValue:AlertStatus;
			try
			{
				switch (StringUtil.trim(value).toLowerCase())
				{
					case "actual":
						returnValue=AlertStatus.Actual;
						break;
					case "draft":
						returnValue=AlertStatus.Draft;
						break;
					case "exercise":
						returnValue=AlertStatus.Exercise;
						break;
					case "system":
						returnValue=AlertStatus.System;
						break;
					case "test":
						returnValue=AlertStatus.Test;
						break;
					default:
						throw new Error(ResourceHelper.getErrorString(10, ResourceHelper.getCurrentLocale, [value, "AlertStatus::fromString"]));
				}
			}
			catch (e:Error)
			{
				throw new Error(ResourceHelper.getErrorString(9, ResourceHelper.getCurrentLocale, [e.message.toString(), "AlertStatus::fromString"]));
			}
			
			return returnValue;
		}
		
		public function toString():String
		{
			switch (this)
			{
				case AlertStatus.Actual:
					return "Actual";
					break;
				case AlertStatus.Draft:
					return "Draft";
					break;
				case AlertStatus.Exercise:
					return "Exercise";
					break;
				case AlertStatus.System:
					return "System";
					break;
				case AlertStatus.Test:
					return "Test";
					break;
				default:
					return null;
			}
		}
	}
}