package MASAS.feedlib.cap.enumerations
{
	import MASAS.feedlib.utilities.ResourceHelper;
	
	public final class AlertInfoCertainty
	{
		import mx.utils.StringUtil;
		
		public static const Observed:AlertInfoCertainty=new AlertInfoCertainty(Observed);
		public static const Likely:AlertInfoCertainty=new AlertInfoCertainty(Likely);
		public static const Possible:AlertInfoCertainty=new AlertInfoCertainty(Possible);
		public static const Unlikely:AlertInfoCertainty=new AlertInfoCertainty(Unlikely);
		public static const Unknown:AlertInfoCertainty=new AlertInfoCertainty(Unknown);
		
		public function AlertInfoCertainty(a:AlertInfoCertainty){}
		
		public static function get values():Vector.<String>
		{
			var v:Vector.<String>=new Vector.<String>();
			v.push(AlertInfoCertainty.Observed, AlertInfoCertainty.Likely, AlertInfoCertainty.Possible, AlertInfoCertainty.Unlikely, AlertInfoCertainty.Unknown);
			return v;
		}
		
		public static function fromString(value:String):AlertInfoCertainty
		{
			var returnValue:AlertInfoCertainty;
			try
			{
				switch (StringUtil.trim(value).toLowerCase())
				{
					case "observed":
						return AlertInfoCertainty.Observed;
						break;
					case "likely":
						return AlertInfoCertainty.Likely;
						break;
					case "possible":
						return AlertInfoCertainty.Possible;
						break;
					case "unlikely":
						return AlertInfoCertainty.Unlikely;
						break;
					case "unknown":
						return AlertInfoCertainty.Unknown;
						break;
					default:
						throw new Error(ResourceHelper.getErrorString(10, ResourceHelper.getCurrentLocale, [value, "AlertInfoCertainty::fromString"]));
				}
			}
			catch (e:Error)
			{
				throw new Error(ResourceHelper.getErrorString(9, ResourceHelper.getCurrentLocale, [e.message.toString(), "AlertInfoCertainty::fromString"]));
			}
			
			return returnValue;
		}
		
		public function toString():String
		{
			switch (this)
			{
				case AlertInfoCertainty.Observed:
					return "Observed";
					break;
				case AlertInfoCertainty.Likely:
					return "Likely";
					break;
				case AlertInfoCertainty.Possible:
					return "Possible";
					break;
				case AlertInfoCertainty.Unlikely:
					return "Unlikely";
					break;
				case AlertInfoCertainty.Unknown:
					return "Unknown";
					break;
				default:
					return null;
			}
		}
	}
}