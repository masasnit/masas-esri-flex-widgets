package MASAS.feedlib.cap.enumerations
{
	import MASAS.feedlib.utilities.ResourceHelper;
	
	public final class CapVersion
	{
		import mx.utils.StringUtil;
	
		public static const v1_0:CapVersion=new CapVersion(v1_0);
		public static const v1_1:CapVersion=new CapVersion(v1_1);
		public static const v1_2:CapVersion=new CapVersion(v1_2);
		
		public function CapVersion(c:CapVersion){}
		
		public static function get values():Vector.<String>
		{
			var v:Vector.<String>=new Vector.<String>();
			v.push(CapVersion.v1_0,CapVersion.v1_1, CapVersion.v1_2);
			return v;
		}
		
		public static function fromString(value:String):CapVersion
		{
			var returnValue:CapVersion;
			try
			{
				switch (StringUtil.trim(value).toLowerCase())
				{
					case "http://www.incident.com/cap/1.0":
						returnValue=CapVersion.v1_0;
						break;
					case "urn:oasis:names:tc:emergency:cap:1.1":
						returnValue=CapVersion.v1_1;
						break;
					case "urn:oasis:names:tc:emergency:cap:1.2":
						returnValue=CapVersion.v1_2;
						break;				
					default:
						throw new Error(ResourceHelper.getErrorString(10, ResourceHelper.getCurrentLocale, [value, "CapVersion::fromString"]));
				}
			}
			catch (e:Error)
			{
				throw new Error(ResourceHelper.getErrorString(9, ResourceHelper.getCurrentLocale, [e.message.toString(), "CapVersion::fromString"]));
			}
			
			return returnValue;
		}
		
		public function toString():String
		{
			switch (this)
			{
				case CapVersion.v1_0:
					return "http://www.incident.com/cap/1.0";
					break;
				case CapVersion.v1_1:
					return "urn:oasis:names:tc:emergency:cap:1.1";
					break;
				case CapVersion.v1_2:
					return "urn:oasis:names:tc:emergency:cap:1.2";
					break;
				default:
					return null;
			}
		}
	}
}