package MASAS.feedlib.cap.enumerations
{
	import MASAS.feedlib.utilities.ResourceHelper;
	
	public final class AlertInfoUrgency
	{
		import mx.utils.StringUtil;
		
		public static const Immediate:AlertInfoUrgency=new AlertInfoUrgency(Immediate);
		public static const Expected:AlertInfoUrgency=new AlertInfoUrgency(Expected);
		public static const Future:AlertInfoUrgency=new AlertInfoUrgency(Future);
		public static const Past:AlertInfoUrgency=new AlertInfoUrgency(Past);
		public static const Unknown:AlertInfoUrgency=new AlertInfoUrgency(Unknown);
		
		public function AlertInfoUrgency(a:AlertInfoUrgency){}
		
		public static function get values():Vector.<String>
		{
			var v:Vector.<String>=new Vector.<String>();
			v.push(AlertInfoUrgency.Immediate, AlertInfoUrgency.Expected, AlertInfoUrgency.Future, AlertInfoUrgency.Past, AlertInfoUrgency.Unknown);
			return v;
		}
		
		public static function fromString(value:String):AlertInfoUrgency
		{
			var returnValue:AlertInfoUrgency;
			try
			{
				switch (StringUtil.trim(value).toLowerCase())
				{
					case "immediate":
						return AlertInfoUrgency.Immediate;
						break;
					case "expected":
						return AlertInfoUrgency.Expected;
						break;
					case "future":
						return AlertInfoUrgency.Future;
						break;
					case "past":
						return AlertInfoUrgency.Past;
						break;
					case "unknown":
						return AlertInfoUrgency.Unknown;
						break;
					default:
						throw new Error(ResourceHelper.getErrorString(10, ResourceHelper.getCurrentLocale, [value, "AlertInfoUrgency::fromString"]));
				}
			}
			catch (e:Error)
			{
				throw new Error(ResourceHelper.getErrorString(9, ResourceHelper.getCurrentLocale, [e.message.toString(), "AlertInfoUrgency::fromString"]));
			}
			
			return returnValue;
		}
		
		public function toString():String
		{
			switch (this)
			{
				case AlertInfoUrgency.Immediate:
					return "Immediate";
					break;
				case AlertInfoUrgency.Expected:
					return "Expected";
					break;
				case AlertInfoUrgency.Future:
					return "Future";
					break;
				case AlertInfoUrgency.Past:
					return "Past";
					break;
				case AlertInfoUrgency.Unknown:
					return "Unknown";
					break;
				default:
					return null;
			}
		}
	}
}