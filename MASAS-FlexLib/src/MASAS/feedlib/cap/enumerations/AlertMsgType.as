package MASAS.feedlib.cap.enumerations
{
	import MASAS.feedlib.utilities.ErrorWrapper;
	import MASAS.feedlib.utilities.ResourceHelper;
	
	public final class AlertMsgType
	{
		//import com.esricanada.MASAS.feedlib.utilities.ErrorWrapper; //this class exposes an enum/class 'Error'. This messes up the compiler
		import mx.utils.StringUtil;
		
		public static const Alert:AlertMsgType=new AlertMsgType(Alert);
		public static const Update:AlertMsgType=new AlertMsgType(Update);
		public static const Cancel:AlertMsgType=new AlertMsgType(Cancel);
		public static const Ack:AlertMsgType=new AlertMsgType(Ack);
		public static const Error:AlertMsgType=new AlertMsgType(Error);
		
		public function AlertMsgType(a:AlertMsgType){}
		
		public static function get values():Vector.<String>
		{
			var v:Vector.<String>=new Vector.<String>();
			v.push(AlertMsgType.Ack, AlertMsgType.Alert, AlertMsgType.Cancel, AlertMsgType.Error, AlertMsgType.Update);
			return v;
		}
		
		public static function fromString(value:String):AlertMsgType
		{
			var returnValue:AlertMsgType;
			try
			{
				switch (StringUtil.trim(value).toLowerCase())
				{
					case "alert":
						return AlertMsgType.Alert;
						break;
					case "update":
						return AlertMsgType.Update;
						break;
					case "cancel":
						return AlertMsgType.Cancel;
						break;
					case "ack":
						return AlertMsgType.Ack;
						break;
					case "error":
						return AlertMsgType.Error;
						break;
					default:
						throw new ErrorWrapper(ResourceHelper.getErrorString(10, ResourceHelper.getCurrentLocale, [value, "AlertMsgType::fromString"]));
				}
			}
			catch (e:ErrorWrapper) //use ErrorWrapper instead of ActionScript default package's Error. 
			{
				throw new ErrorWrapper(ResourceHelper.getErrorString(9, ResourceHelper.getCurrentLocale, [e.message.toString(), "AlertMsgType::fromString"]));
			}
			
			return returnValue;
		}
		
		public function toString():String
		{
			switch (this)
			{
				case AlertMsgType.Alert:
					return "Alert";
					break;
				case AlertMsgType.Update:
					return "Update";
					break;
				case AlertMsgType.Cancel:
					return "Cancel";
					break;
				case AlertMsgType.Ack:
					return "Ack";
					break;
				case AlertMsgType.Error:
					return "Error";
					break;
				default:
					return null;
			}
		}
	}
}