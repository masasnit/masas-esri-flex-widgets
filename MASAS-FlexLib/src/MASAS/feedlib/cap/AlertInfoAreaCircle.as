package MASAS.feedlib.cap
{
	import com.esri.ags.geometry.Polygon;
	
	import mx.utils.StringUtil;
	
	import MASAS.feedlib.utilities.CapUtil;
	import MASAS.feedlib.utilities.ResourceHelper;
	
	public class AlertInfoAreaCircle
	{
		public const MAX_RADIUS_KM:Number=6371;
		
		private var _circleInfo:String;
		private var _coordinates:Array;
		private var _radius:Number;
		private var _isValid:Boolean;
		
		public function AlertInfoAreaCircle(circleInfo:String)
		{
			if (!circleInfo)
				throw new Error(ResourceHelper.getErrorString(1, ResourceHelper.getCurrentLocale, ["circleInfo", "AlertInfoAreaCircle()"]));
			
			_isValid=isValid();
			if (!_isValid)
				throw new Error(ResourceHelper.getErrorString(0, ResourceHelper.getCurrentLocale, ["circleInfo", "AlertInfoAreaCircle()"]));
			
			_circleInfo = circleInfo;
		}
		
		internal function toXMLString():String
		{
			if (!_isValid)
				throw new Error(ResourceHelper.getErrorString(5, ResourceHelper.getCurrentLocale, ["AlertInfoAreaCircle::toXMLString"]));
			else 
				return "<circle>" + _circleInfo + "</circle>";
			
		}
		
		public function asPolygon():Polygon
		{

			return CapUtil.parseGeoRSSCircle(_circleInfo) as Polygon;
		}
		
		public function get centroid():String
		{
			if (_isValid)
			{
				var tokens:Array=StringUtil.trim(_circleInfo).split(" ");
				return StringUtil.trim(tokens[0]);
			}
			else
			{
				throw new Error(ResourceHelper.getErrorString(5, ResourceHelper.getCurrentLocale, ["AlertInfoAreaCircle::centroid"]));
			}
		}
		
		public function get radius():Number
		{
			if (_isValid)
			{
				return _radius;
			}
			else
			{
				throw new Error(ResourceHelper.getErrorString(5, ResourceHelper.getCurrentLocale, ["AlertInfoAreaCircle::radius"]));
			}
		}
		
		private function isValid():Boolean
		{
			//This could be way more robust.
			try
			{
				var tokens:Array=StringUtil.trim(_circleInfo).split(" ");
				_coordinates=StringUtil.trim(tokens[0]).split(",");
				_radius=Number(tokens[1]);
				
				var x:Number=Number(_coordinates[0]);
				var y:Number=Number(_coordinates[1]);
				if ((x < -180) || (x > 180))
					return false;
				if ((y < -90) || (y > 90))
					return false;
				if (_radius <= 0)
					return false;
				if (_radius > MAX_RADIUS_KM)
					return false;
			}
			catch (errObject:Error)
			{
				//An error will just return false
				return false;
			}
			
			//If we did not return 'false' by now, return true 
			return true;
		}
	}
}