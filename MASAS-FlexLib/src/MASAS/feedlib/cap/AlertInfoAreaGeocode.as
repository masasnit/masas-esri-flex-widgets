package MASAS.feedlib.cap
{
	import MASAS.feedlib.cap.enumerations.CapVersion;
	import MASAS.feedlib.utilities.ResourceHelper;

	public class AlertInfoAreaGeocode
	{
		private var _valueName:String;
		private var _value:String;
		public function AlertInfoAreaGeocode(valueName:String, value:String)
		{
			if (!valueName)
				throw new Error(ResourceHelper.getErrorString(1, ResourceHelper.getCurrentLocale, ["valueName", "AlertInfoAreaGeocode()"]));
			if (!value)
				throw new Error(ResourceHelper.getErrorString(1, ResourceHelper.getCurrentLocale, ["value", "AlertInfoAreaGeocode()"]));
			
			_valueName = valueName;
			_value = value
		}
		
		internal function toXMLString():String
		{
			var geocode:String;
			geocode = "<geocode>"; 
			geocode += "<valueName>" + _valueName + "</valueName>"; 
			geocode += "<value>" + _value + "</value>";
			geocode += "</geocode>"; 
			
			return geocode; 
		}
		
		internal static function fromXML(xml:XML, version:CapVersion):AlertInfoAreaGeocode
		{
			if (!xml) return null; 
			
			//We need a CAP version for proper parsing 
			const CAP:Namespace=new Namespace(version.toString());
			use namespace CAP; 
			
			var value:String = xml.CAP::value.toString();
			var valueName:String = xml.CAP::valueName.toString();
			if ((value) && (valueName))
				return new AlertInfoAreaGeocode(valueName,value);
			else 
				return null; 
		}
		
		public function get valueName():String
		{
			return _valueName;
		}
		
		public function get value():String
		{
			return _value;
		}
	}
}