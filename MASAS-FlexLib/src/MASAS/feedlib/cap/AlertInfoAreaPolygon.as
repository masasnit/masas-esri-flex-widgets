package MASAS.feedlib.cap
{
	import com.esri.ags.geometry.*;
	
	import mx.core.mx_internal;
	import mx.utils.StringUtil;
	
	import MASAS.feedlib.utilities.ResourceHelper;
	
	public final class AlertInfoAreaPolygon
	{
		private var _coordinateArray:Array;
		private var _coordinatePairs:String;
		private var _isValid:Boolean;
		
		//Supposed to pass in a whitespace delimited set of four or more coordinate pairs 
		public function AlertInfoAreaPolygon(coordinatePairs:String)
		{
			_coordinatePairs=coordinatePairs;
			_coordinateArray=StringUtil.trim(coordinatePairs).split(" ");
			
			if (!coordinatePairs)
				throw new Error(ResourceHelper.getErrorString(1, ResourceHelper.getCurrentLocale, ["coordinatePairs", "AlertInfoAreaPolygon()"]));
			
			_isValid=isValid();
			if (!_isValid)
				throw new Error(ResourceHelper.getErrorString(0, ResourceHelper.getCurrentLocale, ["coordinatePairs", "AlertInfoAreaPolygon()"]));
			
		}
		
		internal function toXMLString():String
		{
			if (!_isValid)
				throw new Error(ResourceHelper.getErrorString(5, ResourceHelper.getCurrentLocale, ["AlertInfoAreaPolygon::toXMLString"]));
			else 
				return "<polygon>" + _coordinatePairs + "</polygon>";
			
		}
		
		public function get coordinatePairs():String
		{
			if (_isValid)
			{
				return _coordinatePairs;
			}
			else
			{
				throw new Error(ResourceHelper.getErrorString(5, ResourceHelper.getCurrentLocale, ["AlertInfoAreaPolygon::coordinatePairs"]));
			}
		}
		
		public function get coordinates():Array
		{
			if (_isValid)
			{
				return _coordinateArray;
			}
			else
			{
				throw new Error(ResourceHelper.getErrorString(5, ResourceHelper.getCurrentLocale, ["AlertInfoAreaPolygon::coordinates"]));
			}
		}
		
		public function get asPolygon():Polygon
		{
			var path:Array=[];
			var coord:String;
			var coordArray:Array;
			var len:int=_coordinateArray.length;
			for (var i:int=0; i < len; i++)
			{
				coord=_coordinateArray[i];
				coordArray=coord.split(",");
				var y:Number=Number(coordArray[0]);
				var x:Number=Number(coordArray[1]);
				path.push(new MapPoint(x, y));
			}
			
			return new Polygon([path]);
		}
		
		private function isValid():Boolean
		{
			try
			{
				//Must be at least four vertices 
				if (_coordinateArray.length >= 4)
				{
					//First and last have to be the same
					var first:String=_coordinateArray[0];
					var last:String=_coordinateArray[_coordinateArray.length - 1];
					if (first != last)
						return false;
					//Validate that each vertex is ok
					var coord:String;
					var coordArray:Array;
					var len:int=_coordinateArray.length;
					for (var i:int=0; i < len; i++)
					{
						//Each coordinates should be an xy pair seperated by a comma
						coord=_coordinateArray[i];
						coordArray=coord.split(",");
						if ((coordArray.length) != 2)
							return false;
						//WGS84 coordinates will be between -180, 180 and -90, 90
						var y:Number=Number(coordArray[0]);
						var x:Number=Number(coordArray[1]);
						if ((x < -180) || (x > 180))
							return false;
						if ((y < -90) || (y > 90))
							return false;
					}
				}
				else
				{
					return false;
				}
			}
			catch (errObject:Error)
			{
				//An error will just return false
				return false;
			}
			
			//If we did not return 'false' by now, return true 
			return true;
		}
	}
}