package MASAS.feedlib.cap
{
    import com.adobe.utils.DateUtil;
    
    import MASAS.feedlib.cap.enumerations.AlertMsgType;
    import MASAS.feedlib.cap.enumerations.AlertScope;
    import MASAS.feedlib.cap.enumerations.AlertStatus;
    import MASAS.feedlib.cap.enumerations.CapVersion;
    import MASAS.feedlib.utilities.ResourceHelper;
    
    
    public class Alert
    {
        private var _version:CapVersion; 
        private var _id:String;
        private var _sender:String;
        private var _sent:Date;
        private var _alertStatus:AlertStatus;
        private var _alertMsgType:AlertMsgType;
        private var _source:String;
        private var _alertScope:AlertScope;
        private var _restriction:String;
        private var _addresses:String;
        private var _handlingCodes:Vector.<String>;
        private var _note:String;
        private var _referenceIDs:String;
        private var _incidentIDs:String;
        private var _alertInfos:Vector.<AlertInfo>;
        
        public function Alert(version:CapVersion, id:String, sender:String, sent:Date, alertStatus:AlertStatus, alertMsgType:AlertMsgType,alertScope:AlertScope )
        {
            if (!version)
                throw new Error(ResourceHelper.getErrorString(1, ResourceHelper.getCurrentLocale, ["version", "Alert (constructor)"]));
            _version = version; 
            
            if (!id)
                throw new Error(ResourceHelper.getErrorString(1, ResourceHelper.getCurrentLocale, ["id", "Alert()"]));
            _id = id; 
            
            if (!sender)
                throw new Error(ResourceHelper.getErrorString(1, ResourceHelper.getCurrentLocale, ["sender", "Alert()"]));
            _sender = sender;
            
            if (!sent)
                throw new Error(ResourceHelper.getErrorString(1, ResourceHelper.getCurrentLocale, ["sent", "Alert()"]));
            _sent = sent;
            
            if (!alertStatus)
                throw new Error(ResourceHelper.getErrorString(1, ResourceHelper.getCurrentLocale, ["alertStatus", "Alert()"]));
            _alertStatus = alertStatus; 
            
            if (!alertMsgType)
                throw new Error(ResourceHelper.getErrorString(1, ResourceHelper.getCurrentLocale, ["alertMsgType", "Alert()"]));
            _alertMsgType = alertMsgType;
            
            if (!alertScope)
                throw new Error(ResourceHelper.getErrorString(1, ResourceHelper.getCurrentLocale, ["alertScope", "Alert()"]));
            _alertScope = alertScope;		
        }
        
        public function toXMLString():String
        {
            var i:int = 0;
            var alert:String; 
            try
            {
                alert = "<alert xmlns='" + _version.toString()   +"'>";
                alert += "<identifier>" + _id + "</identifier>";
                alert += "<sender>" + _sender + "</sender>";
                
                var utc:String = DateUtil.toW3CDTF(_sent).replace("+00:00", "-00:00");
                alert += "<sent>" + utc + "</sent>";
                alert += "<status>" + _alertStatus.toString() + "</status>";
                alert += "<msgType>" + _alertMsgType.toString() + "</msgType>";
                if (_source) alert += "<source>" + _source + "</source>";
                alert += "<scope>" + _alertScope.toString() + "</scope>";
                if (_restriction) alert += "<restriction>" + _restriction + "</restriction>";
                if (_addresses) alert += "<addresses>" + _addresses + "</addresses>";
                if (_handlingCodes)
                {
                    for (i=0; i < _handlingCodes.length; i++)
                    {
                        alert += "<code>" + _handlingCodes[i] + "</code>"; 
                    }
                }
                if (_note) alert += "<note>" + _note + "</note>";
                if (_referenceIDs) alert += "<references>" + _referenceIDs + "</references>";
                if (_incidentIDs) alert += "<incidents>" + _incidentIDs + "</incidents>";
                if (_alertInfos)
                {
                    var alertInfo:AlertInfo; 
                    for (i=0; i < _alertInfos.length; i++)
                    {
                        alertInfo = _alertInfos[i]; 
                        alert += alertInfo.toXMLString(); 
                    }
                }
                alert += "</alert>";				
            }
            catch (e:Error)
            {
                throw new Error(ResourceHelper.getErrorString(9, ResourceHelper.getCurrentLocale, [e.message.toString(), "Alert::toXMLString"]));
            }
            
            return alert; 
        }
        
        public static function fromXML(alertXML:XML):Alert
        {
            //user is expected to pass in an <alert> representing a CAP Alert 
            if ((!alertXML) || (alertXML.length == 0) )
                throw new Error(ResourceHelper.getErrorString(1, ResourceHelper.getCurrentLocale, ["alertXML", "Alert::fromXML"]));
            
            //TODO: Some validation that an <alert> was passed in. For now, assume something good was passed in.
            
            
            //Get the version from the <alert> passed in
            var version:CapVersion = getAlertVersionFromXML(alertXML); 
            
            if (!version)
                //Throw a 'problem parsing xml' error 
                throw new Error(ResourceHelper.getErrorString(8, ResourceHelper.getCurrentLocale, ["Alert::fromXML"]));			
            
            
            //We need a CAP version for proper parsing 
            const CAP:Namespace=new Namespace(version.toString());
            use namespace CAP; 
            
            var sent : Date; 
            
            try {
                sent = DateUtil.parseW3CDTF( alertXML.CAP::sent.toString() );
            }
            catch( sentError : Error )
            {
                throw new Error( ResourceHelper.getErrorString( 8, ResourceHelper.getCurrentLocale, [sentError.message] ) );			
            }
            
            try
            {
                //There are a bunch of mandatory elements in the CAP XML. We'll assume they're all there. 
                var id:String = alertXML.CAP::identifier.toString(); 
                var sender:String = alertXML.CAP::sender.toString(); 
                var status:AlertStatus = AlertStatus.fromString(alertXML.CAP::status.toString()); 
                var msgType:AlertMsgType = AlertMsgType.fromString(alertXML.CAP::msgType.toString()); 
                var scope:AlertScope = AlertScope.fromString(alertXML.CAP::scope.toString());
                var alert:Alert = new Alert(version,id, sender,sent,status, msgType,scope); 
                
                var s:String = alertXML.CAP::source.toString(); 
                if ((s) && (s.length > 0)) alert.source = s;
                
                s = alertXML.CAP::restriction;
                if ((s) && (s.length > 0)) alert.restriction = s; 
                
                s = alertXML.CAP::addresses
                if ((s) && (s.length > 0)) alert.addresses = s; 
                
                //An Alert can have multiple handling codes. 
                var codes:XMLList = alertXML.CAP::code; 
                var codeVec:Vector.<String> = new Vector.<String>; 
                for each (var code:XML in codes)
                {
                    codeVec.push(code.toString()); //Does code.toString() work?		
                }
                alert.handlingCode = codeVec; 
                
                alert.note = alertXML.CAP::note.toString(); 
                alert.referenceIDs = alertXML.CAP::references.toString() ; 
                alert.incidentIDs = alertXML.CAP::incidents.toString() ;
                
                //An Alert may be composed of one or more AlertInfo objects ... 
                var infos:XMLList = alertXML.CAP::info; 
                var alertInfo:AlertInfo;
                var infoVec:Vector.<AlertInfo> = new Vector.<AlertInfo>(); 
                for each (var info:XML in infos)
                {
                    alertInfo = AlertInfo.fromXML(info,version); 
                    if (alertInfo) infoVec.push(alertInfo); 
                }
                if (infoVec.length > 0) 
                    alert.alertInfo = infoVec; 
            }
            catch (objError:Error)
            {
                //Throw a 'problem parsing xml' error 
                throw new Error(ResourceHelper.getErrorString(8, ResourceHelper.getCurrentLocale, ["Alert::fromXML"]));			
            }
            
            return alert; 
        }
        
        public function get version():CapVersion
        {
            return _version;
        }
        
        public function get identifier():String
        {
            return _id;
        }
        
        public function set identifier(value:String):void
        {
            _id = value;
        }
        
        public function get sender():String
        {
            return _sender;
        }
        
        public function get sent():Date
        {
            return _sent;
        }
        
        public function set sent(value:Date):void
        {
            _sent = value;
        }
        
        public function get alertStatus():AlertStatus
        {
            return _alertStatus;
        }
        
        public function set alertStatus(value:AlertStatus):void
        {
            _alertStatus = value;	
        }
        
        public function get alertMsgType():AlertMsgType
        {
            return _alertMsgType;
        }
        
        public function set alertMsgType(value:AlertMsgType):void
        {
            _alertMsgType = value;
        }
        
        public function get source():String
        {
            return _source;
        }
        
        public function set source(value:String):void
        {
            _source=value;
        }
        
        public function get alertScope():AlertScope
        {
            return _alertScope;
        }
        
        public function get restriction():String
        {
            return _restriction;
        }
        
        public function set restriction(value:String):void
        {
            _restriction=value;
        }
        
        public function get addresses():String
        {
            return _addresses;
        }
        
        public function set addresses(value:String):void
        {
            _addresses=value;
        }
        
        public function get handlingCode():Vector.<String>
        {
            return _handlingCodes;
        }
        
        public function set handlingCode(value:Vector.<String>):void
        {
            _handlingCodes=value;
        }
        
        public function get note():String
        {
            return _note;
        }
        
        public function set note(value:String):void
        {
            _note=value;
        }
        
        public function get referenceIDs():String
        {
            return _referenceIDs;
        }
        
        public function set referenceIDs(value:String):void
        {
            _referenceIDs=value;
        }
        
        public function get incidentIDs():String
        {
            return _incidentIDs;
        }
        
        public function set incidentIDs(value:String):void
        {
            _incidentIDs=value;
        }
        
        public function get alertInfo():Vector.<AlertInfo>
        {
            return _alertInfos;
        }
        
        public function set alertInfo(value:Vector.<AlertInfo>):void
        {
            _alertInfos=value;
        }
        
        public static function getAlertVersionFromXML(alertXML:XML):CapVersion
        {
            if (!alertXML) return null; 
            
            var arr:Array = alertXML.namespaceDeclarations(); 
            if (arr.length > 0 )
                return CapVersion.fromString(arr[0].toString());
            else 
                return null; 
        }
    }
}