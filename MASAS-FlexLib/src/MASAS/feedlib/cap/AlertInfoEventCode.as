package MASAS.feedlib.cap
{
	import MASAS.feedlib.cap.enumerations.CapVersion;
	import MASAS.feedlib.utilities.ResourceHelper;

	public class AlertInfoEventCode
	{
		private var _valueName:String;
		private var _value:String;
		public function AlertInfoEventCode(valueName:String, value:String)
		{
			if (!valueName)
				throw new Error(ResourceHelper.getErrorString(1, ResourceHelper.getCurrentLocale, ["valueName", "AlertInfoEventCode()"]));
			if (!value)
				throw new Error(ResourceHelper.getErrorString(1, ResourceHelper.getCurrentLocale, ["value", "AlertInfoEventCode()"]));
			
			_valueName = valueName;
			_value = value
		}
		
		internal function toXMLString():String
		{
			var eventCode:String;
			eventCode = "<eventCode>"; 
			eventCode += "<valueName>" + _valueName + "</valueName>"; 
			eventCode += "<value>" + _value + "</value>";
			eventCode += "</eventCode>"; 
			
			return eventCode; 
		}
		
		internal static function fromXML(xml:XML, version:CapVersion):AlertInfoEventCode
		{
			if (!xml) return null; 
			
			//We need a CAP version for proper parsing 
			const CAP:Namespace=new Namespace(version.toString());
			use namespace CAP; 
			
			var value:String = xml.CAP::value.toString();
			var valueName:String = xml.CAP::valueName.toString();
			if ((value) && (valueName))
				return new AlertInfoEventCode(valueName,value);
			else 
				return null; 
		}
		
		public function get valueName():String
		{
			return _valueName;
		}
		
		public function get value():String
		{
			return _value;
		}
		
		public function toString() : String
		{
			return _valueName + ": " + _value;
		}
		
	}
}