package MASAS.feedlib.cap
{
	
	import mx.collections.ArrayCollection;
	
	import MASAS.feedlib.cap.enumerations.CapVersion;
	import MASAS.feedlib.utilities.ResourceHelper;
	
	public class AlertInfoArea
	{
		private var _areaDesc:String;
		private var _polygons:Vector.<AlertInfoAreaPolygon>;
		private var _circles:Vector.<AlertInfoAreaCircle>;
		private var _geocodes:Vector.<AlertInfoAreaGeocode>;
		private var _altitude:Number;
		private var _altitudeSpecified:Boolean;
		private var _ceiling:Number;
		private var _ceilingSpecified:Boolean;
		public function AlertInfoArea(areaDesc:String)
		{
			if (!areaDesc)
				throw new Error(ResourceHelper.getErrorString(1, ResourceHelper.getCurrentLocale, ["areaDesc", "AlertInfoArea()"]));
			_areaDesc = areaDesc; 
		}
		
		internal function toXMLString():String
		{
			var i:int = 0; 
			var area:String; 
			area = "<area>";
			area += "<areaDesc>" + _areaDesc + "</areaDesc>";
			if (_polygons)
			{
				var polygon:AlertInfoAreaPolygon; 
				for (i=0; i < _polygons.length; i++)
				{
					polygon = _polygons[i]; 
					area += polygon.toXMLString(); 
				}
			}
			if (_circles)
			{
				var circle:AlertInfoAreaCircle; 
				for (i=0; i < _circles.length; i++)
				{
					circle = _circles[i]; 
					area += circle.toXMLString(); 
				}
			}
			if (_geocodes)
			{
				var geocode:AlertInfoAreaGeocode; 
				for (i=0; i < _geocodes.length; i++)
				{
					geocode = _geocodes[i]; 
					area += geocode.toXMLString(); 
				}
			}
			if (_altitudeSpecified) area += "<altitude>"  +  _altitude.toString() + "</altitude>";	  
			if (_ceilingSpecified) area += "<ceiling>"  +  _ceiling.toString() + "</ceiling>"; 
			area += "</area>"
			
			return area; 			
		}
		
		internal static function fromXML(xml:XML, version:CapVersion):AlertInfoArea
		{
			if (!xml) return null;
			
			//We need a CAP version for proper parsing 
			const CAP:Namespace=new Namespace(version.toString());
			use namespace CAP; 
			
			var desc:String = xml.CAP::areaDesc.toString();
			if (!desc) return null; 
			
			var area:AlertInfoArea = new AlertInfoArea(desc);
			var i:int = 0; 
			var polygons:XMLList = xml.CAP::polygon;
			if (polygons)
			{
				var vecPoly:Vector.<AlertInfoAreaPolygon> = new Vector.<AlertInfoAreaPolygon>();
				var p:AlertInfoAreaPolygon; 
				for each (var polygon:XML in polygons)
				{
					p = new AlertInfoAreaPolygon(polygon.toString()); 
					vecPoly.push(p); 
				}
				if (vecPoly.length > 0 ) area.polygon = vecPoly; 
			}
			
			var circles:XMLList = xml.CAP::circle;
			if (circles)
			{
				var vecCircle:Vector.<AlertInfoAreaCircle> = new Vector.<AlertInfoAreaCircle>();
				var c:AlertInfoAreaCircle; 
				for each (var circle:XML in circles)
				{
					c = new AlertInfoAreaCircle(circle.toString()); 
					vecCircle.push(c); 
				}
				if (vecCircle.length > 0 ) area.circle = vecCircle;
			}			
			
			var geocodes:XMLList = xml.CAP::geocode;
			if (geocodes)
			{
				var vecGeocode:Vector.<AlertInfoAreaGeocode> = new Vector.<AlertInfoAreaGeocode>();
				var g:AlertInfoAreaGeocode; 
				for each (var geocode:XML in geocodes)
				{
					g = AlertInfoAreaGeocode.fromXML(geocode,version); 
					if (g) vecGeocode.push(g); 
				}
				if (vecGeocode.length > 0) area.geocode = vecGeocode;
			}	
			
			var altitude:String = xml.CAP::altitude.toString();
			if ((altitude) && (altitude.length > 0)) area.altitude = int(altitude); 
			var ceiling:String = xml.CAP::ceiling.toString();
			if ((ceiling) && (ceiling.length > 0)) area.ceiling = int(ceiling); 
			
			return area;
		}
		
		public function get areaDesc():String
		{
			return _areaDesc;
		}
		
		public function get polygon():Vector.<AlertInfoAreaPolygon>
		{
			return _polygons;
		}
		
		public function set polygon(value:Vector.<AlertInfoAreaPolygon>):void
		{
			_polygons=value;
		}
		
		public function get circle():Vector.<AlertInfoAreaCircle>
		{
			return _circles;
		}
		
		public function set circle(value:Vector.<AlertInfoAreaCircle>):void
		{
			_circles=value;
		}
		
		public function get geocode():Vector.<AlertInfoAreaGeocode>
		{
			return _geocodes;
		}
		
		public function set geocode(value:Vector.<AlertInfoAreaGeocode>):void
		{
			_geocodes=value;
		}
		
		public function get altitude():Number
		{
			return _altitude;
		}
		
		public function set altitude(value:Number):void
		{
			if (_ceilingSpecified)
			{
				if (value >= _ceiling)
					throw new Error(ResourceHelper.getErrorString(6, ResourceHelper.getCurrentLocale, ["AlertInfoArea::altitude"]));
			}
			_altitudeSpecified=true;
			_altitude=value;
		}
		
		public function get ceiling():Number
		{
			return _ceiling;
		}
		
		public function set ceiling(value:Number):void
		{
			if (!_altitudeSpecified)
				throw new Error(ResourceHelper.getErrorString(7, ResourceHelper.getCurrentLocale, ["AlertInfoArea::ceiling"]));
			_ceilingSpecified=true;
			_ceiling=value;
			
		}
	}
}