package MASAS.feedlib.cap
{
	import com.adobe.utils.DateUtil;
	
	import MASAS.feedlib.cap.enumerations.AlertInfoCategory;
	import MASAS.feedlib.cap.enumerations.AlertInfoCertainty;
	import MASAS.feedlib.cap.enumerations.AlertInfoResponseType;
	import MASAS.feedlib.cap.enumerations.AlertInfoSeverity;
	import MASAS.feedlib.cap.enumerations.AlertInfoUrgency;
	import MASAS.feedlib.cap.enumerations.CapVersion;
	import MASAS.feedlib.utilities.ResourceHelper;
	import MASAS.feedlib.utilities.SupportedLocale;
	
	public class AlertInfo
	{
		private var _language:SupportedLocale;
		private var _categories:Vector.<AlertInfoCategory>;
		private var _eventType:String;
		private var _responseTypes:Vector.<AlertInfoResponseType>;
		private var _urgency:AlertInfoUrgency;
		private var _severity:AlertInfoSeverity;	
		private var _certainty:AlertInfoCertainty;
		private var _audience:String;
		private var _eventCodes:Vector.<AlertInfoEventCode>;
		private var _effectiveDate:Date;
		private var _effectiveSpecified:Boolean;
		private var _onsetDate:Date;
		private var _onsetSpecified:Boolean;
		private var _expiryDate:Date;
		private var _expirySpecified:Boolean;
		private var _senderName:String;
		private var _headline:String;
		private var _description:String;
		private var _instruction:String;
		private var _web:String;
		private var _contact:String;
		private var _parameters:Vector.<AlertInfoParameter>;
		private var _resources:Vector.<AlertInfoResource>;
		private var _areas:Vector.<AlertInfoArea>;
		
		public function AlertInfo(lang:SupportedLocale, categories:Vector.<AlertInfoCategory>,eventType:String, 
								  urgency:AlertInfoUrgency, severity:AlertInfoSeverity,certainty:AlertInfoCertainty )
		{
			if (!lang)
				throw new Error(ResourceHelper.getErrorString(1, ResourceHelper.getCurrentLocale, ["lang", "AlertInfo()"]));
			_language = lang; 
			
			if (!categories)
				throw new Error(ResourceHelper.getErrorString(1, ResourceHelper.getCurrentLocale, ["categories", "AlertInfo()"]));
			_categories = categories;
			
			if (!eventType)
				throw new Error(ResourceHelper.getErrorString(1, ResourceHelper.getCurrentLocale, ["eventType", "AlertInfo()"]));
			_eventType = eventType;
			
			if (!urgency)
				throw new Error(ResourceHelper.getErrorString(1, ResourceHelper.getCurrentLocale, ["urgency", "AlertInfo()"]));
			_urgency = urgency; 
			
			if (!severity)
				throw new Error(ResourceHelper.getErrorString(1, ResourceHelper.getCurrentLocale, ["severity", "AlertInfo()"]));
			_severity = severity; 
			
			if (!certainty)
				throw new Error(ResourceHelper.getErrorString(1, ResourceHelper.getCurrentLocale, ["certainty", "AlertInfo()"]));
			_certainty = certainty; 		
		}
		
		internal function toXMLString():String
		{
			var i:int = 0;
			var info:String; 
			info = "<info>"
			
			if (_language) info += "<language>" + _language.toString() + "</language>"	
			var category:AlertInfoCategory;  
			for (i=0; i < _categories.length; i++)
			{
				category = _categories[i]; 
				info += "<category>" + category.toString() + "</category>"; 
			}
			info += "<event>" + _eventType + "</event>";
			if (_responseTypes)
			{
				var responseType:AlertInfoResponseType;  
				for (i=0; i < _responseTypes.length; i++)
				{
					responseType = _responseTypes[i]; 
					info += "<responseType>" + responseType.toString() + "</responseType>"; 
				}
			}
			info += "<urgency>" + _urgency.toString() + "</urgency>";
			info += "<severity>" + _severity.toString() + "</severity>";
			info += "<certainty>" + _certainty.toString() + "</certainty>";
			if (_audience) info += "<audience>" + _audience + "</audience>";
			if (_eventCodes)
			{
				var eventCode:AlertInfoEventCode;  
				for (i=0; i < _eventCodes.length; i++)
				{
					eventCode = _eventCodes[i]; 
					info += eventCode.toXMLString(); 
				}
			}
			
			var utc:String;
			
			if ( _effectiveDate )
			{
				utc = DateUtil.toW3CDTF(_effectiveDate).replace("+00:00", "-00:00");
				info += "<effective>" + utc + "</effective>"; 
			}
			
			if (_onsetDate )
			{
				utc = DateUtil.toW3CDTF(_onsetDate).replace("+00:00", "-00:00");
				info += "<onset>" + utc + "</onset>"; 
			}
			
			if (_expiryDate )
			{
				utc = DateUtil.toW3CDTF(_expiryDate).replace("+00:00", "-00:00");
				info += "<expires>" + utc + "</expires>";
			}
			
			if (_senderName) info += "<senderName>" + _senderName + "</senderName>";
			if (_headline) info += "<headline>" + _headline + "</headline>";
			if (_description) info += "<description>" + _description + "</description>";
			if (_instruction) info += "<instruction>" + _instruction + "</instruction>";
			if (_web) info += "<web>" + _web + "</web>";
			if (_contact) info += "<contact>" + _contact + "</contact>";
			if (_parameters)
			{
				var parameter:AlertInfoParameter;  
				for (i=0; i < _parameters.length; i++)
				{
					parameter = _parameters[i]; 
					if ( parameter)
					{
						info += parameter.toXMLString(); 
					}
				}
			}
			if (_areas)
			{
				var area:AlertInfoArea;  
				for (i=0; i < _areas.length; i++)
				{
					area = _areas[i]; 
					info += area.toXMLString(); 
				}
			}
			if (_resources)
			{
				var resource:AlertInfoResource;   
				for (i=0; i < _resources.length; i++)
				{
					resource = _resources[i]; 
					info += resource.toXMLString(); 
				}
			}		
			info += "</info>"; 
			
			return info; 
		}
		
		internal static function fromXML(xml:XML, version:CapVersion):AlertInfo
		{
			if (!xml) return null; 
			
			//We need a CAP version for proper parsing 
			const CAP:Namespace=new Namespace(version.toString());
			use namespace CAP; 
			
			//We require a language. The default will be en_US if we can't find one
			var lang:SupportedLocale; 
			var l:String = xml.CAP::language.toString();  
			if (l) 
				lang = SupportedLocale.fromString(l); 
			if (!lang) 
				lang = SupportedLocale.fromString(l.replace("-", "_"));
			if (!lang)	
				lang = SupportedLocale.en_US;
			
			//An <info> can have many categories (at least one is mandatroy)
			var categories:XMLList = xml.CAP::category;
			var vecCategory:Vector.<AlertInfoCategory>= new Vector.<AlertInfoCategory>(); 
			if (categories)
			{
				vecCategory = new Vector.<AlertInfoCategory>();
				var c:AlertInfoCategory; 
				for each (var category:XML in categories)
				{
					c = AlertInfoCategory.fromString(category.toString());  
					vecCategory.push(c); 
				}
			}
			
			//Get the event type (mandatory on an <info>
			var eventType:String = xml.CAP::event.toString(); 
			
			//Get the urgency (mandatory on an <info>
			var urgency:AlertInfoUrgency = AlertInfoUrgency.fromString(xml.CAP::urgency.toString()); 
			
			//Get the severity (mandatory on an <info>
			var severity:AlertInfoSeverity = AlertInfoSeverity.fromString(xml.CAP::severity.toString()); 
			
			//Get the certainty (mandatory on an <info>
			var certainty:AlertInfoCertainty = AlertInfoCertainty.fromString(xml.CAP::certainty.toString()); 
			
			//We now have the mandatory elements of an <info>. Start building an AlertInfo
			var alertInfo:AlertInfo = new AlertInfo(lang, vecCategory, eventType, urgency, severity, certainty); 
			
			//An <info> may have more than one responseType
			var responseTypes:XMLList = xml.CAP::responseType;
			var vecResponseType:Vector.<AlertInfoResponseType>= new Vector.<AlertInfoResponseType>(); 
			if (responseTypes)
			{
				var r:AlertInfoResponseType; 
				for each (var responseType:XML in responseTypes)
				{
					var responseString:String = responseType.toString();
					
					if ((audience) && (responseString.length > 0))
					{
						r = AlertInfoResponseType.fromString(responseString);  
						vecResponseType.push(r); 
					}
				}
			}
			if (vecResponseType.length > 0) alertInfo.responseType = vecResponseType; 
			
			//Set the audience 
			var audience:String = xml.CAP::audience.toString();
			if ((audience) && (audience.length > 0)) alertInfo.audience = audience; 
			
			//An <info> may have more than one eventCode
			var eventCodes:XMLList = xml.CAP::eventCode;
			var vecEventCode:Vector.<AlertInfoEventCode>= new Vector.<AlertInfoEventCode>(); 
			if (eventCodes)
			{
				var e:AlertInfoEventCode; 
				for each (var eventCode:XML in eventCodes)
				{
					e = AlertInfoEventCode.fromXML(eventCode,version);  
					vecEventCode.push(e); 
				}
			}
			if (vecEventCode.length > 0) alertInfo.eventCodes = vecEventCode; 
			
			//Set the <effective> date
			var effective:String = xml.CAP::effective.toString(); 
			if ((effective) && (effective.length > 0)) 
				alertInfo.effectiveDate = DateUtil.parseW3CDTF(effective);
			
			//Set the <onset> date
			var onset:String = xml.CAP::onset.toString(); 
			if ((onset) && (onset.length > 0))				
				alertInfo.onsetDate = DateUtil.parseW3CDTF(onset);
			
			//Set the <expires> date
			var expires:String = xml.CAP::expires.toString(); 
			if ((expires) && (expires.length > 0))
				alertInfo.expiryDate = DateUtil.parseW3CDTF(expires);
			
			//Set the senderName 
			var senderName:String = xml.CAP::senderName.toString();
			if ((senderName) && (senderName.length > 0)) 
				alertInfo.senderName = senderName; 
			
			//Set the headline 
			var headline:String = xml.CAP::headline.toString();
			if ((headline) && (headline.length > 0))
				alertInfo.headline = headline; 
			
			//Set the description 
			var description:String = xml.CAP::description.toString();
			if ((description) && (description.length >0 ))
				alertInfo.description = description; 
			
			//Set the instructions 
			var instructions:String = xml.CAP::instructions.toString();
			if ((instructions) && (instructions.length > 0)) 
				alertInfo.instruction = instructions; 
			
			//Set the web 
			var web:String = xml.CAP::web.toString();
			if ((web) && (web.length > 0))
				alertInfo.web = web; 
			
			//Set the contact 
			var contact:String = xml.CAP::contact.toString();
			if ((contact) && (contact.length > 0))
				alertInfo.contact = contact; 
			
			//An <info> may have more than one parameter
			var parameters:XMLList = xml.CAP::parameter;
			var vecParameter:Vector.<AlertInfoParameter>= new Vector.<AlertInfoParameter>(); 
			if (parameters)
			{
				var p:AlertInfoParameter; 
				for each (var parameter:XML in parameters)
				{
					p= AlertInfoParameter.fromXML(parameter,version);  
					vecParameter.push(p); 
				}
			}
			if (vecParameter.length > 0) alertInfo.parameters = vecParameter; 
			
			//An <info> may have more than one area
			var areas:XMLList = xml.CAP::area;
			var vecArea:Vector.<AlertInfoArea>= new Vector.<AlertInfoArea>(); 
			if (areas)
			{
				var a:AlertInfoArea; 
				for each (var area:XML in areas)
				{
					a= AlertInfoArea.fromXML(area,version);  
					vecArea.push(a); 
				}
			}
			if (vecArea.length > 0) alertInfo.areas = vecArea; 
			
			//An <info> may have more than one resource
			var resources:XMLList = xml.CAP::resource;
			var vecResource:Vector.<AlertInfoResource>= new Vector.<AlertInfoResource>(); 
			if (resources)
			{
				var rr:AlertInfoResource; 
				for each (var resource:XML in resources)
				{
					rr= AlertInfoResource.fromXML(resource,version);  
					vecResource.push(rr); 
				}
			}
			if (vecResource.length > 0) alertInfo.resources = vecResource; 
			
			
			return alertInfo; 
		}
		
		public function get language():SupportedLocale
		{
			return _language;
		}
		
		public function get categories():Vector.<AlertInfoCategory>
		{
			return _categories;
		}
		
		public function get eventType():String
		{
			return _eventType;
		}
		
		public function get responseType():Vector.<AlertInfoResponseType>
		{
			return _responseTypes;
		}
		
		public function set responseType(value:Vector.<AlertInfoResponseType>):void
		{
			_responseTypes=value;
		}
		
		public function get urgency():AlertInfoUrgency
		{
			return _urgency;
		}
		
		public function get severity():AlertInfoSeverity
		{
			return _severity;
		}
		
		public function get certainty():AlertInfoCertainty
		{
			return _certainty;
		}
		
		public function get audience():String
		{
			return _audience;
		}
		
		public function set audience(value:String):void
		{
			_audience=value;
		}
		
		public function get eventCodes():Vector.<AlertInfoEventCode>
		{
			return _eventCodes;
		}
		
		public function set eventCodes(value:Vector.<AlertInfoEventCode>):void
		{
			_eventCodes=value;
		}
		
		public function get effectiveDate():Date
		{
			return _effectiveDate;
		}
		
		public function set effectiveDate(value:Date):void
		{
			_effectiveDate=value;
			_effectiveSpecified=true;
		}
		
		public function get onsetDate():Date
		{
			return _onsetDate;
		}
		
		public function set onsetDate(value:Date):void
		{
			_onsetDate=value;
			_onsetSpecified=true;
		}
		
		public function get expiryDate():Date
		{
			return _expiryDate;
		}
		
		public function set expiryDate(value:Date):void
		{
			_expiryDate=value;
			_expirySpecified=true;
		}
		
		public function get senderName():String
		{
			return _senderName;
		}
		
		public function set senderName(value:String):void
		{
			_senderName=value;
		}
		
		public function get headline():String
		{
			return _headline;
		}
		
		public function set headline(value:String):void
		{
			_headline=value;
		}
		
		
		public function get description():String
		{
			return _description;
		}
		
		public function set description(value:String):void
		{
			_description=value;
		}
		
		public function get instruction():String
		{
			return _instruction;
		}
		
		public function set instruction(value:String):void
		{
			_instruction=value;
		}
		
		public function get web():String
		{
			return _web;
		}
		
		public function set web(value:String):void
		{
			_web=value;
		}
		
		public function get contact():String
		{
			return _contact;
		}
		
		public function set contact(value:String):void
		{
			_contact=value;
		}
		
		public function get parameters():Vector.<AlertInfoParameter>
		{
			return _parameters;
		}
		
		public function set parameters(value:Vector.<AlertInfoParameter>):void
		{
			_parameters=value;
		}
		
		public function get resources():Vector.<AlertInfoResource>
		{
			return _resources;
		}
		
		public function set resources(value:Vector.<AlertInfoResource>):void
		{
			_resources=value;
		}
		
		public function get areas():Vector.<AlertInfoArea>
		{
			return _areas;
		}
		
		public function set areas(value:Vector.<AlertInfoArea>):void
		{
			_areas=value;
		}
	}
}