package MASAS.feedlib.cap
{
	import MASAS.feedlib.cap.enumerations.CapVersion;
	import MASAS.feedlib.utilities.ResourceHelper;

	public class AlertInfoResource
	{	
		private var _resourceDesc:String;
		private var _mimeType:String;
		private var _size:int;
		private var _uri:String;
		private var _derefUri:String;
		private var _digest:String;
		
		public function AlertInfoResource(resourceDesc:String)
		{
			if (!resourceDesc)
				throw new Error(ResourceHelper.getErrorString(1, ResourceHelper.getCurrentLocale, ["resourceDesc", "AlertInfoResource()"]));
			_resourceDesc = resourceDesc; 	
		}
		
		internal function toXMLString():String
		{
			var i:int = 0; 
			var resource:String; 
			resource = "<resource>"; 
			resource += "<resourceDesc>" + _resourceDesc + "</resourceDesc>";
			if (_mimeType) resource += "<mimeType>" + _mimeType + "</mimeType>";
			if (_size) resource += "<size>" + _size + "</size>";
			if (_uri) resource += "<uri>" + _uri + "</uri>";
			if (_derefUri) resource += "<derefUri>" + _derefUri + "</derefUri>";
			if (_digest) resource += "<digest>" + _digest + "</digest>";
			resource += "</resource>";
			
			return resource; 
		}
		
		internal static function fromXML(xml:XML, version:CapVersion):AlertInfoResource
		{
			if (!xml) return null; 
			
			//We need a CAP version for proper parsing 
			const CAP:Namespace=new Namespace(version.toString());
			use namespace CAP; 
			
			var desc:String = xml.CAP::resourceDesc.toString();
			if (!desc) return null; 
			
			var resource:AlertInfoResource = new AlertInfoResource(desc); 
			var mimeType:String = xml.CAP::mimeType.toString();
			if (mimeType) resource.mimeType = mimeType; 
			
			var size:int = int(xml.CAP::size);
			if (size) resource.size = size; 
			
			var uri:String = xml.CAP::uri.toString();
			if (uri) resource.uri = uri; 
			
			var derefUri:String = xml.CAP::derefUri.toString();
			if (derefUri) resource.derefUri = derefUri;
			
			var digest:String = xml.CAP::digest.toString();
			if (digest) resource.digest = digest; 
			
			return resource;
		}
		
		public function get resourceDesc():String
		{
			return _resourceDesc;
		}
		
		public function get mimeType():String
		{
			return _mimeType;
		}
		
		public function set mimeType(value:String):void
		{
			_mimeType=value;
		}
		
		public function get size():int
		{
			return _size;
		}
		
		public function set size(value:int):void
		{
			_size=value;
		}
		
		public function get uri():String
		{
			return _uri;
		}
		
		public function set uri(value:String):void
		{
			_uri=value;
		}
		
		public function get derefUri():String
		{
			return _derefUri;
		}
		
		public function set derefUri(value:String):void
		{
			_derefUri=value;
		}
		
		public function get digest():String
		{
			return _digest;
		}
		
		public function set digest(value:String):void
		{
			_digest=value;
		}
	}
}