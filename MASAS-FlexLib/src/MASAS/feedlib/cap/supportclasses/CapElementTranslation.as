package MASAS.feedlib.cap.supportclasses
{
	import MASAS.feedlib.cap.enumerations.CapElement;

	public class CapElementTranslation
	{
		private var _elementName:String;
		private var _translations:Vector.<CapTranslationValue>;
		
		public function CapElementTranslation(element:CapElement)
		{
			_elementName=element.toString();
		}
		
		public function get elementName():String
		{
			return _elementName;
		}
		
		public function get translations():Vector.<CapTranslationValue>
		{
			return _translations;
		}
		
		public function set translations(value:Vector.<CapTranslationValue>):void
		{
			_translations=value;
		}
	}
}