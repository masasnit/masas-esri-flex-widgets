package MASAS.feedlib.cap.supportclasses
{
	public class CapTranslationValue
	{
		public function CapTranslationValue(){}
		
		private var _id:String;
		private var _valueEN:String;
		private var _valueFR:String;
		private var _descriptionEN:String;
		private var _descriptionFR:String;
		
		public function get id():String
		{
			return _id;
		}
		
		public function set id(value:String):void
		{
			_id=value;
		}
		
		public function get valueEN():String
		{
			return _valueEN;
		}
		
		public function set valueEN(value:String):void
		{
			_valueEN=value;
		}
		
		public function get valueFR():String
		{
			return _valueFR;
		}
		
		public function set valueFR(value:String):void
		{
			_valueFR=value;
		}
		
		public function get descriptionEN():String
		{
			return _descriptionEN;
		}
		
		public function set descriptionEN(value:String):void
		{
			_descriptionEN=value;
		}
		
		public function get descriptionFR():String
		{
			return _descriptionFR;
		}
		
		public function set descriptionFR(value:String):void
		{
			_descriptionFR=value;
		}
	}
}