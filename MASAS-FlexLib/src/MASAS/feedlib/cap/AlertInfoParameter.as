package MASAS.feedlib.cap
{
	import MASAS.feedlib.cap.enumerations.CapVersion;
	import MASAS.feedlib.utilities.ResourceHelper;

	public class AlertInfoParameter
	{		
		private var _valueName:String;
		private var _value:String;
		public function AlertInfoParameter(valueName:String, value:String)
		{
			if (!valueName)
				throw new Error(ResourceHelper.getErrorString(1, ResourceHelper.getCurrentLocale, ["valueName", "AlertInfoParameter()"]));
			if (!value)
				throw new Error(ResourceHelper.getErrorString(1, ResourceHelper.getCurrentLocale, ["value", "AlertInfoParameter()"]));
			
			_valueName = valueName;
			_value = value
		}
		
		internal function toXMLString():String
		{
			var parameter:String;
			parameter = "<parameter>"; 
			parameter += "<valueName>" + _valueName + "</valueName>"; 
			parameter += "<value>" + _value + "</value>";
			parameter += "</parameter>"; 
			
			return parameter; 
		}
		
		internal static function fromXML(xml:XML, version:CapVersion):AlertInfoParameter
		{
			if (!xml) return null; 
			
			//We need a CAP version for proper parsing 
			const CAP:Namespace=new Namespace(version.toString());
			use namespace CAP; 
			
			var value:String = xml.CAP::value.toString();
			var valueName:String = xml.CAP::valueName.toString();
			if ((value) && (valueName))
				return new AlertInfoParameter(valueName,value);
			else 
				return null; 
		}
		
		public function get valueName():String
		{
			return _valueName;
		}
		
		public function get value():String
		{
			return _value;
		}
	}
}