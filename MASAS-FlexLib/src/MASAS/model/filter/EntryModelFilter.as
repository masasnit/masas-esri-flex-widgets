package MASAS.model.filter
{
    
    import flash.net.SharedObject;
    
    import MASAS.feedlib.masas.supportclasses.MasasAtomEntry;
    import MASAS.model.EntryModel;
    
    public class EntryModelFilter
    {
        
        // Private members...
        static private var _filterSO : SharedObject;
        
        private var _title		: String = "";
        private var _author 	: String = "";
        private var _status 	: String = "";
        private var _severity 	: String = "";
        private var _categories : Vector.<String> = new Vector.<String>();
        
        public function get categories() : Vector.<String>
        {
            return _categories;
        }
        
        public function set categories( value : Vector.<String> ) : void
        {
            _categories = value;
        }
        
        public function get severity() : String
        {
            return _severity;
        }
        
        public function set severity( value : String ) : void
        {
            _severity = value;
        }
        
        public function get status() : String
        {
            return _status;
        }
        
        public function set status( value : String ) : void
        {
            _status = value;
        }
        
        public function get author() : String
        {
            return _author;
        }
        
        public function set author( value : String ) : void
        {
            _author = value;
        }
        
        public function get title() : String
        {
            return _title;
        }
        
        public function set title( value : String ) : void
        {
            _title = value;
        }
        
        public function EntryModelFilter()
        {
        }
        
        public function applyFilter( entryModel : EntryModel ) : Boolean
        {
            var titleFilter 	: Boolean = false;
            var authorFilter 	: Boolean = false;
            var statusFilter 	: Boolean = false;
            var severityFilter 	: Boolean = false;
            var categoryFilter 	: Boolean = false;
            
            var entryData : MasasAtomEntry = entryModel.entryData;
            
            titleFilter 	= ( _title === "" ) || ( (entryData.title.toUpperCase()).indexOf( _title.toUpperCase() ) >= 0 );
            authorFilter 	= ( _author === "" ) || ( entryData.author.name === _author );
            statusFilter 	= ( _status === "" ) || ( entryData.category.status.toString() === _status );
            severityFilter 	= ( _severity === "" ) || ( entryData.category.severity.toString().indexOf( _severity ) >= 0 );
            
            // Categories...
            var entryCategories : Array = entryData.category.category.join( " " ).split( " " );
            var filterCategories : Array = _categories.join( " " ).split( " " );
            
            if( _categories.length == 0 )
            {
                categoryFilter = true;
            }
            else if( _categories.length > 0 )
            {
                for each( var entryCat : String in entryCategories )
                {
                    if( _categories.indexOf( entryCat ) >= 0 ) {
                        categoryFilter = true;
                        break;                    
                    }
                }
            }
            
            return titleFilter && authorFilter && statusFilter && severityFilter && categoryFilter;
        }
        
        public function isFilterEnabled() : Boolean
        {
            var titleFilter 	: Boolean = false;
            var authorFilter 	: Boolean = false;
            var statusFilter 	: Boolean = false;
            var severityFilter 	: Boolean = false;
            var categoryFilter 	: Boolean = false;
            
            titleFilter 	= ( _title === "" );
            authorFilter 	= ( _author === "" );
            statusFilter 	= ( _status === "" );
            severityFilter 	= ( _severity === "" );
            categoryFilter 	= ( _categories.length == 0 );
            
            return !( titleFilter && authorFilter && statusFilter && severityFilter && categoryFilter );
        }
        
        public function save() : void
        {
            EntryModelFilter._filterSO.data["title"]        = this.title;
            EntryModelFilter._filterSO.data["author"]       = this.author;
            EntryModelFilter._filterSO.data["status"]       = this.status;
            EntryModelFilter._filterSO.data["severity"]     = this.severity;
            EntryModelFilter._filterSO.data["categories"]   = ( categories.length > 0 ) ? categories.join( " " ) : "";
            
            EntryModelFilter._filterSO.flush();
        }
        
        static public function load() : EntryModelFilter
        {
            var modelFilter : EntryModelFilter = new EntryModelFilter();
            
            _filterSO =  SharedObject.getLocal( "entryFilter" );
            
            if( _filterSO.data["title"] != undefined ) {
                modelFilter.title = _filterSO.data["title"];
            }
            
            if( _filterSO.data["author"] != undefined ) {
                modelFilter.author = _filterSO.data["author"];
            }
            
            if( _filterSO.data["status"] != undefined ) {
                modelFilter.status = _filterSO.data["status"];
            }
            
            if( _filterSO.data["severity"] != undefined ) {
                modelFilter.severity = _filterSO.data["severity"];
            }
            
            if( _filterSO.data["categories"] != undefined && _filterSO.data["categories"].length > 0 )
            {
                var categories : Array = _filterSO.data["categories"].split(" ");
                
                modelFilter.categories.length = 0;
                for each( var category : String in categories ) {
                    modelFilter.categories.push( category );
                }
            }
            
            return modelFilter;
        }
        
    }
    
}