package MASAS.model.filter
{
	import flash.events.Event;
	
	public class EntryModelFilterChangedEvent extends Event
	{

		public static const FILTER_CHANGED:String = "entryModelFilterChangedEvent";

		// Private members...
		
		private var _filter : EntryModelFilter;
		
		// Accessors...
		
		public function get filter():EntryModelFilter
		{
			return _filter;
		}
		
		// Constructor...
		public function EntryModelFilterChangedEvent( type : String, filter : EntryModelFilter )
		{
			super( type );
			
			_filter = filter;
		}

	}
	
}