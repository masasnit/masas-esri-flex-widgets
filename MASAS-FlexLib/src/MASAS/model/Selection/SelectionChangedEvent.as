package MASAS.model.Selection
{
	import flash.events.Event;
	
	import MASAS.model.EntryModel;
	
	public class SelectionChangedEvent extends Event
	{
		
		public static const SELECTION_CHANGED:String = "selectionChanged";
		
		private var _entryModel : EntryModel = null;
		
		public function get entryModel() : EntryModel
		{
			return _entryModel;
		}
		
		public function SelectionChangedEvent( type : String, entryModel : EntryModel = null )
		{
			super( type );
			_entryModel = entryModel;
		}
		
		public override function clone() : Event
		{
			return new SelectionChangedEvent( this.type, this._entryModel );
		}
		
	}
	
}