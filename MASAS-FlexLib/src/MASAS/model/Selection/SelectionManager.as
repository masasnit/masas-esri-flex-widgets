package MASAS.model.Selection
{
    
    import flash.events.EventDispatcher;
    
    import MASAS.MASAS;
    import MASAS.model.EntryModel;
    import MASAS.model.filter.EntryModelFilter;
    import MASAS.model.filter.EntryModelFilterChangedEvent;
    
    // Events...
    [Event(name=SelectionChangedEvent.SELECTION_CHANGED, type="SelectionChangedEvent")]
    
    public class SelectionManager extends EventDispatcher
    {
        
        
        // Private members...
        private var _currentSelection : EntryModel = null;
        private var _masasMgr : MASAS;
        
        // Accessors...
        
        public function get currentSelection() : EntryModel
        {
            return _currentSelection;
        }
        
        // Constructor...
        public function SelectionManager( masasMgr : MASAS )
        {
            _masasMgr = masasMgr;
            _masasMgr.entryModelMgr.addEventListener( EntryModelFilterChangedEvent.FILTER_CHANGED, entryModelMgr_filterChange );
            applyFilterChange( _masasMgr.entryModelMgr.filter );
        }
        
        public function select( entry : EntryModel ) : void
        {
            // De-select the existing model, if there is one...
            if( _currentSelection ) {
                _currentSelection.selected = false;
            }
            
            // Assign the new entry...
            _currentSelection = entry;
            
            // Select the new entry, if there is one...
            if( _currentSelection ) {
                _currentSelection.selected = true;
            }
            
            // Notify the selection change...
            notifySelectionChanged( _currentSelection );
        }
        
        public function deSelectAll() : void
        {
            select( null );
        }
        
        protected function notifySelectionChanged( entry : EntryModel = null ) : void
        {
            // Create and dispatch event...
            dispatchEvent( new SelectionChangedEvent( SelectionChangedEvent.SELECTION_CHANGED, entry ) );
        }
        
        private function entryModelMgr_filterChange( event : EntryModelFilterChangedEvent ) : void
        {
            applyFilterChange( event.filter );
        }
        
        private function applyFilterChange( filter : EntryModelFilter ) : void
        {
            if( _currentSelection && !filter.applyFilter( _currentSelection ) )
            {
                // If the selection has been filter, de-select it!
                select( null );
            }
        }
        
    }
    
}