package MASAS.model
{
	
	import flash.events.Event;
		
	public class EntryModelUpdatedEvent extends Event
	{
		
		public static const ENTRY_MODEL_UPDATED:String = "entryModelUpdated";
		
		private var _dataChanged : Boolean = false;
		
		public function get dataChanged():Boolean
		{
			return _dataChanged;
		}
		
		public function EntryModelUpdatedEvent( type : String, dataChanged : Boolean = false )
		{
			super( type );
			_dataChanged = dataChanged;
		}
		
		public override function clone():Event
		{
			return new EntryModelUpdatedEvent( this.type, this._dataChanged );
		}
		
	}
	
}