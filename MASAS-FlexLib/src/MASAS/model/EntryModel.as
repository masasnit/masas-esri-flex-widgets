package MASAS.model
{
    
    import com.esri.ags.geometry.MapPoint;
    
    import flash.events.EventDispatcher;
    
    import MASAS.feedlib.cap.Alert;
    import MASAS.feedlib.masas.supportclasses.MasasAtomEntry;
    import MASAS.model.EntryModelUpdatedEvent;
    
    public class EntryModel extends EventDispatcher
    {
        
        // Events...
        [Event(name=EntryModelUpdated.ENTRY_MODEL_UPDATED, type="masas.model.EntryModelUpdatedEvent")]
        
        // Private Members...
        private var _entryMgr : EntryModelManager = null;
        private var _entryData : MasasAtomEntry = null;
        private var _alert : Alert = null;
        private var _alertRequestState : String = "none"; // none, requested, error
        private var _showGraphics : Boolean = false;
        private var _selected : Boolean = false;
        
        private var _geoPoint : MapPoint;
        
        // Accessors...
        
        public function get entryModelManager() : EntryModelManager
        {
            return _entryMgr;
        }
        
        public function set entryModelManager( value : EntryModelManager ) : void
        {
            _entryMgr = value;
        }
        
        public function get geoPoint() : MapPoint
        {
            return _geoPoint;
        }
        
        public function set geoPoint( value : MapPoint ) : void
        {
            _geoPoint = value;
        }
        
        public function get selected() : Boolean
        {
            return _selected;
        }
        
        public function set selected( value : Boolean ) : void
        {
            _selected = value;
            notifyUpdate();
        }
        
        public function get showGraphics() : Boolean
        {
            return _showGraphics;
        }
        
        public function set showGraphics( value : Boolean ) : void
        {
            _showGraphics = value;
            notifyUpdate();
        }
        
        public function get containsAlert() : Boolean
        {
            var retValue : Boolean = false;
            
            if( _entryData.capAlertLink ) {
                retValue = true;
            }
            
            return retValue;
        }
        
        public function get alert() : Alert
        {
            if( containsAlert )
            {
                // Get the Alert if it's not available...
                if( !_alert && ( _alertRequestState != "requested" ) )
                {
                    _alertRequestState = "requested";
                    _entryMgr.masas.requestAlert( _entryData.capAlertLink, onRequestSuccess, onRequestFailed );
                }
            }
            
            return _alert;
        }
        
        public function get entryData() : MasasAtomEntry
        {
            return _entryData;
        }
        
        public function set entryData( value : MasasAtomEntry ) : void
        {
            _entryData = value;
            notifyUpdate( true );
        }
        
        // Constructor...
        public function EntryModel( entryModelManager : EntryModelManager, entryData : MasasAtomEntry )
        {
            _entryMgr = entryModelManager;
            _entryData = entryData;
        }
        
        // Methods...
        
        private function onRequestSuccess( alert : Alert ) : void
        {
            _alertRequestState = "none";
            _alert = alert;
            
            notifyUpdate( true );
        }
        
        private function onRequestFailed( error : Error ) : void
        {
            _alertRequestState = "error";
        }
        
        private function notifyUpdate( dataChanged : Boolean = false ) : void
        {
            // Dispatch the Entry Updated event...
            dispatchEvent( new EntryModelUpdatedEvent( EntryModelUpdatedEvent.ENTRY_MODEL_UPDATED, dataChanged ) );	
        }
        
    }
    
}