package MASAS.model
{
    
    import mx.collections.ArrayCollection;
    import mx.collections.ArrayList;
    import mx.utils.ObjectUtil;
    
    import MASAS.MASAS;
    import MASAS.feedlib.events.FeedEvent;
    import MASAS.feedlib.feeds.MasasFeed;
    import MASAS.feedlib.masas.supportclasses.MasasAtomEntry;
    import MASAS.model.filter.EntryModelFilter;
    import MASAS.model.filter.EntryModelFilterChangedEvent;
    
    public class EntryModelManager extends ArrayList
    {
        
        // Private members...
        private var _masas : MASAS;
        private var _dataManager : MasasFeed;
        private var _filter : EntryModelFilter = new EntryModelFilter();
        
        // Events...
        [Event(name=EntryModelFilterChangedEvent.FILTER_CHANGED, type="MASAS.model.filter.EntryModelFilterChangedEvent")]
        
        // Public accessors...
        
        public function get masas():MASAS
        {
            return _masas;
        }
        
        public function get filter() : EntryModelFilter
        {
            return _filter;
        }
        
        public function set filter( value : EntryModelFilter ) : void
        {
            _filter = value;
            
            if( _filter == null ) {
                _filter = new EntryModelFilter();    
            }
            
            _filter.save();
            
            // Create and dispatch event...
            dispatchEvent( new EntryModelFilterChangedEvent( EntryModelFilterChangedEvent.FILTER_CHANGED, _filter ) );
        }
        
        public function EntryModelManager( masas : MASAS, dataManager : MasasFeed )
        {
            _masas = masas;
            _dataManager = dataManager;
            
            _filter = EntryModelFilter.load();
            
            _dataManager.addEventListener( FeedEvent.FEED_SUCCESS, masasFeed_ReadyHandler );
        }
        
        public function addUpdateEntryByData( entryData : MasasAtomEntry ) : void
        {
            var foundEntry : EntryModel = findEntryModelById( entryData.ID );
            
            if( foundEntry == null )
            {
                // Add a new entry...
                var entryModel : EntryModel = new EntryModel( this, entryData );
                addItemAt( entryModel, 0 );
            }
            else
            {
                // Update the existing data only if it's been changed...
                if( ObjectUtil.dateCompare( entryData.lastUpdated, foundEntry.entryData.lastUpdated ) == 1 )
                {
                    foundEntry.entryData = entryData;
                    itemUpdated( foundEntry );
                }
            }
        }

        private function masasFeed_ReadyHandler( event : FeedEvent) : void
        {
            // Process the atom entries from the hub
            processMasasFeed( event.data as ArrayCollection );
        }
        
        // Process the collection of Atom entries from the masas feed.  
        private function processMasasFeed( arrayCollection : ArrayCollection ):void
        {
            if( arrayCollection )
            {
                var itemsToAdd : ArrayCollection = new ArrayCollection();
                
                // Remove any items that aren't in the new data...
                for each( var entry : EntryModel in source )
                {
                    if( null == findAtomEntryModelById( arrayCollection, entry.entryData.ID ) ) {
                        this.removeItem( entry );
                    }
                }
                
                // Remove any items that aren't in the new data...
                for each( var atomEntry : MasasAtomEntry in arrayCollection )
                {
                    var foundEntry : EntryModel = findEntryModelById( atomEntry.ID );
                    
                    if( foundEntry == null )
                    {
                        // Add a new one...
                        var entryModel : EntryModel = new EntryModel( this, atomEntry );
                        
                        itemsToAdd.addItem( entryModel );
                    }
                    else
                    {
                        // Update the existing data only if it's been changed...
                        if( ObjectUtil.dateCompare( atomEntry.lastUpdated, foundEntry.entryData.lastUpdated ) == 1 )
                        {
                            foundEntry.entryData = atomEntry;
                            itemUpdated( foundEntry );
                        }
                    }
                    
                }
                
                addAll( itemsToAdd );
            }
            
            
        }
        
        private function findAtomEntryModelById( ac : ArrayCollection, identifier : String ) : MasasAtomEntry
        {
            var retValue : MasasAtomEntry = null;
            
            for each( var curEntry : MasasAtomEntry in ac )
            {
                if( curEntry.ID == identifier ) {
                    retValue = curEntry;
                    break;
                }
            }
            
            return retValue;
        }
        
        public function findEntryModelById( identifier : String ) : EntryModel
        {
            var retValue : EntryModel = null;
            
            for each( var curEntry : EntryModel in this.source )
            {
                if( curEntry.entryData.ID == identifier ) {
                    retValue = curEntry;
                    break;
                }
            }
            
            return retValue;
        }
        
    }
    
}