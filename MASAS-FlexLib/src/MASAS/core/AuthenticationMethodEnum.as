package MASAS.core
{
    
    import mx.utils.StringUtil;
    
    public final class AuthenticationMethodEnum
    {
        
        public static const useDefault	: AuthenticationMethodEnum = new AuthenticationMethodEnum( useDefault );
        public static const useProxy	: AuthenticationMethodEnum = new AuthenticationMethodEnum( useProxy );
        
        public function AuthenticationMethodEnum( authorizationMethodEnum : AuthenticationMethodEnum )
        {
        }
        
        public function toString() : String
        {
            switch( this )
            {
                case AuthenticationMethodEnum.useDefault:
                    return "useDefault";
                    break;
                case AuthenticationMethodEnum.useProxy:
                    return "useProxy";
                    break;
                default:
                    return "useDefault";
                    break;
            }
        }
        
        public static function fromString( value : String ) : AuthenticationMethodEnum
        {
            var returnValue : AuthenticationMethodEnum;
            
            try
            {
                switch( StringUtil.trim( value ).toLowerCase() )
                {
                    case "usedefault":
                    case "default":
                        returnValue = AuthenticationMethodEnum.useDefault;
                        break;
                    case "useproxy":
                    case "proxy":
                        returnValue = AuthenticationMethodEnum.useProxy;
                        break;				
                    default:
                        returnValue = AuthenticationMethodEnum.useDefault;
                        break;
                }
            }
            catch( e : Error )
            {
                returnValue = null;
            }
            
            return returnValue;
        }
        
    }
    
}