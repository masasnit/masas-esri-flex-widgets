package MASAS.core
{
	
	public final class EnumLoadingState
	{
		
		public static const UNKNOWN : String = "Unknown";
		public static const LOADING : String = "LOADING";
		public static const LOADED : String = "LOADED";
		public static const ERROR : String = "ERROR";
		
	}
	
}