package MASAS.core
{
    
    public class Account
    {
        
        // Private members...
        private var _groups	: Array;
        private var _hubs	: Array;
        private var _id		: Number;
        private var _name	: String;
        private var _uri	: String;
        
        // Constructor.
        public function Account( jsonStr : String, availableHubs : Array )
        {
            var resultObj : Object = JSON.parse( jsonStr );
            
            _uri 	= resultObj.uri;
            _id 	= resultObj.id;
            _name 	= resultObj.name;
            _groups	= resultObj.groups;
            
            var hubs : Array = resultObj.hubs;
            _hubs = new Array();
            
            for each ( var hub : * in hubs )
            {
                var existingHub : Hub = availableHubs[ hub.url ];
                if( existingHub )
                {
                    var hubCopy : Hub = existingHub.clone();
                    hubCopy.isReadOnly = !( hub["post"] == "Y" );
                    _hubs.push( hubCopy );
                }
            }
            
        }
        
        // Accessors.
        public function get groups() : Array
        {
            return _groups;
        }
        
        public function get hubs() : Array
        {
            return _hubs;
        }
        
        public function get id() : Number
        {
            return _id;
        }
        
        public function get name() : String
        {
            return _name;
        }
        
        public function get uri() : String
        {
            return _uri;
        }
        
        public function isHubReadOnly( url : String ) : Boolean
        {
            var retValue :  Boolean = false;
            
            for each ( var hub : Hub in _hubs )
            {
                if( hub.url == url )
                {
                    retValue = hub.isReadOnly;
                    break;
                }
            }
            
            return retValue;
        }
        
    }
    
}