package MASAS.core
{
    import flash.utils.Dictionary;
    
    public class Hub
    {
        
        // Private members...
        private var _identifier	: String;
        private var _name       : Dictionary	= new Dictionary();
        private var _url        : String;
        private var _color      : String;
        private var _isReadOnly : Boolean		= true;
        
        // Constructor.
        public function Hub()
        {
        }
        
        // Accessors.
        
        public function get isReadOnly() : Boolean
        {
            return _isReadOnly;
        }
        
        public function set isReadOnly( value : Boolean) : void
        {
            _isReadOnly = value;
        }
        
        public function get identifier() : String
        {
            return _identifier;
        }
        
        public function getName( lang : String ) : String
        {
            var retValue : String;
            
            if( _name[lang] )
            {
                retValue = _name[lang];
            }
            
            return retValue;
        }
        
        public function get url() : String
        {
            return _url;
        }
        
        public function get color() : String
        {
            return _color;
        }
        
        // Public Methods.
        public function fromXML( hubXML : XML ) : void
        {
            if( hubXML.id != undefined ) {
                _identifier	= hubXML.id;
            }
            
            _url 	= hubXML.url;
            _color 	= hubXML.color;
            
            var displayName : * = hubXML.displayName[0];
            
            for each ( var lang : * in displayName.lang )
            {
                var langValue : String = lang.@value;
                var langContent : String = lang;
                
                _name[ langValue ] = langContent;	
            }
            
        }
        
        public function clone() : Hub
        {
            var copy : Hub = new Hub();
            
            copy._identifier	= this._identifier;
            copy._url			= this._url;
            copy._color			= this._color;
            
            copy._name = new Dictionary();
            for( var key : Object in this._name ) {
                copy._name[key] = this._name[key];
            }
            
            return copy;
        }
        
    }
    
}