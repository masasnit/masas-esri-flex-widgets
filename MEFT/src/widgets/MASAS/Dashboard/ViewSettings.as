package widgets.MASAS.Dashboard
{

	public class ViewSettings
	{
		
		// Private members..
		
		// Zoom to selected...
		private var _zoomToSelected : Boolean = false;
		private var _zoomToSelectedScale : Number = 100000;
		
		public function ViewSettings( configXML : XML )
		{
			_zoomToSelected = ( configXML.view.behavior.zoomToSelected.toString() === "true" ) ? true : false;
			_zoomToSelectedScale = parseInt( configXML.view.behavior.zoomToSelected.@scale );
		}
		
		public function get zoomToSelectedScale():Number
		{
			return _zoomToSelectedScale;
		}

		public function set zoomToSelectedScale(value:Number):void
		{
			_zoomToSelectedScale = value;
		}

		public function get zoomToSelected():Boolean
		{
			return _zoomToSelected;
		}

		public function set zoomToSelected(value:Boolean):void
		{
			_zoomToSelected = value;
		}
		
	}
}