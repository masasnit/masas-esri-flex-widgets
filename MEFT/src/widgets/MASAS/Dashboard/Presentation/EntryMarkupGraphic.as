package widgets.MASAS.Dashboard.Presentation
{
    
    import com.esri.ags.Graphic;
    
    public class EntryMarkupGraphic extends Graphic
    {
        
        // Private members...
        private var _presObject : EntryPresentationObject;
        
        // Accessors...
        public function get presentationObject() : EntryPresentationObject
        {
            return _presObject;
        }
        
        // Constructor...
        public function EntryMarkupGraphic( presentationObject : EntryPresentationObject )
        {
            super( null, null, null ); 
            
            _presObject = presentationObject;
        }
        
    }
}