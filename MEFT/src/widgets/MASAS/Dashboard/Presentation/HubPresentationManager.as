package widgets.MASAS.Dashboard.Presentation
{

	import com.esri.ags.Graphic;
	import com.esri.ags.Map;
	import com.esri.ags.layers.GraphicsLayer;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ArrayList;
	import mx.collections.IList;
	import mx.events.CollectionEvent;
	import mx.events.CollectionEventKind;
	
	import MASAS.MASAS;
	import MASAS.model.EntryModel;
	import MASAS.model.filter.EntryModelFilterChangedEvent;
	
	import widgets.MASAS.Dashboard.MASASWidget;
	
	public class HubPresentationManager
	{
		
		// Private members...
		
		private var _masasWidget : MASASWidget;
		private var _masasMgr : MASAS;
		private var _map : Map;
		
		private var _markupLayer : GraphicsLayer;
		private var _detailsLayer : GraphicsLayer;
		private var _presObjs : ArrayList;
		
		// Accessors...
		
		public function get masasWidget():MASASWidget
		{
			return _masasWidget;
		}

		public function get detailsLayer():GraphicsLayer
		{
			return _detailsLayer;
		}
		
		public function get markupLayer():GraphicsLayer
		{
			return _markupLayer;
		}
		
		// Constructor...
		public function HubPresentationManager(  layerName : String, masasWidget : MASASWidget )
		{
			_masasWidget = masasWidget
			_masasMgr = _masasWidget.masasMgr;
			_map = _masasWidget.map;
			
			// Create the internal storage...
			_presObjs = new ArrayList();
			
			// Create the markup layer...
			_markupLayer = new GraphicsLayer();
			_markupLayer.name = layerName + "_markups"
			_markupLayer.symbol = _masasMgr.symbolManager.resultMarkerSymbol;

			// Create the details layer...
			_detailsLayer = new GraphicsLayer();
			_detailsLayer.name = layerName + "_details";
			
			// Add the layers to the map...
			_map.addLayer( _detailsLayer );
			_map.addLayer( _markupLayer );
			
			// Hook up the events to the model manager...
			_masasMgr.entryModelMgr.addEventListener( CollectionEvent.COLLECTION_CHANGE, entryModelMgr_collectionChange );
			_masasMgr.entryModelMgr.addEventListener( EntryModelFilterChangedEvent.FILTER_CHANGED, entryModelMgr_filterChange );
			
			// Let's apply any existing filters...
			applyFilterChange();
		}
		
		public function removeFromMarkups( graphic : Graphic ) : void
		{
			var graphicList : IList = ( markupLayer.graphicProvider as ArrayCollection ).list;
			var index : int = graphicList.getItemIndex( graphic );
			if( index >= 0 ) {
				graphicList.removeItemAt( index );
			}
		}
		
		public function removeFromDetails( graphic : Graphic ) : void
		{
			var graphicList : IList = ( detailsLayer.graphicProvider as ArrayCollection ).list;
			var index : int = graphicList.getItemIndex( graphic );
			if( index >= 0 ) {
				graphicList.removeItemAt( index );
			}
		}
	
		private function applyFilters( item : Object ) : Boolean
		{
			var retValue : Boolean = true;
			
			if( item is EntryMarkupGraphic )
			{
				var entryModel : EntryModel = ( item as EntryMarkupGraphic ).presentationObject.entryModel;

				retValue = _masasMgr.entryModelMgr.filter.applyFilter( entryModel );
				
				if( retValue == true ) {
					entryModel.showGraphics = false;	
				}
			}
			
			return retValue;
		}

		private function entryModelMgr_filterChange( event : Event ) : void
		{
			applyFilterChange();
		}
		
		private function applyFilterChange() : void
		{
			var graphicAC : ArrayCollection = (_markupLayer.graphicProvider as ArrayCollection);
			
			graphicAC.filterFunction = applyFilters;
			graphicAC.refresh();
		}
		
		private function entryModelMgr_collectionChange( event : CollectionEvent ) : void
		{
			if( event.kind == CollectionEventKind.ADD )
			{
				// Add the new models...
				addModels( event.items );	
			}
			else if( event.kind == CollectionEventKind.REMOVE )
			{
				// Remove the old models...
				removeModels( event.items );
			}
			else if( event.kind == CollectionEventKind.RESET )
			{
				// Clear the existing graphics...
				var graphicAC : ArrayCollection = (_markupLayer.graphicProvider as ArrayCollection);
				graphicAC.list.removeAll();
				
				graphicAC = (_detailsLayer.graphicProvider as ArrayCollection);
				graphicAC.list.removeAll();
				
				// Clear the presentation objects...
				_presObjs.removeAll();
			}
			else
			{
				var errorStr : String;
				errorStr = "Didn't reconize event kind: " + event.kind + "."; 
				//Alert.show( errorStr );
			}
		}

		private function addModels( newModels : Array ) : void
		{
			var arrayCollection : ArrayCollection = new ArrayCollection( newModels );
			
			// For every entry in the collection...
			for( var i : int; i < arrayCollection.length; i++ )
			{
				var entryModel : EntryModel = (arrayCollection[i] as EntryModel);
				var presObject : EntryPresentationObject = new EntryPresentationObject( this, entryModel );
				_presObjs.addItem( presObject );
			}
		}
		
		private function removeModels( oldModels : Array ) : void
		{
			// For every entry in the collection...
			for( var iModelCtr : int = 0; iModelCtr < oldModels.length; iModelCtr++ )
			{
				
				var entryModel : EntryModel = (oldModels[iModelCtr] as EntryModel);
				
				for( var iPresCtr : int = 0; iPresCtr < _presObjs.length; iPresCtr++ )
				{
					var presObject : EntryPresentationObject = _presObjs.getItemAt(iPresCtr) as EntryPresentationObject;
					if( presObject.entryModel.entryData.ID === entryModel.entryData.ID )
					{
						presObject.Dispose();
						_presObjs.removeItemAt( iPresCtr );
						break;
					}
				}
			}
		}

	}
	
}