package widgets.MASAS.Dashboard.Presentation
{
    import com.esri.ags.Graphic;
    import com.esri.ags.Map;
    import com.esri.ags.events.ExtentEvent;
    import com.esri.ags.geometry.Extent;
    import com.esri.ags.geometry.Geometry;
    import com.esri.ags.geometry.MapPoint;
    import com.esri.ags.geometry.Polygon;
    import com.esri.ags.geometry.Polyline;
    import com.esri.ags.symbols.CompositeSymbol;
    import com.esri.ags.symbols.Symbol;
    import com.esri.ags.utils.GraphicUtil;
    import com.esri.ags.utils.WebMercatorUtil;
    
    import flash.events.ContextMenuEvent;
    import flash.events.MouseEvent;
    import flash.ui.ContextMenu;
    import flash.ui.ContextMenuItem;
    
    import mx.collections.ArrayCollection;
    import mx.collections.IList;
    
    import MASAS.MASAS;
    import MASAS.feedlib.cap.AlertInfo;
    import MASAS.feedlib.cap.AlertInfoArea;
    import MASAS.feedlib.cap.AlertInfoAreaCircle;
    import MASAS.feedlib.cap.AlertInfoAreaPolygon;
    import MASAS.feedlib.cap.AlertInfoParameter;
    import MASAS.feedlib.graphics.CapGraphic;
    import MASAS.feedlib.graphics.CapanGraphic;
    import MASAS.feedlib.masas.supportclasses.MasasAtomEntry;
    import MASAS.feedlib.masas.supportclasses.MasasEntryColourEnum;
    import MASAS.feedlib.utilities.CapUtil;
    import MASAS.model.EntryModel;
    import MASAS.model.EntryModelUpdatedEvent;
    import MASAS.symbology.ColourCodeSymbol;
    
    import widgets.MASAS.Dashboard.MASASWidget;
    
    public class EntryPresentationObject
    {
        
        // Private members...
        
        private var _masasWidget : MASASWidget;
        private var _masasMgr : MASAS;
        private var _map : Map;
        
        private var _presMgr : HubPresentationManager;
        private var _entryModel : EntryModel;
        
        private var _marker : EntryMarkupGraphic;
        private var _graphics : ArrayCollection;
        private var _extent : Extent;
        private var _isArea : Boolean = false;
        
        // Accessors...
        
        public function get entryModel():EntryModel
        {
            return _entryModel;
        }
        
        // Constructor...
        public function EntryPresentationObject( presMgr : HubPresentationManager, entryModel : EntryModel )
        {
            _masasWidget = presMgr.masasWidget;
            _masasMgr = _masasWidget.masasMgr;
            _map = _masasWidget.map;
            _presMgr = presMgr;
            _entryModel = entryModel;
            
            updateMarker();
            
            _marker.contextMenu = createContextMenu();
            
            _entryModel.addEventListener( EntryModelUpdatedEvent.ENTRY_MODEL_UPDATED, onEntryModelUpdated );
        }
        
        public function Dispose() : void
        {
            _map.removeEventListener(ExtentEvent.EXTENT_CHANGE, map_extentChangeHandler );
            _entryModel.removeEventListener( EntryModelUpdatedEvent.ENTRY_MODEL_UPDATED, onEntryModelUpdated );
            
            // Remove the old graphics...
            cleanUpGraphics();
            
            if( _marker != null )
            {
                _marker.removeEventListener( MouseEvent.CLICK, graphic_MouseClickHandler );
                _presMgr.removeFromMarkups( _marker );
                _marker = null;
            }
            
            _entryModel = null;
        }
        
        private function createContextMenu() : ContextMenu 
        {
            var markupContextMenu : ContextMenu = new ContextMenu();
            
            var sendToBack : ContextMenuItem = new ContextMenuItem("Send to Back")
            sendToBack.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, contextMenu_sendToBack );
            markupContextMenu.customItems.push(sendToBack);
            
            var bringToFront : ContextMenuItem = new ContextMenuItem("Bring to Front")
            bringToFront.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, contextMenu_bringToFront );
            markupContextMenu.customItems.push(bringToFront);
            
            return markupContextMenu
        }
        
        private function contextMenu_sendToBack( event : ContextMenuEvent ) : void
        {
            _presMgr.markupLayer.setChildIndex( _marker, 0 );
        }
        
        private function contextMenu_bringToFront( event : ContextMenuEvent ) : void
        {
            _presMgr.markupLayer.moveToTop( _marker );
        }
        
        private function graphic_MouseClickHandler(event:MouseEvent) : void
        {
            if( event.currentTarget is EntryMarkupGraphic )
            {
                var graphic : EntryMarkupGraphic = event.currentTarget as EntryMarkupGraphic;
                
                // Select this item if needed...
                _masasMgr.selectionMgr.select( graphic.presentationObject.entryModel );
                
                // Toggle the callout...
                _masasWidget.toggleCallout( graphic.presentationObject.entryModel );
            }
        }
        
        private function map_extentChangeHandler( event : ExtentEvent ) : void
        {
            if( _entryModel != null ) {
                updateAreaMarkup( event.extent );
            }
        }
        
        private function updateAreaMarkup( extent : Extent ) : void
        {
            if( _isArea )
            {
                // Let's check to see if any of the geometries intersect with the Map View's extent!
                var newExtent : Extent = _extent.intersection( extent );
                if( newExtent != null && newExtent.height > 0 && newExtent.width > 0 )
                {
                    var newMarkerLocation : MapPoint = newExtent.center;
                    var locationIsValid : Boolean = false;
                    var goodGeometries : Array = new Array();
                    
                    if( _graphics )
                    {
                        // Now we need to make sure the marker is either in a polygon, or on a polyline!
                        // First, let's get all the geometries...
                        var geometries : Array = GraphicUtil.getGeometries( _graphics.source );
                        
                        // Let's go through all the geometries...
                        for each( var geometry : Geometry in geometries )
                        {
                            // Check to see if we should even consider this geometry...
                            if( geometry.extent.intersects( newExtent ) )
                            {
                                if( geometry.type == Geometry.POLYGON )
                                {
                                    // Is the center of the new extent in this polygon...
                                    if( (geometry as Polygon).contains( newExtent.center ) )
                                    {
                                        // We are good, let's stop here...
                                        newMarkerLocation = newExtent.center;
                                        locationIsValid = true;
                                        break;
                                    }
                                    
                                    // The center may still be closer to this polygon,
                                    // let's keep it for the next step.
                                    goodGeometries.push( geometry );
                                }
                                else if( geometry.type == Geometry.POLYLINE )
                                {
                                    // The center may still be closer to this polyline,
                                    // let's keep it for the next step.
                                    goodGeometries.push( geometry );
                                }
                            }
                        }
                        
                        if( !locationIsValid )
                        {
                            var distance : Number = Number.MAX_VALUE;
                            var curDistance : Number = Number.MAX_VALUE;
                            
                            // This means the point is outside a polygon, and/or we have polylines.
                            // We will now check every point and find the close one to use.
                            for each( var goodGeometry : Geometry in goodGeometries )
                            {
                                if( goodGeometry.type == Geometry.POLYGON )
                                {
                                    for each( var ring : Array in (goodGeometry as Polygon).rings )
                                    {
                                        for each( var ringPoint : MapPoint in ring )
                                        {
                                            curDistance = getSimpleDistance( newExtent.center, ringPoint );
                                            if( curDistance < distance ) {
                                                distance = curDistance;
                                                newMarkerLocation = ringPoint;
                                            }
                                        }
                                    }
                                }
                                else if( goodGeometry.type == Geometry.POLYLINE )
                                {
                                    for each( var path : Array in (goodGeometry as Polyline).paths )
                                    {
                                        for each( var pathPoint : MapPoint in path )
                                        {
                                            curDistance = getSimpleDistance( newExtent.center, pathPoint );
                                            if( curDistance < distance ) {
                                                distance = curDistance;
                                                newMarkerLocation = pathPoint;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    // Now that we kow where we want to marker, set the values as needed.
                    _marker.geometry = newMarkerLocation;
                    _entryModel.geoPoint = newMarkerLocation;
                    
                    if( _entryModel.selected ) {
                        _masasWidget.updateCalloutLocation( _entryModel );
                    }
                }
            }
        }
        
        private function getSimpleDistance( firstPoint : MapPoint, secondPoint : MapPoint ) : Number
        {
            var distance : Number;
            distance = Math.sqrt( Math.pow( (secondPoint.x - firstPoint.x), 2 ) + Math.pow( (secondPoint.y - firstPoint.y), 2 ) );
            return distance;
        }
        
        private function onEntryModelUpdated( event : EntryModelUpdatedEvent ) : void
        {
            if( event.dataChanged )
            {
                updateMarker();
                
                // Remove the old graphics...
                cleanUpGraphics();
            }
            
            if( _entryModel.selected || _entryModel.showGraphics )
            {
                // Load the graphics if it hasn't been done yet...
                if( _graphics == null ) {
                    _graphics = new ArrayCollection();
                    updateGraphics();
                    
                    // Get all the extents of the graphics..
                    _extent = GraphicUtil.getGraphicsExtent( _graphics.source );
                }
            }
            
            // Show/Hide the graphics...
            showGraphics( _entryModel.selected || _entryModel.showGraphics );
        }
        
        private function showGraphics( isVisible : Boolean ) : void
        {
            if( _graphics != null )
            {
                for each ( var graphic : Graphic in _graphics )
                {
                    graphic.visible = isVisible;
                }
            }
        }
        
        private function updateMarker() : void
        {
            var entryData : MasasAtomEntry = entryModel.entryData;
            var geometry : Geometry = entryData.geometry;
            
            // Reset some data...
            _map.removeEventListener(ExtentEvent.EXTENT_CHANGE, map_extentChangeHandler );
            
            _isArea = false;
            _extent = null;
            
            if( _map.spatialReference.wkid == 102113 || _map.spatialReference.wkid == 102100 || _map.spatialReference.wkid == 3857 ) {
                geometry = WebMercatorUtil.geographicToWebMercator( geometry );
            }
            
            if( _marker == null )
            {
                _marker = new EntryMarkupGraphic( this );
                _marker.addEventListener( MouseEvent.CLICK, graphic_MouseClickHandler );
                _presMgr.markupLayer.add( _marker );
            }
            
            if( geometry is MapPoint ) {
                _marker.geometry = geometry;
                _entryModel.geoPoint = geometry as MapPoint;
            }
            else {
                _marker.geometry = geometry.extent.center;
                _isArea = true;
                _extent = geometry.extent.extent;
                
                updateAreaMarkup( _map.extent );
                
                _map.addEventListener(ExtentEvent.EXTENT_CHANGE, map_extentChangeHandler );
            }
            
            // Get symbol from symbol set based on the entry eventCode
            var markerSymbol        : Symbol            = _masasMgr.symbolManager.getSymbol( entryData.category.eventCode );
            var symbolCollection    : ArrayCollection   = new ArrayCollection();
            var fullSymbol          : CompositeSymbol   = new CompositeSymbol();
            
            if( entryData.category.colourCode )
            {
                var colourSymbol : ColourCodeSymbol = new ColourCodeSymbol( entryData.category.colourCode.colour );
                symbolCollection.addItem( colourSymbol );    
            }
            
            symbolCollection.addItem( markerSymbol );
            
            fullSymbol.symbols = symbolCollection;
            _marker.symbol = fullSymbol;
        }
        
        private function cleanUpGraphics() : void
        {
            if( _graphics != null && _graphics.length > 0 )
            {
                var index : int = 0;
                var detailsLayer : IList = (_presMgr.detailsLayer.graphicProvider as ArrayCollection).list;
                
                for each ( var graphic : Graphic in _graphics )
                {
                    index = detailsLayer.getItemIndex( graphic );
                    if( index >= 0 ) {
                        detailsLayer.removeItemAt( index );
                    }
                }
                
                _graphics.removeAll();
                _graphics = null;
            }
        }
        
        private function updateGraphics() : void
        {
            if ( _entryModel )
            {
                if( _entryModel.containsAlert && _entryModel.alert)
                {
                    for each ( var infoBlock : AlertInfo in _entryModel.alert.alertInfo ) {
                        processInfoBlock( _entryModel, infoBlock );
                    }
                }
                else
                {
                    var geometry : Geometry = _entryModel.entryData.geometry;
                    
                    if ( geometry && geometry.extent != null )
                    {
                        var graphic : Graphic = new CapanGraphic( _entryModel, WebMercatorUtil.geographicToWebMercator( geometry ) );
                        var colour : MasasEntryColourEnum = null;
                        
                        if( _entryModel.entryData.category.colourCode ) {
                            colour = _entryModel.entryData.category.colourCode.colour;
                        }
                        
                        graphic.symbol = _masasMgr.symbolManager.getEntryAreaSymbol( geometry.type, colour );
                        
                        if( graphic.symbol )
                        {
                            (_presMgr.detailsLayer.graphicProvider as ArrayCollection).addItemAt( graphic, 0 );
                            _graphics.addItem( graphic );
                        }
                    }
                }
            }
        }
        
        private function processInfoBlock( entry : EntryModel, info : AlertInfo ) : void
        {
            var graphic:Graphic;
            var geometry:Geometry;
            
            var paramList:Vector.<AlertInfoParameter> = info.parameters;
            for each (var param:AlertInfoParameter in paramList )
            {
                if ( param )
                {					
                    switch (param.valueName)
                    {
                        case "layer:CAPAN:eventLocation:point":
                        {
                            geometry = WebMercatorUtil.geographicToWebMercator( CapUtil.parseGeoRSSPoint(param.value) );
                            
                            if (geometry)
                            {
                                graphic = new CapanGraphic( entry, geometry );
                                graphic.symbol = _masasMgr.symbolManager.resultMarkerSymbol;
                                (_presMgr.detailsLayer.graphicProvider as ArrayCollection).addItemAt( graphic, 0 );
                                _graphics.addItem( graphic );
                            }						
                            break;
                        }
                        case "layer:CAPAN:eventLocation:line":
                        {
                            geometry = WebMercatorUtil.geographicToWebMercator( CapUtil.parseGeoRSSLine(param.value) );
                            
                            if (geometry)
                            {
                                graphic = new CapanGraphic( entry, geometry );
                                graphic.symbol = _masasMgr.symbolManager.capanLineSymbol;
                                (_presMgr.detailsLayer.graphicProvider as ArrayCollection).addItemAt( graphic, 0 );
                                _graphics.addItem( graphic );
                            }		
                            
                            break;
                        }
                        case "layer:CAPAN:eventLocation:polygon":
                        {
                            geometry = WebMercatorUtil.geographicToWebMercator( CapUtil.parseGeoRSSPolygon(param.value) );
                            
                            if (geometry)
                            {
                                graphic = new CapanGraphic( entry, geometry );
                                graphic.symbol = _masasMgr.symbolManager.capanFillSymbol;
                                (_presMgr.detailsLayer.graphicProvider as ArrayCollection).addItemAt( graphic, 0 );
                                _graphics.addItem( graphic );
                            }		
                            
                            break;
                        }
                            
                        case "layer:CAPAN:eventLocation:circle":
                        {
                            geometry = WebMercatorUtil.geographicToWebMercator( CapUtil.parseGeoRSSCircle(param.value) );
                            
                            if (geometry)
                            {
                                graphic = new CapanGraphic( entry, geometry );
                                graphic.symbol = _masasMgr.symbolManager.capanFillSymbol;
                                (_presMgr.detailsLayer.graphicProvider as ArrayCollection).addItemAt( graphic, 0 );
                                _graphics.addItem( graphic );
                            }		
                            
                            break;
                        }
                    }
                }
            }
            
            var areaBlocks:Vector.<AlertInfoArea> = info.areas;
            for each (var areaBlock:AlertInfoArea in areaBlocks )
            {
                var polygons:Vector.<AlertInfoAreaPolygon> = areaBlock.polygon;
                
                for each (var polygon:AlertInfoAreaPolygon in polygons)
                {							
                    graphic = new CapGraphic( entry, WebMercatorUtil.geographicToWebMercator(polygon.asPolygon) );
                    graphic.symbol = _masasMgr.symbolManager.alertFillSymbol;
                    
                    if (graphic.geometry)
                    {
                        (_presMgr.detailsLayer.graphicProvider as ArrayCollection).addItemAt( graphic, 0 );
                        _graphics.addItem( graphic );
                    }
                }
                
                var circles:Vector.<AlertInfoAreaCircle> = areaBlock.circle;
                
                for each (var circle:AlertInfoAreaCircle in circles )
                {
                    graphic =  new CapGraphic( entry, WebMercatorUtil.geographicToWebMercator(circle.asPolygon()) );
                    graphic.symbol = _masasMgr.symbolManager.alertFillSymbol;
                    
                    if (graphic.geometry)
                    {
                        (_presMgr.detailsLayer.graphicProvider as ArrayCollection).addItemAt( graphic, 0 );
                        _graphics.addItem( graphic );
                    }
                }
            }
        }
        
    }
    
}