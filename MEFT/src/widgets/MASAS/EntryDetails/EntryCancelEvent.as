package widgets.MASAS.EntryDetails
{
	
	import mx.events.CloseEvent;
	
	public class EntryCancelEvent extends CloseEvent
	{
		
		public var reason : String = "";
		
		public function EntryCancelEvent( type : String, bubbles : Boolean = false, cancelable : Boolean = false, detail : int = -1, reason : String = "" )
		{
			super(type, bubbles, cancelable, detail);
			
			this.reason = reason;
		}
		
	}
	
}