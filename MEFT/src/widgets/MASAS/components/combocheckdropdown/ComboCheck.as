////////////////////////////////////////////////////////////////////////////////
// 
// ComboCheck v1.4.0
// Arcadio Carballares Martín, 2011
// http://www.arcadiocarballares.com
// Creative Commons - http://creativecommons.org/licenses/by-sa/2.5/es/deed.en_GB
// 
////////////////////////////////////////////////////////////////////////////////
package widgets.MASAS.components.combocheckdropdown
{
    import flash.events.Event;
    import flash.events.KeyboardEvent;
    import flash.events.MouseEvent;
    import flash.ui.Keyboard;
    
    import mx.collections.ArrayCollection;
    import mx.collections.IList;
    import mx.core.ClassFactory;
    import mx.events.FlexEvent;
    import mx.events.ItemClickEvent;
    
    import spark.components.CheckBox;
    import spark.components.ComboBox;
    import spark.events.DropDownEvent;
    
    [Event("addItem", type="flash.events.Event")]
    [Event("removeItem", type="flash.events.Event")]
    
    [Style(name="textDelimiter", type="String", inherit="no")]
    
    public class ComboCheck extends ComboBox {
        private var m_labelField:String = "label";
        
        private var _selectedItems:Vector.<Object>;
        
        [Bindable("change")]
        [Bindable("valueCommit")]
        [Bindable("collectionChange")]
        override public function set selectedItems(value:Vector.<Object>):void {
            _selectedItems = value;
        }
        
        override public function get selectedItems():Vector.<Object> {
            return _selectedItems;
        }
        
        public function get selectedItemsCount():uint
        {
            return _selectedItems.length;
        }
        
        public function ComboCheck() {
            super();
            
            var render:ClassFactory = new ClassFactory(ComboCheckItemRenderer);
            
            super.itemRenderer = render;
            super.dropDownController = new DropController;
            
            addEventListener(DropDownEvent.CLOSE, onDropDownClose);
            addEventListener(ItemClickEvent.ITEM_CLICK, onItemClick);
            addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown_EventHandler);
            
            addEventListener(FlexEvent.CREATION_COMPLETE, onCreationComplete);
            
            // Set the default style...
            setStyle( "textDelimiter", ", " );
        }
        
        public function clearSelection():void
        {
            for (var i:int; i < dataProvider.length; i++) 
            {
                dataProvider[i].selected = false;
            }
            selectedItems = new Vector.<Object>;
            updateText();
        }
        
        public function updateSelection(ac:ArrayCollection):void
        {
            selectedItems = new Vector.<Object>;
            
            for (var i:int = 0; i < ac.length; i++ )
            {
                for (var j:int = 0; j < dataProvider.length; j++ )
                {
                    if ( dataProvider[j].label == ac[i] )
                    {
                        dataProvider[j].selected = true;
                        selectedItems.push(dataProvider[j]);
                    }
                }
            }			
            updateText();			
        }
        
        private function onCreationComplete(event:FlexEvent):void
        {
            textInput.enabled = false;
            updateText();
        }
        
        private function onKeyDown_EventHandler(event:KeyboardEvent):void {
            if (event.keyCode == Keyboard.ENTER && isDropDownOpen && textInput.text.length > 0) {
                var currentItem:Object = getItem(textInput.text);
                
                if (currentItem != null) {
                    currentItem.selected = !currentItem.selected;
                    onCheck(currentItem);
                    invalidateProperties();
                }
            }
        }
        
        private function getItem(text:String):Object {
            for each (var item:Object in dataProvider) {
                if (item.label == text)
                    return item;
            }
            
            return null;
        }
        
        override public function set labelField(value:String):void {
            m_labelField = value;
        }
        
        override public function get labelField():String {
            return m_labelField;
        }
        
        override public function set dataProvider(value:IList):void {
            super.dataProvider = value;
            
            // Load initial selected items
            selectedItems = new Vector.<Object>;
            
            for (var i:int; i < dataProvider.length; i++) {
                if (dataProvider[i].selected == true) {
                    selectedItems.push(dataProvider[i]);
                }
            }
        }
        
        override protected function commitProperties():void {
            super.commitProperties();
            
            var render:ClassFactory = new ClassFactory(ComboCheckItemRenderer);
            
            super.itemRenderer = render;
        }
        
        override protected function item_mouseDownHandler(event:MouseEvent):void {
            if (event.target is ComboCheckItemRenderer) {
                var render:ComboCheckItemRenderer = event.target as ComboCheckItemRenderer;
                var check:CheckBox = render.item as CheckBox;
                
                if (check.selected) {
                    render.data.selected = false;
                    check.selected = false;
                }
                else {
                    render.data.selected = true;
                    check.selected = true;
                }
                
                onCheck(render.data);
            }
        }
        
        private function onItemClick(event:ItemClickEvent):void 
        {
            onCheck(event.item);
        }
        
        private function onDropDownClose(event:DropDownEvent):void {
            selectedIndex = -1;
        }
        
        private function onCheck(obj:Object):void 
        {
            if (selectedItems == null || selectedItems.indexOf(obj) == -1) {
                if (selectedItems == null) {
                    selectedItems = new Vector.<Object>();
                }
                selectedItems.push(obj);
                
                dispatchEvent(new Event("addItem"));
            }
            else {
                var index:int = selectedItems.indexOf(obj);
                selectedItems.splice(index, 1);
                dispatchEvent(new Event("removeItem"));
            }
            
            updateText();
            
            dispatchEvent(new Event("valueCommit"));
        }
        
        private function updateText():void
        {
            if ( selectedItems )
            {
                if (selectedItems.length>1) {
                    //textInput.text='Multiple'
                    textInput.text = "";
                    for each( var item : Object in selectedItems )
                    {
                        if( textInput.text.length > 0 ) {
                            textInput.text += getStyle("textDelimiter");
                        }
                        
                        textInput.text += item[labelField];
                    }
                }
                if (selectedItems.length==1) {
                    textInput.text=selectedItems[0][labelField];
                }
                if (selectedItems.length<1) {
                    textInput.text='';
                }
            }
            else
            {
                textInput.text='';
            }
        }
    }
}