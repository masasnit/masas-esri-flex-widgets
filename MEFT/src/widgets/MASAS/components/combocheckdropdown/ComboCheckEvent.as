////////////////////////////////////////////////////////////////////////////////
// 
// ComboCheck v1.4.0
// Arcadio Carballares Martín, 2011
// http://www.arcadiocarballares.com
// Creative Commons - http://creativecommons.org/licenses/by-sa/2.5/es/deed.en_GB
// 
////////////////////////////////////////////////////////////////////////////////
package widgets.MASAS.components.combocheckdropdown
{
	import flash.events.Event;
	
	public class ComboCheckEvent extends Event {
		
		public static const COMBO_CHECKED:String = "comboChecked";
		public var obj:Object=new Object();
		
		public function ComboCheckEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false) {
			super(type, bubbles, cancelable);
		}		
	}	
}