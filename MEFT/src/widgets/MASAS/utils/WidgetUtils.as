package widgets.MASAS.utils
{
	import com.esri.viewer.ConfigData;
	import com.esri.viewer.IBaseWidget;
	import com.esri.viewer.ViewerContainer;
	
	import MASAS.MASAS;
	
	public class WidgetUtils
	{
		
		static public const MASAS_WIDGET_NAME : String = "MASASWidget";
		static public const PUBLISH_WIDGET_NAME : String = "PublishWidget";
		static public const ENTRY_DETAILS_WIDGET_NAME : String = "EntryDetailsWidget";
		
		// Returns a widget Id based on a label
		static public function getWidgetId( configData : ConfigData, widgetLabel : String) : Number
		{
			var id:Number;
			for (var i:Number = 0; i < configData.widgets.length; i++)
			{
				if (configData.widgets[i].label == widgetLabel)
					id = configData.widgets[i].id;
			}
			return id;
		}
		
		static public function getMasasWidget( configData : ConfigData ) : IBaseWidget
		{
			return 	getWidgetByClassName( configData, MASAS_WIDGET_NAME );
		}
		
		static public function getPublishWidget( configData : ConfigData ) : IBaseWidget
		{
			return 	getWidgetByClassName( configData, PUBLISH_WIDGET_NAME );
		}
		
		static public function getEntryDetailsWidget( configData : ConfigData ) : IBaseWidget
		{
			return 	getWidgetByClassName( configData, ENTRY_DETAILS_WIDGET_NAME );
		}
		
		static public function getWidgetByClassName( configData : ConfigData, className : String ) : IBaseWidget
		{
			var widget : IBaseWidget;
			
			for( var i : Number = 0; i < configData.widgets.length; i++ )
			{
				var widgetId : Number = configData.widgets[i].id;
				var widgetUrl : String = configData.widgets[i].url; 
				if( widgetUrl && ( widgetUrl.indexOf( className ) >= 0 ) )
				{
					widget = ViewerContainer.getInstance().widgetManager.getWidget( widgetId, true );
					break;
				}
			}
			
			return widget;
		}
		
		static public function getWidgetIdByClassName( configData : ConfigData, className : String ) : Number
		{
			var widgetId : Number = -1;
			
			for( var i : Number = 0; i < configData.widgets.length; i++ )
			{
				var widgetUrl : String = configData.widgets[i].url; 
				if( widgetUrl && ( widgetUrl.indexOf( className ) >= 0 ) )
				{
					widgetId = configData.widgets[i].id;
					break;
				}
			}
			
			return widgetId;
		}
		
	}
	
}