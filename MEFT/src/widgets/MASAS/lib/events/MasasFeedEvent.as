////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 ESRI
//
// All rights reserved under the copyright laws of the United States.
// You may freely redistribute and use this software, with or
// without modification, provided you include the original copyright
// and use restrictions.  See use restrictions in the file:
// <install location>/License.txt
//
////////////////////////////////////////////////////////////////////////////////
package widgets.MASAS.lib.events
{	
	import com.esri.viewer.AppEvent;
	
	import flash.events.Event;
	
	public class MasasFeedEvent extends AppEvent
	{
		public static const MASAS_FEED_REFRESH:String = "masasFeedRefresh";
		
		public function MasasFeedEvent(type:String, data:Object = null, callback:Function = null)
		{
			super(type, data, callback);
		}
		
		public override function clone():Event
		{
			return new MasasFeedEvent(this.type, this.data, this.callback);
		}
	}
}